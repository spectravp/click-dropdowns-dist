import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DropdownComponent } from './components/dropdown/dropdown.component';
import { CommonModule } from '@angular/common';
import { DropdownMultiComponent } from './components/dropdown-multi/dropdown-multi.component';
import * as i0 from "@angular/core";
export class DropdownsModule {
}
DropdownsModule.ɵfac = function DropdownsModule_Factory(t) { return new (t || DropdownsModule)(); };
DropdownsModule.ɵmod = /*@__PURE__*/ i0.ɵɵdefineNgModule({ type: DropdownsModule });
DropdownsModule.ɵinj = /*@__PURE__*/ i0.ɵɵdefineInjector({ imports: [FormsModule,
        ReactiveFormsModule,
        CommonModule] });
(function () { (typeof ngDevMode === "undefined" || ngDevMode) && i0.ɵsetClassMetadata(DropdownsModule, [{
        type: NgModule,
        args: [{
                declarations: [DropdownComponent, DropdownMultiComponent],
                imports: [
                    FormsModule,
                    ReactiveFormsModule,
                    CommonModule
                ],
                exports: [DropdownComponent, DropdownMultiComponent]
            }]
    }], null, null); })();
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && i0.ɵɵsetNgModuleScope(DropdownsModule, { declarations: [DropdownComponent, DropdownMultiComponent], imports: [FormsModule,
        ReactiveFormsModule,
        CommonModule], exports: [DropdownComponent, DropdownMultiComponent] }); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZHJvcGRvd25zLm1vZHVsZS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uL3Byb2plY3RzL2Ryb3Bkb3ducy9zcmMvbGliL2Ryb3Bkb3ducy5tb2R1bGUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxFQUFFLFFBQVEsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUN6QyxPQUFPLEVBQUMsV0FBVyxFQUFFLG1CQUFtQixFQUFDLE1BQU0sZ0JBQWdCLENBQUM7QUFDaEUsT0FBTyxFQUFDLGlCQUFpQixFQUFDLE1BQU0sMENBQTBDLENBQUM7QUFDM0UsT0FBTyxFQUFDLFlBQVksRUFBQyxNQUFNLGlCQUFpQixDQUFDO0FBQzdDLE9BQU8sRUFBRSxzQkFBc0IsRUFBRSxNQUFNLHNEQUFzRCxDQUFDOztBQWE5RixNQUFNLE9BQU8sZUFBZTs7OEVBQWYsZUFBZTtpRUFBZixlQUFlO3FFQU54QixXQUFXO1FBQ1gsbUJBQW1CO1FBQ25CLFlBQVk7dUZBSUgsZUFBZTtjQVQzQixRQUFRO2VBQUM7Z0JBQ1IsWUFBWSxFQUFFLENBQUMsaUJBQWlCLEVBQUUsc0JBQXNCLENBQUM7Z0JBQ3pELE9BQU8sRUFBRTtvQkFDUCxXQUFXO29CQUNYLG1CQUFtQjtvQkFDbkIsWUFBWTtpQkFDYjtnQkFDRCxPQUFPLEVBQUUsQ0FBQyxpQkFBaUIsRUFBRSxzQkFBc0IsQ0FBQzthQUNyRDs7d0ZBQ1ksZUFBZSxtQkFSWCxpQkFBaUIsRUFBRSxzQkFBc0IsYUFFdEQsV0FBVztRQUNYLG1CQUFtQjtRQUNuQixZQUFZLGFBRUosaUJBQWlCLEVBQUUsc0JBQXNCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgTmdNb2R1bGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7Rm9ybXNNb2R1bGUsIFJlYWN0aXZlRm9ybXNNb2R1bGV9IGZyb20gJ0Bhbmd1bGFyL2Zvcm1zJztcbmltcG9ydCB7RHJvcGRvd25Db21wb25lbnR9IGZyb20gJy4vY29tcG9uZW50cy9kcm9wZG93bi9kcm9wZG93bi5jb21wb25lbnQnO1xuaW1wb3J0IHtDb21tb25Nb2R1bGV9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbic7XG5pbXBvcnQgeyBEcm9wZG93bk11bHRpQ29tcG9uZW50IH0gZnJvbSAnLi9jb21wb25lbnRzL2Ryb3Bkb3duLW11bHRpL2Ryb3Bkb3duLW11bHRpLmNvbXBvbmVudCc7XG5cblxuXG5ATmdNb2R1bGUoe1xuICBkZWNsYXJhdGlvbnM6IFtEcm9wZG93bkNvbXBvbmVudCwgRHJvcGRvd25NdWx0aUNvbXBvbmVudF0sXG4gIGltcG9ydHM6IFtcbiAgICBGb3Jtc01vZHVsZSxcbiAgICBSZWFjdGl2ZUZvcm1zTW9kdWxlLFxuICAgIENvbW1vbk1vZHVsZVxuICBdLFxuICBleHBvcnRzOiBbRHJvcGRvd25Db21wb25lbnQsIERyb3Bkb3duTXVsdGlDb21wb25lbnRdXG59KVxuZXhwb3J0IGNsYXNzIERyb3Bkb3duc01vZHVsZSB7IH1cbiJdfQ==