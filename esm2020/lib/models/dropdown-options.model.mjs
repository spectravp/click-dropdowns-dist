export class DropdownOptionsModel {
    /**
     * Constructor
     */
    constructor(items, options) {
        /**
         * The is what we should use to get the identifier of the selected item
         * Defaults to ID
         */
        this.identifier = 'id';
        /**
         * This is what we should use to get the display value of the selected item
         */
        this.label = 'label';
        /**
         * Placeholder to display
         */
        this.placeholder = 'Select...';
        /**
         * If true, adds the animated class to the dropdown
         */
        this.animated = true;
        /**
         * If true, will close other dropdowns when this one opens
         */
        this.closeOthers = true;
        /**
         * If true, will close when the document is clicked
         */
        this.closeOnDocumentClick = true;
        /**
         * If true, will close when they make a selection
         */
        this.closeOnSelection = true;
        /**
         * When true, will display the placeholder caret after the placeholder text
         */
        this.displayPlaceholderCaretAfter = true;
        /**
         * When true, will display the placeholder caret before the placeholder text
         */
        this.displayPlaceholderCaretBefore = false;
        /**
         * The CSS caret class (You can use font awesome classes, but I wouldn't include font awesome in this library)
         */
        this.caretClass = 'fa fa-angle-down';
        /**
         * The CSS selected item class (You can use font awesome classes, but I wouldn't include font awesome in this library)
         */
        this.selectedItemClass = 'fa fa-check-circle';
        /**
         * Used only for multi dropdowns, when true will use all of the selections as the placeholder label
         * When it is a string, it will use the string. When false, it will use the placeholder label
         */
        this.selectionsAsLabel = false;
        /**
         * When true, will use an image in the drop down, requires the imageKey
         */
        this.useImages = false;
        /**
         * Where in the object of options to get the image from
         */
        this.imageKey = 'image';
        /**
         * Flag to use labels, typically only used if you want to hide labels because you are using images
         */
        this.useLabels = true;
        /**
         * When true, it will only allow valid values. All other will be destroyed
         */
        this.allowOnlyValidValues = true;
        /**
         * Flag for if this is filterable
         */
        this.filterable = false;
        /**
         * If true, the placeholder will become a selectable item when something other than
         * the placeholder is selected.
         */
        this.usePlaceholderAsItem = false;
        this.items = items;
        if (typeof options !== 'undefined' && options !== null) {
            if (options.identifier) {
                this.identifier = options.identifier;
            }
            if (options.label) {
                this.label = options.label;
            }
            if (typeof options.placeholder !== 'undefined') {
                this.placeholder = options.placeholder;
            }
            if (typeof options.animated !== 'undefined') {
                this.animated = options.animated;
            }
            if (typeof options.closeOthers !== 'undefined') {
                this.closeOthers = options.closeOthers;
            }
            if (typeof options.closeOnDocumentClick !== 'undefined') {
                this.closeOnDocumentClick = options.closeOnDocumentClick;
            }
            if (typeof options.closeOnSelection !== 'undefined') {
                this.closeOnSelection = options.closeOnSelection;
            }
            if (typeof options.displayPlaceholderCaretAfter !== 'undefined') {
                this.displayPlaceholderCaretAfter = options.displayPlaceholderCaretAfter;
            }
            if (typeof options.displayPlaceholderCaretBefore !== 'undefined') {
                this.displayPlaceholderCaretBefore = options.displayPlaceholderCaretBefore;
            }
            if (typeof options.selectionsAsLabel !== 'undefined') {
                this.selectionsAsLabel = options.selectionsAsLabel;
            }
            if (typeof options.caretClass !== 'undefined') {
                this.caretClass = options.caretClass;
            }
            if (typeof options.selectedItemClass !== 'undefined') {
                this.selectedItemClass = options.selectedItemClass;
            }
            if (typeof options.useImages !== 'undefined') {
                this.useImages = options.useImages;
            }
            if (typeof options.imageKey !== 'undefined') {
                this.imageKey = options.imageKey;
            }
            if (typeof options.useLabels !== 'undefined') {
                this.useLabels = options.useLabels;
            }
            if (typeof options.allowOnlyValidValues !== 'undefined') {
                this.allowOnlyValidValues = options.allowOnlyValidValues;
            }
            if (typeof options.filterable !== 'undefined') {
                this.filterable = options.filterable;
            }
            if (typeof options.usePlaceholderAsItem !== 'undefined') {
                this.usePlaceholderAsItem = options.usePlaceholderAsItem;
            }
        }
    }
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZHJvcGRvd24tb3B0aW9ucy5tb2RlbC5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uL3Byb2plY3RzL2Ryb3Bkb3ducy9zcmMvbGliL21vZGVscy9kcm9wZG93bi1vcHRpb25zLm1vZGVsLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUVBLE1BQU0sT0FBTyxvQkFBb0I7SUFvRy9COztPQUVHO0lBQ0gsWUFBWSxLQUFLLEVBQUUsT0FBYTtRQWhHaEM7OztXQUdHO1FBQ0ksZUFBVSxHQUFHLElBQUksQ0FBQztRQUV6Qjs7V0FFRztRQUNJLFVBQUssR0FBRyxPQUFPLENBQUM7UUFFdkI7O1dBRUc7UUFDSSxnQkFBVyxHQUFHLFdBQVcsQ0FBQztRQUVqQzs7V0FFRztRQUNJLGFBQVEsR0FBRyxJQUFJLENBQUM7UUFFdkI7O1dBRUc7UUFDSSxnQkFBVyxHQUFHLElBQUksQ0FBQztRQUUxQjs7V0FFRztRQUNJLHlCQUFvQixHQUFHLElBQUksQ0FBQztRQUVuQzs7V0FFRztRQUNJLHFCQUFnQixHQUFHLElBQUksQ0FBQztRQUUvQjs7V0FFRztRQUNJLGlDQUE0QixHQUFHLElBQUksQ0FBQztRQUUzQzs7V0FFRztRQUNJLGtDQUE2QixHQUFHLEtBQUssQ0FBQztRQUU3Qzs7V0FFRztRQUNJLGVBQVUsR0FBRyxrQkFBa0IsQ0FBQztRQUV2Qzs7V0FFRztRQUNJLHNCQUFpQixHQUFHLG9CQUFvQixDQUFDO1FBRWhEOzs7V0FHRztRQUNJLHNCQUFpQixHQUFtQixLQUFLLENBQUM7UUFFakQ7O1dBRUc7UUFDSSxjQUFTLEdBQUcsS0FBSyxDQUFDO1FBRXpCOztXQUVHO1FBQ0ksYUFBUSxHQUFHLE9BQU8sQ0FBQztRQUUxQjs7V0FFRztRQUNJLGNBQVMsR0FBRyxJQUFJLENBQUM7UUFFeEI7O1dBRUc7UUFDSSx5QkFBb0IsR0FBRyxJQUFJLENBQUM7UUFFbkM7O1dBRUc7UUFDSSxlQUFVLEdBQUcsS0FBSyxDQUFDO1FBRTFCOzs7V0FHRztRQUNJLHlCQUFvQixHQUFHLEtBQUssQ0FBQztRQU1sQyxJQUFJLENBQUMsS0FBSyxHQUFHLEtBQUssQ0FBQztRQUNuQixJQUFJLE9BQU8sT0FBTyxLQUFLLFdBQVcsSUFBSSxPQUFPLEtBQUssSUFBSSxFQUFFO1lBQ3RELElBQUksT0FBTyxDQUFDLFVBQVUsRUFBRTtnQkFDdEIsSUFBSSxDQUFDLFVBQVUsR0FBRyxPQUFPLENBQUMsVUFBVSxDQUFDO2FBQ3RDO1lBQ0QsSUFBSSxPQUFPLENBQUMsS0FBSyxFQUFFO2dCQUNqQixJQUFJLENBQUMsS0FBSyxHQUFHLE9BQU8sQ0FBQyxLQUFLLENBQUM7YUFDNUI7WUFDRCxJQUFJLE9BQU8sT0FBTyxDQUFDLFdBQVcsS0FBSyxXQUFXLEVBQUU7Z0JBQzlDLElBQUksQ0FBQyxXQUFXLEdBQUcsT0FBTyxDQUFDLFdBQVcsQ0FBQzthQUN4QztZQUNELElBQUksT0FBTyxPQUFPLENBQUMsUUFBUSxLQUFLLFdBQVcsRUFBRTtnQkFDM0MsSUFBSSxDQUFDLFFBQVEsR0FBRyxPQUFPLENBQUMsUUFBUSxDQUFDO2FBQ2xDO1lBQ0QsSUFBSSxPQUFPLE9BQU8sQ0FBQyxXQUFXLEtBQUssV0FBVyxFQUFFO2dCQUM5QyxJQUFJLENBQUMsV0FBVyxHQUFHLE9BQU8sQ0FBQyxXQUFXLENBQUM7YUFDeEM7WUFDRCxJQUFJLE9BQU8sT0FBTyxDQUFDLG9CQUFvQixLQUFLLFdBQVcsRUFBRTtnQkFDdkQsSUFBSSxDQUFDLG9CQUFvQixHQUFHLE9BQU8sQ0FBQyxvQkFBb0IsQ0FBQzthQUMxRDtZQUNELElBQUksT0FBTyxPQUFPLENBQUMsZ0JBQWdCLEtBQUssV0FBVyxFQUFFO2dCQUNuRCxJQUFJLENBQUMsZ0JBQWdCLEdBQUcsT0FBTyxDQUFDLGdCQUFnQixDQUFDO2FBQ2xEO1lBQ0QsSUFBSSxPQUFPLE9BQU8sQ0FBQyw0QkFBNEIsS0FBSyxXQUFXLEVBQUU7Z0JBQy9ELElBQUksQ0FBQyw0QkFBNEIsR0FBRyxPQUFPLENBQUMsNEJBQTRCLENBQUM7YUFDMUU7WUFDRCxJQUFJLE9BQU8sT0FBTyxDQUFDLDZCQUE2QixLQUFLLFdBQVcsRUFBRTtnQkFDaEUsSUFBSSxDQUFDLDZCQUE2QixHQUFHLE9BQU8sQ0FBQyw2QkFBNkIsQ0FBQzthQUM1RTtZQUNELElBQUksT0FBTyxPQUFPLENBQUMsaUJBQWlCLEtBQUssV0FBVyxFQUFFO2dCQUNwRCxJQUFJLENBQUMsaUJBQWlCLEdBQUcsT0FBTyxDQUFDLGlCQUFpQixDQUFDO2FBQ3BEO1lBQ0QsSUFBSSxPQUFPLE9BQU8sQ0FBQyxVQUFVLEtBQUssV0FBVyxFQUFFO2dCQUM3QyxJQUFJLENBQUMsVUFBVSxHQUFHLE9BQU8sQ0FBQyxVQUFVLENBQUM7YUFDdEM7WUFDRCxJQUFJLE9BQU8sT0FBTyxDQUFDLGlCQUFpQixLQUFLLFdBQVcsRUFBRTtnQkFDcEQsSUFBSSxDQUFDLGlCQUFpQixHQUFHLE9BQU8sQ0FBQyxpQkFBaUIsQ0FBQzthQUNwRDtZQUNELElBQUksT0FBTyxPQUFPLENBQUMsU0FBUyxLQUFLLFdBQVcsRUFBRTtnQkFDNUMsSUFBSSxDQUFDLFNBQVMsR0FBRyxPQUFPLENBQUMsU0FBUyxDQUFDO2FBQ3BDO1lBQ0QsSUFBSSxPQUFPLE9BQU8sQ0FBQyxRQUFRLEtBQUssV0FBVyxFQUFFO2dCQUMzQyxJQUFJLENBQUMsUUFBUSxHQUFHLE9BQU8sQ0FBQyxRQUFRLENBQUM7YUFDbEM7WUFDRCxJQUFJLE9BQU8sT0FBTyxDQUFDLFNBQVMsS0FBSyxXQUFXLEVBQUU7Z0JBQzVDLElBQUksQ0FBQyxTQUFTLEdBQUcsT0FBTyxDQUFDLFNBQVMsQ0FBQzthQUNwQztZQUNELElBQUksT0FBTyxPQUFPLENBQUMsb0JBQW9CLEtBQUssV0FBVyxFQUFFO2dCQUN2RCxJQUFJLENBQUMsb0JBQW9CLEdBQUcsT0FBTyxDQUFDLG9CQUFvQixDQUFDO2FBQzFEO1lBQ0QsSUFBSSxPQUFPLE9BQU8sQ0FBQyxVQUFVLEtBQUssV0FBVyxFQUFFO2dCQUM3QyxJQUFJLENBQUMsVUFBVSxHQUFHLE9BQU8sQ0FBQyxVQUFVLENBQUM7YUFDdEM7WUFDRCxJQUFJLE9BQU8sT0FBTyxDQUFDLG9CQUFvQixLQUFLLFdBQVcsRUFBRTtnQkFDdkQsSUFBSSxDQUFDLG9CQUFvQixHQUFHLE9BQU8sQ0FBQyxvQkFBb0IsQ0FBQzthQUMxRDtTQUNGO0lBQ0gsQ0FBQztDQUNGIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtGb3JtQ29udHJvbH0gZnJvbSAnQGFuZ3VsYXIvZm9ybXMnO1xuXG5leHBvcnQgY2xhc3MgRHJvcGRvd25PcHRpb25zTW9kZWwge1xuXG4gIC8qKlxuICAgKiBUaGVzZSBhcmUgdGhlIGl0ZW1zIHRvIGxvb3Agb3ZlciBpbiB0aGUgZHJvcCBkb3duXG4gICAqL1xuICBwdWJsaWMgaXRlbXM6IGFueVtdO1xuXG4gIC8qKlxuICAgKiBUaGUgaXMgd2hhdCB3ZSBzaG91bGQgdXNlIHRvIGdldCB0aGUgaWRlbnRpZmllciBvZiB0aGUgc2VsZWN0ZWQgaXRlbVxuICAgKiBEZWZhdWx0cyB0byBJRFxuICAgKi9cbiAgcHVibGljIGlkZW50aWZpZXIgPSAnaWQnO1xuXG4gIC8qKlxuICAgKiBUaGlzIGlzIHdoYXQgd2Ugc2hvdWxkIHVzZSB0byBnZXQgdGhlIGRpc3BsYXkgdmFsdWUgb2YgdGhlIHNlbGVjdGVkIGl0ZW1cbiAgICovXG4gIHB1YmxpYyBsYWJlbCA9ICdsYWJlbCc7XG5cbiAgLyoqXG4gICAqIFBsYWNlaG9sZGVyIHRvIGRpc3BsYXlcbiAgICovXG4gIHB1YmxpYyBwbGFjZWhvbGRlciA9ICdTZWxlY3QuLi4nO1xuXG4gIC8qKlxuICAgKiBJZiB0cnVlLCBhZGRzIHRoZSBhbmltYXRlZCBjbGFzcyB0byB0aGUgZHJvcGRvd25cbiAgICovXG4gIHB1YmxpYyBhbmltYXRlZCA9IHRydWU7XG5cbiAgLyoqXG4gICAqIElmIHRydWUsIHdpbGwgY2xvc2Ugb3RoZXIgZHJvcGRvd25zIHdoZW4gdGhpcyBvbmUgb3BlbnNcbiAgICovXG4gIHB1YmxpYyBjbG9zZU90aGVycyA9IHRydWU7XG5cbiAgLyoqXG4gICAqIElmIHRydWUsIHdpbGwgY2xvc2Ugd2hlbiB0aGUgZG9jdW1lbnQgaXMgY2xpY2tlZFxuICAgKi9cbiAgcHVibGljIGNsb3NlT25Eb2N1bWVudENsaWNrID0gdHJ1ZTtcblxuICAvKipcbiAgICogSWYgdHJ1ZSwgd2lsbCBjbG9zZSB3aGVuIHRoZXkgbWFrZSBhIHNlbGVjdGlvblxuICAgKi9cbiAgcHVibGljIGNsb3NlT25TZWxlY3Rpb24gPSB0cnVlO1xuXG4gIC8qKlxuICAgKiBXaGVuIHRydWUsIHdpbGwgZGlzcGxheSB0aGUgcGxhY2Vob2xkZXIgY2FyZXQgYWZ0ZXIgdGhlIHBsYWNlaG9sZGVyIHRleHRcbiAgICovXG4gIHB1YmxpYyBkaXNwbGF5UGxhY2Vob2xkZXJDYXJldEFmdGVyID0gdHJ1ZTtcblxuICAvKipcbiAgICogV2hlbiB0cnVlLCB3aWxsIGRpc3BsYXkgdGhlIHBsYWNlaG9sZGVyIGNhcmV0IGJlZm9yZSB0aGUgcGxhY2Vob2xkZXIgdGV4dFxuICAgKi9cbiAgcHVibGljIGRpc3BsYXlQbGFjZWhvbGRlckNhcmV0QmVmb3JlID0gZmFsc2U7XG5cbiAgLyoqXG4gICAqIFRoZSBDU1MgY2FyZXQgY2xhc3MgKFlvdSBjYW4gdXNlIGZvbnQgYXdlc29tZSBjbGFzc2VzLCBidXQgSSB3b3VsZG4ndCBpbmNsdWRlIGZvbnQgYXdlc29tZSBpbiB0aGlzIGxpYnJhcnkpXG4gICAqL1xuICBwdWJsaWMgY2FyZXRDbGFzcyA9ICdmYSBmYS1hbmdsZS1kb3duJztcblxuICAvKipcbiAgICogVGhlIENTUyBzZWxlY3RlZCBpdGVtIGNsYXNzIChZb3UgY2FuIHVzZSBmb250IGF3ZXNvbWUgY2xhc3NlcywgYnV0IEkgd291bGRuJ3QgaW5jbHVkZSBmb250IGF3ZXNvbWUgaW4gdGhpcyBsaWJyYXJ5KVxuICAgKi9cbiAgcHVibGljIHNlbGVjdGVkSXRlbUNsYXNzID0gJ2ZhIGZhLWNoZWNrLWNpcmNsZSc7XG5cbiAgLyoqXG4gICAqIFVzZWQgb25seSBmb3IgbXVsdGkgZHJvcGRvd25zLCB3aGVuIHRydWUgd2lsbCB1c2UgYWxsIG9mIHRoZSBzZWxlY3Rpb25zIGFzIHRoZSBwbGFjZWhvbGRlciBsYWJlbFxuICAgKiBXaGVuIGl0IGlzIGEgc3RyaW5nLCBpdCB3aWxsIHVzZSB0aGUgc3RyaW5nLiBXaGVuIGZhbHNlLCBpdCB3aWxsIHVzZSB0aGUgcGxhY2Vob2xkZXIgbGFiZWxcbiAgICovXG4gIHB1YmxpYyBzZWxlY3Rpb25zQXNMYWJlbDogYm9vbGVhbnxzdHJpbmcgPSBmYWxzZTtcblxuICAvKipcbiAgICogV2hlbiB0cnVlLCB3aWxsIHVzZSBhbiBpbWFnZSBpbiB0aGUgZHJvcCBkb3duLCByZXF1aXJlcyB0aGUgaW1hZ2VLZXlcbiAgICovXG4gIHB1YmxpYyB1c2VJbWFnZXMgPSBmYWxzZTtcblxuICAvKipcbiAgICogV2hlcmUgaW4gdGhlIG9iamVjdCBvZiBvcHRpb25zIHRvIGdldCB0aGUgaW1hZ2UgZnJvbVxuICAgKi9cbiAgcHVibGljIGltYWdlS2V5ID0gJ2ltYWdlJztcblxuICAvKipcbiAgICogRmxhZyB0byB1c2UgbGFiZWxzLCB0eXBpY2FsbHkgb25seSB1c2VkIGlmIHlvdSB3YW50IHRvIGhpZGUgbGFiZWxzIGJlY2F1c2UgeW91IGFyZSB1c2luZyBpbWFnZXNcbiAgICovXG4gIHB1YmxpYyB1c2VMYWJlbHMgPSB0cnVlO1xuXG4gIC8qKlxuICAgKiBXaGVuIHRydWUsIGl0IHdpbGwgb25seSBhbGxvdyB2YWxpZCB2YWx1ZXMuIEFsbCBvdGhlciB3aWxsIGJlIGRlc3Ryb3llZFxuICAgKi9cbiAgcHVibGljIGFsbG93T25seVZhbGlkVmFsdWVzID0gdHJ1ZTtcblxuICAvKipcbiAgICogRmxhZyBmb3IgaWYgdGhpcyBpcyBmaWx0ZXJhYmxlXG4gICAqL1xuICBwdWJsaWMgZmlsdGVyYWJsZSA9IGZhbHNlO1xuXG4gIC8qKlxuICAgKiBJZiB0cnVlLCB0aGUgcGxhY2Vob2xkZXIgd2lsbCBiZWNvbWUgYSBzZWxlY3RhYmxlIGl0ZW0gd2hlbiBzb21ldGhpbmcgb3RoZXIgdGhhblxuICAgKiB0aGUgcGxhY2Vob2xkZXIgaXMgc2VsZWN0ZWQuXG4gICAqL1xuICBwdWJsaWMgdXNlUGxhY2Vob2xkZXJBc0l0ZW0gPSBmYWxzZTtcblxuICAvKipcbiAgICogQ29uc3RydWN0b3JcbiAgICovXG4gIGNvbnN0cnVjdG9yKGl0ZW1zLCBvcHRpb25zPzogYW55KSB7XG4gICAgdGhpcy5pdGVtcyA9IGl0ZW1zO1xuICAgIGlmICh0eXBlb2Ygb3B0aW9ucyAhPT0gJ3VuZGVmaW5lZCcgJiYgb3B0aW9ucyAhPT0gbnVsbCkge1xuICAgICAgaWYgKG9wdGlvbnMuaWRlbnRpZmllcikge1xuICAgICAgICB0aGlzLmlkZW50aWZpZXIgPSBvcHRpb25zLmlkZW50aWZpZXI7XG4gICAgICB9XG4gICAgICBpZiAob3B0aW9ucy5sYWJlbCkge1xuICAgICAgICB0aGlzLmxhYmVsID0gb3B0aW9ucy5sYWJlbDtcbiAgICAgIH1cbiAgICAgIGlmICh0eXBlb2Ygb3B0aW9ucy5wbGFjZWhvbGRlciAhPT0gJ3VuZGVmaW5lZCcpIHtcbiAgICAgICAgdGhpcy5wbGFjZWhvbGRlciA9IG9wdGlvbnMucGxhY2Vob2xkZXI7XG4gICAgICB9XG4gICAgICBpZiAodHlwZW9mIG9wdGlvbnMuYW5pbWF0ZWQgIT09ICd1bmRlZmluZWQnKSB7XG4gICAgICAgIHRoaXMuYW5pbWF0ZWQgPSBvcHRpb25zLmFuaW1hdGVkO1xuICAgICAgfVxuICAgICAgaWYgKHR5cGVvZiBvcHRpb25zLmNsb3NlT3RoZXJzICE9PSAndW5kZWZpbmVkJykge1xuICAgICAgICB0aGlzLmNsb3NlT3RoZXJzID0gb3B0aW9ucy5jbG9zZU90aGVycztcbiAgICAgIH1cbiAgICAgIGlmICh0eXBlb2Ygb3B0aW9ucy5jbG9zZU9uRG9jdW1lbnRDbGljayAhPT0gJ3VuZGVmaW5lZCcpIHtcbiAgICAgICAgdGhpcy5jbG9zZU9uRG9jdW1lbnRDbGljayA9IG9wdGlvbnMuY2xvc2VPbkRvY3VtZW50Q2xpY2s7XG4gICAgICB9XG4gICAgICBpZiAodHlwZW9mIG9wdGlvbnMuY2xvc2VPblNlbGVjdGlvbiAhPT0gJ3VuZGVmaW5lZCcpIHtcbiAgICAgICAgdGhpcy5jbG9zZU9uU2VsZWN0aW9uID0gb3B0aW9ucy5jbG9zZU9uU2VsZWN0aW9uO1xuICAgICAgfVxuICAgICAgaWYgKHR5cGVvZiBvcHRpb25zLmRpc3BsYXlQbGFjZWhvbGRlckNhcmV0QWZ0ZXIgIT09ICd1bmRlZmluZWQnKSB7XG4gICAgICAgIHRoaXMuZGlzcGxheVBsYWNlaG9sZGVyQ2FyZXRBZnRlciA9IG9wdGlvbnMuZGlzcGxheVBsYWNlaG9sZGVyQ2FyZXRBZnRlcjtcbiAgICAgIH1cbiAgICAgIGlmICh0eXBlb2Ygb3B0aW9ucy5kaXNwbGF5UGxhY2Vob2xkZXJDYXJldEJlZm9yZSAhPT0gJ3VuZGVmaW5lZCcpIHtcbiAgICAgICAgdGhpcy5kaXNwbGF5UGxhY2Vob2xkZXJDYXJldEJlZm9yZSA9IG9wdGlvbnMuZGlzcGxheVBsYWNlaG9sZGVyQ2FyZXRCZWZvcmU7XG4gICAgICB9XG4gICAgICBpZiAodHlwZW9mIG9wdGlvbnMuc2VsZWN0aW9uc0FzTGFiZWwgIT09ICd1bmRlZmluZWQnKSB7XG4gICAgICAgIHRoaXMuc2VsZWN0aW9uc0FzTGFiZWwgPSBvcHRpb25zLnNlbGVjdGlvbnNBc0xhYmVsO1xuICAgICAgfVxuICAgICAgaWYgKHR5cGVvZiBvcHRpb25zLmNhcmV0Q2xhc3MgIT09ICd1bmRlZmluZWQnKSB7XG4gICAgICAgIHRoaXMuY2FyZXRDbGFzcyA9IG9wdGlvbnMuY2FyZXRDbGFzcztcbiAgICAgIH1cbiAgICAgIGlmICh0eXBlb2Ygb3B0aW9ucy5zZWxlY3RlZEl0ZW1DbGFzcyAhPT0gJ3VuZGVmaW5lZCcpIHtcbiAgICAgICAgdGhpcy5zZWxlY3RlZEl0ZW1DbGFzcyA9IG9wdGlvbnMuc2VsZWN0ZWRJdGVtQ2xhc3M7XG4gICAgICB9XG4gICAgICBpZiAodHlwZW9mIG9wdGlvbnMudXNlSW1hZ2VzICE9PSAndW5kZWZpbmVkJykge1xuICAgICAgICB0aGlzLnVzZUltYWdlcyA9IG9wdGlvbnMudXNlSW1hZ2VzO1xuICAgICAgfVxuICAgICAgaWYgKHR5cGVvZiBvcHRpb25zLmltYWdlS2V5ICE9PSAndW5kZWZpbmVkJykge1xuICAgICAgICB0aGlzLmltYWdlS2V5ID0gb3B0aW9ucy5pbWFnZUtleTtcbiAgICAgIH1cbiAgICAgIGlmICh0eXBlb2Ygb3B0aW9ucy51c2VMYWJlbHMgIT09ICd1bmRlZmluZWQnKSB7XG4gICAgICAgIHRoaXMudXNlTGFiZWxzID0gb3B0aW9ucy51c2VMYWJlbHM7XG4gICAgICB9XG4gICAgICBpZiAodHlwZW9mIG9wdGlvbnMuYWxsb3dPbmx5VmFsaWRWYWx1ZXMgIT09ICd1bmRlZmluZWQnKSB7XG4gICAgICAgIHRoaXMuYWxsb3dPbmx5VmFsaWRWYWx1ZXMgPSBvcHRpb25zLmFsbG93T25seVZhbGlkVmFsdWVzO1xuICAgICAgfVxuICAgICAgaWYgKHR5cGVvZiBvcHRpb25zLmZpbHRlcmFibGUgIT09ICd1bmRlZmluZWQnKSB7XG4gICAgICAgIHRoaXMuZmlsdGVyYWJsZSA9IG9wdGlvbnMuZmlsdGVyYWJsZTtcbiAgICAgIH1cbiAgICAgIGlmICh0eXBlb2Ygb3B0aW9ucy51c2VQbGFjZWhvbGRlckFzSXRlbSAhPT0gJ3VuZGVmaW5lZCcpIHtcbiAgICAgICAgdGhpcy51c2VQbGFjZWhvbGRlckFzSXRlbSA9IG9wdGlvbnMudXNlUGxhY2Vob2xkZXJBc0l0ZW07XG4gICAgICB9XG4gICAgfVxuICB9XG59XG4iXX0=