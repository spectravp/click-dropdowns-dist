import { Injectable } from '@angular/core';
import { Subject, fromEvent } from 'rxjs';
import * as i0 from "@angular/core";
export class DropdownService {
    /**
     * Constructor, init the observables
     */
    constructor() {
        this.closeAllObserver = new Subject();
        this.documentClickObserver = fromEvent(document, 'click');
    }
    /**
     * Gets the document click observer
     */
    getDocumentClickObserver() {
        return this.documentClickObserver;
    }
    /**
     * Gets the close all dropdowns observer
     */
    getCloseAllDropdownsObservable() {
        return this.closeAllObserver.asObservable();
    }
    /**
     * Will broadcast a close all to all observables
     */
    closeAllDropdowns() {
        this.closeAllObserver.next(new Date());
    }
}
DropdownService.ɵfac = function DropdownService_Factory(t) { return new (t || DropdownService)(); };
DropdownService.ɵprov = /*@__PURE__*/ i0.ɵɵdefineInjectable({ token: DropdownService, factory: DropdownService.ɵfac, providedIn: 'root' });
(function () { (typeof ngDevMode === "undefined" || ngDevMode) && i0.ɵsetClassMetadata(DropdownService, [{
        type: Injectable,
        args: [{
                providedIn: 'root'
            }]
    }], function () { return []; }, null); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZHJvcGRvd24uc2VydmljZS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uL3Byb2plY3RzL2Ryb3Bkb3ducy9zcmMvbGliL3NlcnZpY2VzL2Ryb3Bkb3duLnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUMzQyxPQUFPLEVBQWEsT0FBTyxFQUFFLFNBQVMsRUFBQyxNQUFNLE1BQU0sQ0FBQzs7QUFLcEQsTUFBTSxPQUFPLGVBQWU7SUFZMUI7O09BRUc7SUFDSDtRQUNFLElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxJQUFJLE9BQU8sRUFBRSxDQUFDO1FBQ3RDLElBQUksQ0FBQyxxQkFBcUIsR0FBRyxTQUFTLENBQUMsUUFBUSxFQUFFLE9BQU8sQ0FBQyxDQUFDO0lBQzVELENBQUM7SUFFRDs7T0FFRztJQUNILHdCQUF3QjtRQUN0QixPQUFPLElBQUksQ0FBQyxxQkFBcUIsQ0FBQztJQUNwQyxDQUFDO0lBRUQ7O09BRUc7SUFDSCw4QkFBOEI7UUFDNUIsT0FBTyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsWUFBWSxFQUFFLENBQUM7SUFDOUMsQ0FBQztJQUVEOztPQUVHO0lBQ0gsaUJBQWlCO1FBQ2YsSUFBSSxDQUFDLGdCQUFnQixDQUFDLElBQUksQ0FBQyxJQUFJLElBQUksRUFBRSxDQUFDLENBQUM7SUFDekMsQ0FBQzs7OEVBdkNVLGVBQWU7cUVBQWYsZUFBZSxXQUFmLGVBQWUsbUJBRmQsTUFBTTt1RkFFUCxlQUFlO2NBSDNCLFVBQVU7ZUFBQztnQkFDVixVQUFVLEVBQUUsTUFBTTthQUNuQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEluamVjdGFibGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7T2JzZXJ2YWJsZSwgU3ViamVjdCwgZnJvbUV2ZW50fSBmcm9tICdyeGpzJztcblxuQEluamVjdGFibGUoe1xuICBwcm92aWRlZEluOiAncm9vdCdcbn0pXG5leHBvcnQgY2xhc3MgRHJvcGRvd25TZXJ2aWNlIHtcblxuICAvKipcbiAgICogQSBzaW5ndWxhciBzdWJzY3JpcHRpb24gZm9yIGNsb3NpbmcgYWxsIGRyb3Bkb3ducyBpbiB0aGUgYXBwXG4gICAqL1xuICBwcml2YXRlIGNsb3NlQWxsT2JzZXJ2ZXI6IFN1YmplY3Q8YW55PjtcblxuICAvKipcbiAgICogQSBzaW5ndWFyIHN1YnNjcmlwdGlvbiBmb3IgZG9jdW1lbnQgY2xpY2tzXG4gICAqL1xuICBwcml2YXRlIGRvY3VtZW50Q2xpY2tPYnNlcnZlcjogT2JzZXJ2YWJsZTxhbnk+O1xuXG4gIC8qKlxuICAgKiBDb25zdHJ1Y3RvciwgaW5pdCB0aGUgb2JzZXJ2YWJsZXNcbiAgICovXG4gIGNvbnN0cnVjdG9yKCkge1xuICAgIHRoaXMuY2xvc2VBbGxPYnNlcnZlciA9IG5ldyBTdWJqZWN0KCk7XG4gICAgdGhpcy5kb2N1bWVudENsaWNrT2JzZXJ2ZXIgPSBmcm9tRXZlbnQoZG9jdW1lbnQsICdjbGljaycpO1xuICB9XG5cbiAgLyoqXG4gICAqIEdldHMgdGhlIGRvY3VtZW50IGNsaWNrIG9ic2VydmVyXG4gICAqL1xuICBnZXREb2N1bWVudENsaWNrT2JzZXJ2ZXIoKTogT2JzZXJ2YWJsZTxhbnk+IHtcbiAgICByZXR1cm4gdGhpcy5kb2N1bWVudENsaWNrT2JzZXJ2ZXI7XG4gIH1cblxuICAvKipcbiAgICogR2V0cyB0aGUgY2xvc2UgYWxsIGRyb3Bkb3ducyBvYnNlcnZlclxuICAgKi9cbiAgZ2V0Q2xvc2VBbGxEcm9wZG93bnNPYnNlcnZhYmxlKCk6IE9ic2VydmFibGU8YW55PiB7XG4gICAgcmV0dXJuIHRoaXMuY2xvc2VBbGxPYnNlcnZlci5hc09ic2VydmFibGUoKTtcbiAgfVxuXG4gIC8qKlxuICAgKiBXaWxsIGJyb2FkY2FzdCBhIGNsb3NlIGFsbCB0byBhbGwgb2JzZXJ2YWJsZXNcbiAgICovXG4gIGNsb3NlQWxsRHJvcGRvd25zKCkge1xuICAgIHRoaXMuY2xvc2VBbGxPYnNlcnZlci5uZXh0KG5ldyBEYXRlKCkpO1xuICB9XG59XG4iXX0=