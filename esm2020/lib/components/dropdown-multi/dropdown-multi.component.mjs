import { Component } from '@angular/core';
import { DropdownComponent } from '../dropdown/dropdown.component';
import * as i0 from "@angular/core";
import * as i1 from "../../services/dropdown.service";
import * as i2 from "@angular/forms";
import * as i3 from "@angular/common";
function DropdownMultiComponent_i_2_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelement(0, "i");
} if (rf & 2) {
    const ctx_r0 = i0.ɵɵnextContext();
    i0.ɵɵclassMap(ctx_r0.options.caretClass);
} }
function DropdownMultiComponent_3_ng_template_0_Template(rf, ctx) { }
function DropdownMultiComponent_3_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵtemplate(0, DropdownMultiComponent_3_ng_template_0_Template, 0, 0, "ng-template", 10);
} if (rf & 2) {
    const ctx_r1 = i0.ɵɵnextContext();
    i0.ɵɵproperty("ngTemplateOutlet", ctx_r1.placeholderCaratTemplate);
} }
function DropdownMultiComponent_span_4_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "span", 11);
    i0.ɵɵtext(1);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const ctx_r2 = i0.ɵɵnextContext();
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate(ctx_r2.options.placeholder);
} }
function DropdownMultiComponent_span_5_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "span", 12);
    i0.ɵɵelement(1, "img", 13);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const ctx_r3 = i0.ɵɵnextContext();
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("src", ctx_r3.getSelectedImage(), i0.ɵɵsanitizeUrl);
} }
function DropdownMultiComponent_span_6_span_1_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "span");
    i0.ɵɵtext(1);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const ctx_r10 = i0.ɵɵnextContext(2);
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate(ctx_r10.getSelectedLabel());
} }
function DropdownMultiComponent_span_6_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "span", 14);
    i0.ɵɵtemplate(1, DropdownMultiComponent_span_6_span_1_Template, 2, 1, "span", 3);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const ctx_r4 = i0.ɵɵnextContext();
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngIf", ctx_r4.isSelected());
} }
function DropdownMultiComponent_i_7_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelement(0, "i");
} if (rf & 2) {
    const ctx_r5 = i0.ɵɵnextContext();
    i0.ɵɵclassMap(ctx_r5.options.caretClass);
} }
function DropdownMultiComponent_8_ng_template_0_Template(rf, ctx) { }
function DropdownMultiComponent_8_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵtemplate(0, DropdownMultiComponent_8_ng_template_0_Template, 0, 0, "ng-template", 10);
} if (rf & 2) {
    const ctx_r6 = i0.ɵɵnextContext();
    i0.ɵɵproperty("ngTemplateOutlet", ctx_r6.placeholderCaratTemplate);
} }
function DropdownMultiComponent_div_10_Template(rf, ctx) { if (rf & 1) {
    const _r13 = i0.ɵɵgetCurrentView();
    i0.ɵɵelementStart(0, "div", 15)(1, "input", 16);
    i0.ɵɵlistener("ngModelChange", function DropdownMultiComponent_div_10_Template_input_ngModelChange_1_listener($event) { i0.ɵɵrestoreView(_r13); const ctx_r12 = i0.ɵɵnextContext(); return i0.ɵɵresetView(ctx_r12.filterModel = $event); })("ngModelChange", function DropdownMultiComponent_div_10_Template_input_ngModelChange_1_listener() { i0.ɵɵrestoreView(_r13); const ctx_r14 = i0.ɵɵnextContext(); return i0.ɵɵresetView(ctx_r14.onFilterChange()); });
    i0.ɵɵelementEnd()();
} if (rf & 2) {
    const ctx_r7 = i0.ɵɵnextContext();
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngModel", ctx_r7.filterModel);
} }
function DropdownMultiComponent_div_11_span_1_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "span", 12)(1, "span");
    i0.ɵɵelement(2, "img", 13);
    i0.ɵɵelementEnd()();
} if (rf & 2) {
    const option_r15 = i0.ɵɵnextContext().$implicit;
    const ctx_r16 = i0.ɵɵnextContext();
    i0.ɵɵadvance(2);
    i0.ɵɵproperty("src", option_r15[ctx_r16.options.imageKey], i0.ɵɵsanitizeUrl);
} }
function DropdownMultiComponent_div_11_span_2_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "span", 14)(1, "span");
    i0.ɵɵtext(2);
    i0.ɵɵelementEnd()();
} if (rf & 2) {
    const option_r15 = i0.ɵɵnextContext().$implicit;
    const ctx_r17 = i0.ɵɵnextContext();
    i0.ɵɵadvance(2);
    i0.ɵɵtextInterpolate(option_r15[ctx_r17.options.label]);
} }
function DropdownMultiComponent_div_11_i_4_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelement(0, "i");
} if (rf & 2) {
    const ctx_r18 = i0.ɵɵnextContext(2);
    i0.ɵɵclassMap(ctx_r18.options.selectedItemClass);
} }
function DropdownMultiComponent_div_11_5_ng_template_0_Template(rf, ctx) { }
function DropdownMultiComponent_div_11_5_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵtemplate(0, DropdownMultiComponent_div_11_5_ng_template_0_Template, 0, 0, "ng-template", 10);
} if (rf & 2) {
    const ctx_r19 = i0.ɵɵnextContext(2);
    i0.ɵɵproperty("ngTemplateOutlet", ctx_r19.selectedCaratTemplate);
} }
const _c0 = function (a0) { return { selected: a0 }; };
function DropdownMultiComponent_div_11_Template(rf, ctx) { if (rf & 1) {
    const _r24 = i0.ɵɵgetCurrentView();
    i0.ɵɵelementStart(0, "div", 17);
    i0.ɵɵlistener("click", function DropdownMultiComponent_div_11_Template_div_click_0_listener($event) { const restoredCtx = i0.ɵɵrestoreView(_r24); const option_r15 = restoredCtx.$implicit; const ctx_r23 = i0.ɵɵnextContext(); return i0.ɵɵresetView(ctx_r23.selectItem($event, option_r15)); });
    i0.ɵɵtemplate(1, DropdownMultiComponent_div_11_span_1_Template, 3, 1, "span", 5);
    i0.ɵɵtemplate(2, DropdownMultiComponent_div_11_span_2_Template, 3, 1, "span", 6);
    i0.ɵɵelementStart(3, "span", 18);
    i0.ɵɵtemplate(4, DropdownMultiComponent_div_11_i_4_Template, 1, 3, "i", 2);
    i0.ɵɵtemplate(5, DropdownMultiComponent_div_11_5_Template, 1, 1, null, 3);
    i0.ɵɵelementEnd()();
} if (rf & 2) {
    const option_r15 = ctx.$implicit;
    const ctx_r8 = i0.ɵɵnextContext();
    i0.ɵɵproperty("ngClass", i0.ɵɵpureFunction1(5, _c0, ctx_r8.isItemSelected(option_r15)));
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngIf", ctx_r8.options.useImages);
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngIf", ctx_r8.options.useLabels);
    i0.ɵɵadvance(2);
    i0.ɵɵproperty("ngIf", !ctx_r8.selectedCaratTemplate);
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngIf", ctx_r8.selectedCaratTemplate && ctx_r8.isItemSelected(option_r15));
} }
const _c1 = function (a0, a1) { return { open: a0, animated: a1 }; };
export class DropdownMultiComponent extends DropdownComponent {
    /**
     * Constructor
     */
    constructor(dropdownService) {
        super(dropdownService);
        this.dropdownService = dropdownService;
        this.multiModel = [];
        this.multiModelLabels = [];
    }
    /**
     * Selects an item
     */
    selectItem($event, item) {
        $event.stopImmediatePropagation();
        $event.stopPropagation();
        if (this.options.closeOnSelection) {
            this.isOpen = false;
        }
        if (this.multiModel.indexOf(item[this.options.identifier]) >= 0) {
            this.multiModel.splice(this.multiModel.indexOf(item[this.options.identifier]), 1);
            this.multiModelLabels.splice(this.multiModel.indexOf(item[this.options.identifier]), 1);
            this.setElementValue(this.multiModel);
            return false;
        }
        this.multiModel.push(item[this.options.identifier]);
        this.multiModelLabels.push(item[this.options.label]);
        this.setElementValue(this.multiModel);
        return false;
    }
    isValidSelection(item) {
        return true;
    }
    clearSelection() { }
    /**
     * Tests if an item is the selected item
     */
    isItemSelected(item) {
        return this.multiModel.indexOf(item[this.options.identifier]) >= 0;
    }
    /**
     * Tests if the dropdown has a selection
     */
    isSelected() {
        return this.multiModel.length > 0;
    }
    /**
     * Gets the label of the selected item
     */
    getSelectedLabel() {
        if (this.options.selectionsAsLabel === false) {
            return this.options.placeholder;
        }
        else if (this.options.selectionsAsLabel === true) {
            return this.multiModelLabels.join(', ');
        }
        return this.options.selectionsAsLabel;
    }
    /**
     * Gets the label of the selected item
     */
    getSelectedImage() {
        return null;
    }
}
DropdownMultiComponent.ɵfac = function DropdownMultiComponent_Factory(t) { return new (t || DropdownMultiComponent)(i0.ɵɵdirectiveInject(i1.DropdownService)); };
DropdownMultiComponent.ɵcmp = /*@__PURE__*/ i0.ɵɵdefineComponent({ type: DropdownMultiComponent, selectors: [["click-dropdown-multi"]], features: [i0.ɵɵInheritDefinitionFeature], decls: 12, vars: 13, consts: [[1, "dropdown", 3, "ngClass"], [1, "dropdown-display", 3, "click"], [3, "class", 4, "ngIf"], [4, "ngIf"], ["class", "placeholder", 4, "ngIf"], ["class", "image", 4, "ngIf"], ["class", "label", 4, "ngIf"], [1, "dropdown-list"], ["class", "dropdown-list-filter", 4, "ngIf"], ["class", "dropdown-list-item", 3, "ngClass", "click", 4, "ngFor", "ngForOf"], [3, "ngTemplateOutlet"], [1, "placeholder"], [1, "image"], [3, "src"], [1, "label"], [1, "dropdown-list-filter"], [3, "ngModel", "ngModelChange"], [1, "dropdown-list-item", 3, "ngClass", "click"], [1, "selected-item-indicator"]], template: function DropdownMultiComponent_Template(rf, ctx) { if (rf & 1) {
        i0.ɵɵelementStart(0, "div", 0)(1, "div", 1);
        i0.ɵɵlistener("click", function DropdownMultiComponent_Template_div_click_1_listener($event) { return ctx.toggle($event); });
        i0.ɵɵtemplate(2, DropdownMultiComponent_i_2_Template, 1, 3, "i", 2);
        i0.ɵɵtemplate(3, DropdownMultiComponent_3_Template, 1, 1, null, 3);
        i0.ɵɵtemplate(4, DropdownMultiComponent_span_4_Template, 2, 1, "span", 4);
        i0.ɵɵtemplate(5, DropdownMultiComponent_span_5_Template, 2, 1, "span", 5);
        i0.ɵɵtemplate(6, DropdownMultiComponent_span_6_Template, 2, 1, "span", 6);
        i0.ɵɵtemplate(7, DropdownMultiComponent_i_7_Template, 1, 3, "i", 2);
        i0.ɵɵtemplate(8, DropdownMultiComponent_8_Template, 1, 1, null, 3);
        i0.ɵɵelementEnd();
        i0.ɵɵelementStart(9, "div", 7);
        i0.ɵɵtemplate(10, DropdownMultiComponent_div_10_Template, 2, 1, "div", 8);
        i0.ɵɵtemplate(11, DropdownMultiComponent_div_11_Template, 6, 7, "div", 9);
        i0.ɵɵelementEnd()();
    } if (rf & 2) {
        i0.ɵɵproperty("ngClass", i0.ɵɵpureFunction2(10, _c1, ctx.isOpen, ctx.options.animated));
        i0.ɵɵadvance(2);
        i0.ɵɵproperty("ngIf", ctx.options.displayPlaceholderCaretBefore && !ctx.placeholderCaratTemplate);
        i0.ɵɵadvance(1);
        i0.ɵɵproperty("ngIf", ctx.options.displayPlaceholderCaretBefore && ctx.placeholderCaratTemplate);
        i0.ɵɵadvance(1);
        i0.ɵɵproperty("ngIf", !ctx.isSelected());
        i0.ɵɵadvance(1);
        i0.ɵɵproperty("ngIf", ctx.isSelected() && ctx.getSelectedImage());
        i0.ɵɵadvance(1);
        i0.ɵɵproperty("ngIf", ctx.options.useLabels);
        i0.ɵɵadvance(1);
        i0.ɵɵproperty("ngIf", ctx.options.displayPlaceholderCaretAfter && !ctx.placeholderCaratTemplate);
        i0.ɵɵadvance(1);
        i0.ɵɵproperty("ngIf", ctx.options.displayPlaceholderCaretAfter && ctx.placeholderCaratTemplate);
        i0.ɵɵadvance(2);
        i0.ɵɵproperty("ngIf", ctx.options.filterable);
        i0.ɵɵadvance(1);
        i0.ɵɵproperty("ngForOf", ctx.options.items);
    } }, dependencies: [i2.DefaultValueAccessor, i2.NgControlStatus, i2.NgModel, i3.NgClass, i3.NgForOf, i3.NgIf, i3.NgTemplateOutlet], styles: ["[_nghost-%COMP%]   .pull-left[_ngcontent-%COMP%]{float:left}[_nghost-%COMP%]   .pull-right[_ngcontent-%COMP%]{float:right}[_nghost-%COMP%]   .relative[_ngcontent-%COMP%]{position:relative}[_nghost-%COMP%]   .align-left[_ngcontent-%COMP%]{text-align:left}@media (max-width: 1199px){[_nghost-%COMP%]   .align-lg-left[_ngcontent-%COMP%]{text-align:left}}@media (max-width: 991px){[_nghost-%COMP%]   .align-md-left[_ngcontent-%COMP%]{text-align:left}}@media (max-width: 767px){[_nghost-%COMP%]   .align-sm-left[_ngcontent-%COMP%]{text-align:left}}@media (max-width: 543px){[_nghost-%COMP%]   .align-xs-left[_ngcontent-%COMP%]{text-align:left}}[_nghost-%COMP%]   .align-center[_ngcontent-%COMP%]{text-align:center}@media (max-width: 1199px){[_nghost-%COMP%]   .align-lg-center[_ngcontent-%COMP%]{text-align:center}}@media (max-width: 991px){[_nghost-%COMP%]   .align-md-center[_ngcontent-%COMP%]{text-align:center}}@media (max-width: 767px){[_nghost-%COMP%]   .align-sm-center[_ngcontent-%COMP%]{text-align:center}}@media (max-width: 543px){[_nghost-%COMP%]   .align-xs-center[_ngcontent-%COMP%]{text-align:center}}[_nghost-%COMP%]   .align-right[_ngcontent-%COMP%]{text-align:right}@media (max-width: 1199px){[_nghost-%COMP%]   .align-lg-right[_ngcontent-%COMP%]{text-align:right}}@media (max-width: 991px){[_nghost-%COMP%]   .align-md-right[_ngcontent-%COMP%]{text-align:right}}@media (max-width: 767px){[_nghost-%COMP%]   .align-sm-right[_ngcontent-%COMP%]{text-align:right}}@media (max-width: 543px){[_nghost-%COMP%]   .align-xs-right[_ngcontent-%COMP%]{text-align:right}}[_nghost-%COMP%]   .dropdown[_ngcontent-%COMP%]{position:relative;line-height:100%}[_nghost-%COMP%]   .dropdown[_ngcontent-%COMP%]   *[_ngcontent-%COMP%]{box-sizing:border-box}[_nghost-%COMP%]   .dropdown[_ngcontent-%COMP%]   .dropdown-display[_ngcontent-%COMP%]{position:relative;padding:1rem;border:.0625rem solid black;cursor:pointer}[_nghost-%COMP%]   .dropdown[_ngcontent-%COMP%]   .dropdown-display[_ngcontent-%COMP%]   i[_ngcontent-%COMP%], [_nghost-%COMP%]   .dropdown[_ngcontent-%COMP%]   .dropdown-display[_ngcontent-%COMP%]   fa-icon[_ngcontent-%COMP%]{position:absolute;top:50%;transform:translateY(-50%);right:1rem}[_nghost-%COMP%]   .dropdown[_ngcontent-%COMP%]   .dropdown-display[_ngcontent-%COMP%]   span[_ngcontent-%COMP%]{line-height:100%}[_nghost-%COMP%]   .dropdown[_ngcontent-%COMP%]   img[_ngcontent-%COMP%]{max-width:50px;max-height:50px}[_nghost-%COMP%]   .dropdown[_ngcontent-%COMP%]   .dropdown-list[_ngcontent-%COMP%]{max-height:0;overflow:hidden;position:absolute;width:100%;background:white;z-index:2;left:0;transition:max-height .2s linear;transition-delay:.15s;animation-delay:.15s;backface-visibility:hidden;-webkit-transition:max-height .2s linear;-webkit-transition-delay:.15s;-webkit-animation-delay:.15s;-webkit-backface-visibility:hidden}[_nghost-%COMP%]   .dropdown[_ngcontent-%COMP%]   .dropdown-list[_ngcontent-%COMP%]   .dropdown-list-item[_ngcontent-%COMP%]{border-left:.0625rem solid black;border-bottom:.0625rem solid black;border-right:.0625rem solid black;padding:.875rem;font-size:.875rem;position:relative;transition:all .3s linear;transition-delay:0s;animation-delay:0s;backface-visibility:hidden;-webkit-transition:all .3s linear;-webkit-transition-delay:0s;-webkit-animation-delay:0s;-webkit-backface-visibility:hidden;cursor:pointer}[_nghost-%COMP%]   .dropdown[_ngcontent-%COMP%]   .dropdown-list[_ngcontent-%COMP%]   .dropdown-list-item[_ngcontent-%COMP%]   i[_ngcontent-%COMP%], [_nghost-%COMP%]   .dropdown[_ngcontent-%COMP%]   .dropdown-list[_ngcontent-%COMP%]   .dropdown-list-item[_ngcontent-%COMP%]   fa-icon[_ngcontent-%COMP%]{color:#000;opacity:0;position:absolute;top:50%;transform:translateY(-50%);transition:all .15s linear;transition-delay:0s;animation-delay:0s;backface-visibility:hidden;-webkit-transition:all .15s linear;-webkit-transition-delay:0s;-webkit-animation-delay:0s;-webkit-backface-visibility:hidden;right:1rem}[_nghost-%COMP%]   .dropdown[_ngcontent-%COMP%]   .dropdown-list[_ngcontent-%COMP%]   .dropdown-list-item[_ngcontent-%COMP%]   img[_ngcontent-%COMP%]{vertical-align:middle;margin:0 .5rem 0 0}[_nghost-%COMP%]   .dropdown[_ngcontent-%COMP%]   .dropdown-list[_ngcontent-%COMP%]   .dropdown-list-item.selected[_ngcontent-%COMP%], [_nghost-%COMP%]   .dropdown[_ngcontent-%COMP%]   .dropdown-list[_ngcontent-%COMP%]   .dropdown-list-item[_ngcontent-%COMP%]:hover{background:#f2f2f2}[_nghost-%COMP%]   .dropdown[_ngcontent-%COMP%]   .dropdown-list[_ngcontent-%COMP%]   .dropdown-list-item.selected[_ngcontent-%COMP%]   i[_ngcontent-%COMP%], [_nghost-%COMP%]   .dropdown[_ngcontent-%COMP%]   .dropdown-list[_ngcontent-%COMP%]   .dropdown-list-item.selected[_ngcontent-%COMP%]   fa-icon[_ngcontent-%COMP%]{opacity:1}[_nghost-%COMP%]   .dropdown.open[_ngcontent-%COMP%] > .dropdown-list[_ngcontent-%COMP%]{max-height:800px}"] });
(function () { (typeof ngDevMode === "undefined" || ngDevMode) && i0.ɵsetClassMetadata(DropdownMultiComponent, [{
        type: Component,
        args: [{ selector: 'click-dropdown-multi', template: "<div class=\"dropdown\" [ngClass]=\"{open: isOpen, animated: options.animated}\">\n  <div class=\"dropdown-display\" (click)=\"toggle($event)\">\n    <i class=\"{{options.caretClass}}\" *ngIf=\"options.displayPlaceholderCaretBefore && !placeholderCaratTemplate\"></i>\n    <ng-template *ngIf=\"options.displayPlaceholderCaretBefore && placeholderCaratTemplate\" [ngTemplateOutlet]=\"placeholderCaratTemplate\"></ng-template>\n    <span *ngIf=\"! isSelected()\" class=\"placeholder\">{{ options.placeholder }}</span>\n    <span class=\"image\" *ngIf=\"isSelected() && getSelectedImage()\">\n      <img [src]=\"getSelectedImage()\"/>\n    </span>\n    <span class=\"label\" *ngIf=\"options.useLabels\">\n      <span *ngIf=\"isSelected()\">{{ getSelectedLabel() }}</span>\n    </span>\n    <i class=\"{{options.caretClass}}\" *ngIf=\"options.displayPlaceholderCaretAfter && !placeholderCaratTemplate\"></i>\n    <ng-template *ngIf=\"options.displayPlaceholderCaretAfter && placeholderCaratTemplate\" [ngTemplateOutlet]=\"placeholderCaratTemplate\"></ng-template>\n  </div>\n  <div class=\"dropdown-list\">\n    <div class=\"dropdown-list-filter\" *ngIf=\"options.filterable\">\n      <input [(ngModel)]=\"filterModel\" (ngModelChange)=\"onFilterChange()\"/>\n    </div>\n    <div class=\"dropdown-list-item\" [ngClass]=\"{selected:isItemSelected(option)}\" *ngFor=\"let option of options.items\" (click)=\"selectItem($event, option)\">\n      <span class=\"image\" *ngIf=\"options.useImages\">\n         <span><img [src]=\"option[options.imageKey]\"/></span>\n       </span>\n      <span class=\"label\" *ngIf=\"options.useLabels\">\n        <span>{{option[options.label]}}</span>\n      </span>\n       <span class=\"selected-item-indicator\">\n         <i class=\"{{options.selectedItemClass}}\" *ngIf=\"!selectedCaratTemplate\"></i>\n         <ng-template *ngIf=\"selectedCaratTemplate && isItemSelected(option)\" [ngTemplateOutlet]=\"selectedCaratTemplate\"></ng-template>\n     </span>\n    </div>\n  </div>\n</div>\n", styles: [":host .pull-left{float:left}:host .pull-right{float:right}:host .relative{position:relative}:host .align-left{text-align:left}@media (max-width: 1199px){:host .align-lg-left{text-align:left}}@media (max-width: 991px){:host .align-md-left{text-align:left}}@media (max-width: 767px){:host .align-sm-left{text-align:left}}@media (max-width: 543px){:host .align-xs-left{text-align:left}}:host .align-center{text-align:center}@media (max-width: 1199px){:host .align-lg-center{text-align:center}}@media (max-width: 991px){:host .align-md-center{text-align:center}}@media (max-width: 767px){:host .align-sm-center{text-align:center}}@media (max-width: 543px){:host .align-xs-center{text-align:center}}:host .align-right{text-align:right}@media (max-width: 1199px){:host .align-lg-right{text-align:right}}@media (max-width: 991px){:host .align-md-right{text-align:right}}@media (max-width: 767px){:host .align-sm-right{text-align:right}}@media (max-width: 543px){:host .align-xs-right{text-align:right}}:host .dropdown{position:relative;line-height:100%}:host .dropdown *{box-sizing:border-box}:host .dropdown .dropdown-display{position:relative;padding:1rem;border:.0625rem solid black;cursor:pointer}:host .dropdown .dropdown-display i,:host .dropdown .dropdown-display fa-icon{position:absolute;top:50%;transform:translateY(-50%);right:1rem}:host .dropdown .dropdown-display span{line-height:100%}:host .dropdown img{max-width:50px;max-height:50px}:host .dropdown .dropdown-list{max-height:0;overflow:hidden;position:absolute;width:100%;background:white;z-index:2;left:0;transition:max-height .2s linear;transition-delay:.15s;animation-delay:.15s;backface-visibility:hidden;-webkit-transition:max-height .2s linear;-webkit-transition-delay:.15s;-webkit-animation-delay:.15s;-webkit-backface-visibility:hidden}:host .dropdown .dropdown-list .dropdown-list-item{border-left:.0625rem solid black;border-bottom:.0625rem solid black;border-right:.0625rem solid black;padding:.875rem;font-size:.875rem;position:relative;transition:all .3s linear;transition-delay:0s;animation-delay:0s;backface-visibility:hidden;-webkit-transition:all .3s linear;-webkit-transition-delay:0s;-webkit-animation-delay:0s;-webkit-backface-visibility:hidden;cursor:pointer}:host .dropdown .dropdown-list .dropdown-list-item i,:host .dropdown .dropdown-list .dropdown-list-item fa-icon{color:#000;opacity:0;position:absolute;top:50%;transform:translateY(-50%);transition:all .15s linear;transition-delay:0s;animation-delay:0s;backface-visibility:hidden;-webkit-transition:all .15s linear;-webkit-transition-delay:0s;-webkit-animation-delay:0s;-webkit-backface-visibility:hidden;right:1rem}:host .dropdown .dropdown-list .dropdown-list-item img{vertical-align:middle;margin:0 .5rem 0 0}:host .dropdown .dropdown-list .dropdown-list-item.selected,:host .dropdown .dropdown-list .dropdown-list-item:hover{background:#f2f2f2}:host .dropdown .dropdown-list .dropdown-list-item.selected i,:host .dropdown .dropdown-list .dropdown-list-item.selected fa-icon{opacity:1}:host .dropdown.open>.dropdown-list{max-height:800px}\n"] }]
    }], function () { return [{ type: i1.DropdownService }]; }, null); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZHJvcGRvd24tbXVsdGkuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vLi4vLi4vLi4vLi4vcHJvamVjdHMvZHJvcGRvd25zL3NyYy9saWIvY29tcG9uZW50cy9kcm9wZG93bi1tdWx0aS9kcm9wZG93bi1tdWx0aS5jb21wb25lbnQudHMiLCIuLi8uLi8uLi8uLi8uLi8uLi9wcm9qZWN0cy9kcm9wZG93bnMvc3JjL2xpYi9jb21wb25lbnRzL2Ryb3Bkb3duL2Ryb3Bkb3duLmNvbXBvbmVudC5odG1sIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDMUMsT0FBTyxFQUFDLGlCQUFpQixFQUFDLE1BQU0sZ0NBQWdDLENBQUM7Ozs7OztJQ0M3RCxvQkFBaUg7OztJQUE5Ryx3Q0FBOEI7Ozs7SUFDakMsMEZBQW1KOzs7SUFBNUQsa0VBQTZDOzs7SUFDcEksZ0NBQWlEO0lBQUEsWUFBeUI7SUFBQSxpQkFBTzs7O0lBQWhDLGVBQXlCO0lBQXpCLGdEQUF5Qjs7O0lBQzFFLGdDQUErRDtJQUM3RCwwQkFBaUM7SUFDbkMsaUJBQU87OztJQURBLGVBQTBCO0lBQTFCLGlFQUEwQjs7O0lBRy9CLDRCQUEyQjtJQUFBLFlBQXdCO0lBQUEsaUJBQU87OztJQUEvQixlQUF3QjtJQUF4QixnREFBd0I7OztJQURyRCxnQ0FBOEM7SUFDNUMsZ0ZBQTBEO0lBQzVELGlCQUFPOzs7SUFERSxlQUFrQjtJQUFsQiwwQ0FBa0I7OztJQUUzQixvQkFBZ0g7OztJQUE3Ryx3Q0FBOEI7Ozs7SUFDakMsMEZBQWtKOzs7SUFBNUQsa0VBQTZDOzs7O0lBR25JLCtCQUE2RCxnQkFBQTtJQUNwRCwyT0FBeUIsd0tBQWtCLGVBQUEsd0JBQWdCLENBQUEsSUFBbEM7SUFBaEMsaUJBQXFFLEVBQUE7OztJQUE5RCxlQUF5QjtJQUF6Qiw0Q0FBeUI7OztJQUdoQyxnQ0FBOEMsV0FBQTtJQUNyQywwQkFBdUM7SUFBQSxpQkFBTyxFQUFBOzs7O0lBQXpDLGVBQWdDO0lBQWhDLDRFQUFnQzs7O0lBRTlDLGdDQUE4QyxXQUFBO0lBQ3RDLFlBQXlCO0lBQUEsaUJBQU8sRUFBQTs7OztJQUFoQyxlQUF5QjtJQUF6Qix1REFBeUI7OztJQUc5QixvQkFBNEU7OztJQUF6RSxnREFBcUM7Ozs7SUFDeEMsaUdBQThIOzs7SUFBekQsZ0VBQTBDOzs7OztJQVRwSCwrQkFBd0o7SUFBckMsdU9BQVMsZUFBQSxzQ0FBMEIsQ0FBQSxJQUFDO0lBQ3JKLGdGQUVRO0lBQ1IsZ0ZBRU87SUFDTixnQ0FBc0M7SUFDcEMsMEVBQTRFO0lBQzVFLHlFQUE4SDtJQUNsSSxpQkFBTyxFQUFBOzs7O0lBVndCLHVGQUE2QztJQUN0RCxlQUF1QjtJQUF2QiwrQ0FBdUI7SUFHdkIsZUFBdUI7SUFBdkIsK0NBQXVCO0lBSUMsZUFBNEI7SUFBNUIsb0RBQTRCO0lBQ3hELGVBQXFEO0lBQXJELHdGQUFxRDs7O0FEbEI1RSxNQUFNLE9BQU8sc0JBQXVCLFNBQVEsaUJBQWlCO0lBWTNEOztPQUVHO0lBQ0gsWUFBc0IsZUFBZ0M7UUFDcEQsS0FBSyxDQUFDLGVBQWUsQ0FBQyxDQUFDO1FBREgsb0JBQWUsR0FBZixlQUFlLENBQWlCO1FBRXBELElBQUksQ0FBQyxVQUFVLEdBQUcsRUFBRSxDQUFDO1FBQ3JCLElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxFQUFFLENBQUM7SUFDN0IsQ0FBQztJQUVEOztPQUVHO0lBQ0gsVUFBVSxDQUFDLE1BQWEsRUFBRSxJQUFTO1FBQ2pDLE1BQU0sQ0FBQyx3QkFBd0IsRUFBRSxDQUFDO1FBQ2xDLE1BQU0sQ0FBQyxlQUFlLEVBQUUsQ0FBQztRQUV6QixJQUFLLElBQUksQ0FBQyxPQUFPLENBQUMsZ0JBQWdCLEVBQUc7WUFDbkMsSUFBSSxDQUFDLE1BQU0sR0FBRyxLQUFLLENBQUM7U0FDckI7UUFFRCxJQUFJLElBQUksQ0FBQyxVQUFVLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLFVBQVUsQ0FBQyxDQUFDLElBQUksQ0FBQyxFQUFHO1lBQ2hFLElBQUksQ0FBQyxVQUFVLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLFVBQVUsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUM7WUFDbEYsSUFBSSxDQUFDLGdCQUFnQixDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxVQUFVLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDO1lBQ3hGLElBQUksQ0FBQyxlQUFlLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDO1lBQ3RDLE9BQU8sS0FBSyxDQUFDO1NBQ2Q7UUFDRCxJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDO1FBQ3BELElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQztRQUNyRCxJQUFJLENBQUMsZUFBZSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQztRQUN0QyxPQUFPLEtBQUssQ0FBQztJQUNmLENBQUM7SUFFRCxnQkFBZ0IsQ0FBQyxJQUFTO1FBQ3hCLE9BQU8sSUFBSSxDQUFDO0lBQ2QsQ0FBQztJQUVELGNBQWMsS0FBVSxDQUFDO0lBRXpCOztPQUVHO0lBQ0gsY0FBYyxDQUFDLElBQVM7UUFDdEIsT0FBTyxJQUFJLENBQUMsVUFBVSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxVQUFVLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQztJQUNyRSxDQUFDO0lBRUQ7O09BRUc7SUFDSCxVQUFVO1FBQ1IsT0FBTyxJQUFJLENBQUMsVUFBVSxDQUFDLE1BQU0sR0FBRyxDQUFDLENBQUM7SUFDcEMsQ0FBQztJQUVEOztPQUVHO0lBQ0gsZ0JBQWdCO1FBQ2QsSUFBSSxJQUFJLENBQUMsT0FBTyxDQUFDLGlCQUFpQixLQUFLLEtBQUssRUFBRztZQUM3QyxPQUFPLElBQUksQ0FBQyxPQUFPLENBQUMsV0FBVyxDQUFDO1NBQ2pDO2FBQU0sSUFBSyxJQUFJLENBQUMsT0FBTyxDQUFDLGlCQUFpQixLQUFLLElBQUksRUFBRztZQUNwRCxPQUFPLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7U0FDekM7UUFFRCxPQUFPLElBQUksQ0FBQyxPQUFPLENBQUMsaUJBQWlCLENBQUM7SUFDeEMsQ0FBQztJQUVEOztPQUVHO0lBQ0gsZ0JBQWdCO1FBQ2QsT0FBTyxJQUFJLENBQUM7SUFDZCxDQUFDOzs0RkFsRlUsc0JBQXNCO3lFQUF0QixzQkFBc0I7UUNUbkMsOEJBQTZFLGFBQUE7UUFDN0Msc0dBQVMsa0JBQWMsSUFBQztRQUNwRCxtRUFBaUg7UUFDakgsa0VBQW1KO1FBQ25KLHlFQUFpRjtRQUNqRix5RUFFTztRQUNQLHlFQUVPO1FBQ1AsbUVBQWdIO1FBQ2hILGtFQUFrSjtRQUNwSixpQkFBTTtRQUNOLDhCQUEyQjtRQUN6Qix5RUFFTTtRQUNOLHlFQVdNO1FBQ1IsaUJBQU0sRUFBQTs7UUE5QmMsdUZBQXNEO1FBRXJDLGVBQXdFO1FBQXhFLGlHQUF3RTtRQUM3RixlQUF1RTtRQUF2RSxnR0FBdUU7UUFDOUUsZUFBb0I7UUFBcEIsd0NBQW9CO1FBQ04sZUFBd0M7UUFBeEMsaUVBQXdDO1FBR3hDLGVBQXVCO1FBQXZCLDRDQUF1QjtRQUdULGVBQXVFO1FBQXZFLGdHQUF1RTtRQUM1RixlQUFzRTtRQUF0RSwrRkFBc0U7UUFHakQsZUFBd0I7UUFBeEIsNkNBQXdCO1FBR3NDLGVBQWdCO1FBQWhCLDJDQUFnQjs7dUZEVHhHLHNCQUFzQjtjQUxsQyxTQUFTOzJCQUNFLHNCQUFzQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHtEcm9wZG93bkNvbXBvbmVudH0gZnJvbSAnLi4vZHJvcGRvd24vZHJvcGRvd24uY29tcG9uZW50JztcbmltcG9ydCB7RHJvcGRvd25TZXJ2aWNlfSBmcm9tICcuLi8uLi9zZXJ2aWNlcy9kcm9wZG93bi5zZXJ2aWNlJztcblxuQENvbXBvbmVudCh7XG4gIHNlbGVjdG9yOiAnY2xpY2stZHJvcGRvd24tbXVsdGknLFxuICB0ZW1wbGF0ZVVybDogJy4uL2Ryb3Bkb3duL2Ryb3Bkb3duLmNvbXBvbmVudC5odG1sJyxcbiAgc3R5bGVVcmxzOiBbJy4vZHJvcGRvd24tbXVsdGkuY29tcG9uZW50LnNjc3MnXVxufSlcbmV4cG9ydCBjbGFzcyBEcm9wZG93bk11bHRpQ29tcG9uZW50IGV4dGVuZHMgRHJvcGRvd25Db21wb25lbnQge1xuXG4gIC8qKlxuICAgKiBUaGUgYWN0dWFsIG1vZGVsIG9mIHNlbGVjdGVkIHZhbHVlc1xuICAgKi9cbiAgcHJvdGVjdGVkIG11bHRpTW9kZWw6IGFueVtdO1xuXG4gIC8qKlxuICAgKiBBIG1vZGVsIG9mIHNlbGVjdGVkIGxhYmVsc1xuICAgKi9cbiAgcHJvdGVjdGVkIG11bHRpTW9kZWxMYWJlbHM6IGFueVtdO1xuXG4gIC8qKlxuICAgKiBDb25zdHJ1Y3RvclxuICAgKi9cbiAgY29uc3RydWN0b3IocHJvdGVjdGVkIGRyb3Bkb3duU2VydmljZTogRHJvcGRvd25TZXJ2aWNlKSB7XG4gICAgc3VwZXIoZHJvcGRvd25TZXJ2aWNlKTtcbiAgICB0aGlzLm11bHRpTW9kZWwgPSBbXTtcbiAgICB0aGlzLm11bHRpTW9kZWxMYWJlbHMgPSBbXTtcbiAgfVxuXG4gIC8qKlxuICAgKiBTZWxlY3RzIGFuIGl0ZW1cbiAgICovXG4gIHNlbGVjdEl0ZW0oJGV2ZW50OiBFdmVudCwgaXRlbTogYW55KTogYm9vbGVhbiB7XG4gICAgJGV2ZW50LnN0b3BJbW1lZGlhdGVQcm9wYWdhdGlvbigpO1xuICAgICRldmVudC5zdG9wUHJvcGFnYXRpb24oKTtcblxuICAgIGlmICggdGhpcy5vcHRpb25zLmNsb3NlT25TZWxlY3Rpb24gKSB7XG4gICAgICB0aGlzLmlzT3BlbiA9IGZhbHNlO1xuICAgIH1cblxuICAgIGlmICh0aGlzLm11bHRpTW9kZWwuaW5kZXhPZihpdGVtW3RoaXMub3B0aW9ucy5pZGVudGlmaWVyXSkgPj0gMCApIHtcbiAgICAgIHRoaXMubXVsdGlNb2RlbC5zcGxpY2UodGhpcy5tdWx0aU1vZGVsLmluZGV4T2YoaXRlbVt0aGlzLm9wdGlvbnMuaWRlbnRpZmllcl0pLCAxKTtcbiAgICAgIHRoaXMubXVsdGlNb2RlbExhYmVscy5zcGxpY2UodGhpcy5tdWx0aU1vZGVsLmluZGV4T2YoaXRlbVt0aGlzLm9wdGlvbnMuaWRlbnRpZmllcl0pLCAxKTtcbiAgICAgIHRoaXMuc2V0RWxlbWVudFZhbHVlKHRoaXMubXVsdGlNb2RlbCk7XG4gICAgICByZXR1cm4gZmFsc2U7XG4gICAgfVxuICAgIHRoaXMubXVsdGlNb2RlbC5wdXNoKGl0ZW1bdGhpcy5vcHRpb25zLmlkZW50aWZpZXJdKTtcbiAgICB0aGlzLm11bHRpTW9kZWxMYWJlbHMucHVzaChpdGVtW3RoaXMub3B0aW9ucy5sYWJlbF0pO1xuICAgIHRoaXMuc2V0RWxlbWVudFZhbHVlKHRoaXMubXVsdGlNb2RlbCk7XG4gICAgcmV0dXJuIGZhbHNlO1xuICB9XG5cbiAgaXNWYWxpZFNlbGVjdGlvbihpdGVtOiBhbnkpOiBib29sZWFuIHtcbiAgICByZXR1cm4gdHJ1ZTtcbiAgfVxuXG4gIGNsZWFyU2VsZWN0aW9uKCk6IHZvaWQge31cblxuICAvKipcbiAgICogVGVzdHMgaWYgYW4gaXRlbSBpcyB0aGUgc2VsZWN0ZWQgaXRlbVxuICAgKi9cbiAgaXNJdGVtU2VsZWN0ZWQoaXRlbTogYW55KTogYm9vbGVhbiB7XG4gICAgcmV0dXJuIHRoaXMubXVsdGlNb2RlbC5pbmRleE9mKGl0ZW1bdGhpcy5vcHRpb25zLmlkZW50aWZpZXJdKSA+PSAwO1xuICB9XG5cbiAgLyoqXG4gICAqIFRlc3RzIGlmIHRoZSBkcm9wZG93biBoYXMgYSBzZWxlY3Rpb25cbiAgICovXG4gIGlzU2VsZWN0ZWQoKTogYm9vbGVhbiB7XG4gICAgcmV0dXJuIHRoaXMubXVsdGlNb2RlbC5sZW5ndGggPiAwO1xuICB9XG5cbiAgLyoqXG4gICAqIEdldHMgdGhlIGxhYmVsIG9mIHRoZSBzZWxlY3RlZCBpdGVtXG4gICAqL1xuICBnZXRTZWxlY3RlZExhYmVsKCk6IGFueSB7XG4gICAgaWYgKHRoaXMub3B0aW9ucy5zZWxlY3Rpb25zQXNMYWJlbCA9PT0gZmFsc2UgKSB7XG4gICAgICByZXR1cm4gdGhpcy5vcHRpb25zLnBsYWNlaG9sZGVyO1xuICAgIH0gZWxzZSBpZiAoIHRoaXMub3B0aW9ucy5zZWxlY3Rpb25zQXNMYWJlbCA9PT0gdHJ1ZSApIHtcbiAgICAgIHJldHVybiB0aGlzLm11bHRpTW9kZWxMYWJlbHMuam9pbignLCAnKTtcbiAgICB9XG5cbiAgICByZXR1cm4gdGhpcy5vcHRpb25zLnNlbGVjdGlvbnNBc0xhYmVsO1xuICB9XG5cbiAgLyoqXG4gICAqIEdldHMgdGhlIGxhYmVsIG9mIHRoZSBzZWxlY3RlZCBpdGVtXG4gICAqL1xuICBnZXRTZWxlY3RlZEltYWdlKCk6IHN0cmluZyB7XG4gICAgcmV0dXJuIG51bGw7XG4gIH1cbn1cbiIsIjxkaXYgY2xhc3M9XCJkcm9wZG93blwiIFtuZ0NsYXNzXT1cIntvcGVuOiBpc09wZW4sIGFuaW1hdGVkOiBvcHRpb25zLmFuaW1hdGVkfVwiPlxuICA8ZGl2IGNsYXNzPVwiZHJvcGRvd24tZGlzcGxheVwiIChjbGljayk9XCJ0b2dnbGUoJGV2ZW50KVwiPlxuICAgIDxpIGNsYXNzPVwie3tvcHRpb25zLmNhcmV0Q2xhc3N9fVwiICpuZ0lmPVwib3B0aW9ucy5kaXNwbGF5UGxhY2Vob2xkZXJDYXJldEJlZm9yZSAmJiAhcGxhY2Vob2xkZXJDYXJhdFRlbXBsYXRlXCI+PC9pPlxuICAgIDxuZy10ZW1wbGF0ZSAqbmdJZj1cIm9wdGlvbnMuZGlzcGxheVBsYWNlaG9sZGVyQ2FyZXRCZWZvcmUgJiYgcGxhY2Vob2xkZXJDYXJhdFRlbXBsYXRlXCIgW25nVGVtcGxhdGVPdXRsZXRdPVwicGxhY2Vob2xkZXJDYXJhdFRlbXBsYXRlXCI+PC9uZy10ZW1wbGF0ZT5cbiAgICA8c3BhbiAqbmdJZj1cIiEgaXNTZWxlY3RlZCgpXCIgY2xhc3M9XCJwbGFjZWhvbGRlclwiPnt7IG9wdGlvbnMucGxhY2Vob2xkZXIgfX08L3NwYW4+XG4gICAgPHNwYW4gY2xhc3M9XCJpbWFnZVwiICpuZ0lmPVwiaXNTZWxlY3RlZCgpICYmIGdldFNlbGVjdGVkSW1hZ2UoKVwiPlxuICAgICAgPGltZyBbc3JjXT1cImdldFNlbGVjdGVkSW1hZ2UoKVwiLz5cbiAgICA8L3NwYW4+XG4gICAgPHNwYW4gY2xhc3M9XCJsYWJlbFwiICpuZ0lmPVwib3B0aW9ucy51c2VMYWJlbHNcIj5cbiAgICAgIDxzcGFuICpuZ0lmPVwiaXNTZWxlY3RlZCgpXCI+e3sgZ2V0U2VsZWN0ZWRMYWJlbCgpIH19PC9zcGFuPlxuICAgIDwvc3Bhbj5cbiAgICA8aSBjbGFzcz1cInt7b3B0aW9ucy5jYXJldENsYXNzfX1cIiAqbmdJZj1cIm9wdGlvbnMuZGlzcGxheVBsYWNlaG9sZGVyQ2FyZXRBZnRlciAmJiAhcGxhY2Vob2xkZXJDYXJhdFRlbXBsYXRlXCI+PC9pPlxuICAgIDxuZy10ZW1wbGF0ZSAqbmdJZj1cIm9wdGlvbnMuZGlzcGxheVBsYWNlaG9sZGVyQ2FyZXRBZnRlciAmJiBwbGFjZWhvbGRlckNhcmF0VGVtcGxhdGVcIiBbbmdUZW1wbGF0ZU91dGxldF09XCJwbGFjZWhvbGRlckNhcmF0VGVtcGxhdGVcIj48L25nLXRlbXBsYXRlPlxuICA8L2Rpdj5cbiAgPGRpdiBjbGFzcz1cImRyb3Bkb3duLWxpc3RcIj5cbiAgICA8ZGl2IGNsYXNzPVwiZHJvcGRvd24tbGlzdC1maWx0ZXJcIiAqbmdJZj1cIm9wdGlvbnMuZmlsdGVyYWJsZVwiPlxuICAgICAgPGlucHV0IFsobmdNb2RlbCldPVwiZmlsdGVyTW9kZWxcIiAobmdNb2RlbENoYW5nZSk9XCJvbkZpbHRlckNoYW5nZSgpXCIvPlxuICAgIDwvZGl2PlxuICAgIDxkaXYgY2xhc3M9XCJkcm9wZG93bi1saXN0LWl0ZW1cIiBbbmdDbGFzc109XCJ7c2VsZWN0ZWQ6aXNJdGVtU2VsZWN0ZWQob3B0aW9uKX1cIiAqbmdGb3I9XCJsZXQgb3B0aW9uIG9mIG9wdGlvbnMuaXRlbXNcIiAoY2xpY2spPVwic2VsZWN0SXRlbSgkZXZlbnQsIG9wdGlvbilcIj5cbiAgICAgIDxzcGFuIGNsYXNzPVwiaW1hZ2VcIiAqbmdJZj1cIm9wdGlvbnMudXNlSW1hZ2VzXCI+XG4gICAgICAgICA8c3Bhbj48aW1nIFtzcmNdPVwib3B0aW9uW29wdGlvbnMuaW1hZ2VLZXldXCIvPjwvc3Bhbj5cbiAgICAgICA8L3NwYW4+XG4gICAgICA8c3BhbiBjbGFzcz1cImxhYmVsXCIgKm5nSWY9XCJvcHRpb25zLnVzZUxhYmVsc1wiPlxuICAgICAgICA8c3Bhbj57e29wdGlvbltvcHRpb25zLmxhYmVsXX19PC9zcGFuPlxuICAgICAgPC9zcGFuPlxuICAgICAgIDxzcGFuIGNsYXNzPVwic2VsZWN0ZWQtaXRlbS1pbmRpY2F0b3JcIj5cbiAgICAgICAgIDxpIGNsYXNzPVwie3tvcHRpb25zLnNlbGVjdGVkSXRlbUNsYXNzfX1cIiAqbmdJZj1cIiFzZWxlY3RlZENhcmF0VGVtcGxhdGVcIj48L2k+XG4gICAgICAgICA8bmctdGVtcGxhdGUgKm5nSWY9XCJzZWxlY3RlZENhcmF0VGVtcGxhdGUgJiYgaXNJdGVtU2VsZWN0ZWQob3B0aW9uKVwiIFtuZ1RlbXBsYXRlT3V0bGV0XT1cInNlbGVjdGVkQ2FyYXRUZW1wbGF0ZVwiPjwvbmctdGVtcGxhdGU+XG4gICAgIDwvc3Bhbj5cbiAgICA8L2Rpdj5cbiAgPC9kaXY+XG48L2Rpdj5cbiJdfQ==