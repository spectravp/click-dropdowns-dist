import { Component, EventEmitter, Input, Output } from '@angular/core';
import { FormControl } from '@angular/forms';
import * as i0 from "@angular/core";
import * as i1 from "../../services/dropdown.service";
import * as i2 from "@angular/forms";
import * as i3 from "@angular/common";
function DropdownComponent_i_2_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelement(0, "i");
} if (rf & 2) {
    const ctx_r0 = i0.ɵɵnextContext();
    i0.ɵɵclassMap(ctx_r0.options.caretClass);
} }
function DropdownComponent_3_ng_template_0_Template(rf, ctx) { }
function DropdownComponent_3_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵtemplate(0, DropdownComponent_3_ng_template_0_Template, 0, 0, "ng-template", 10);
} if (rf & 2) {
    const ctx_r1 = i0.ɵɵnextContext();
    i0.ɵɵproperty("ngTemplateOutlet", ctx_r1.placeholderCaratTemplate);
} }
function DropdownComponent_span_4_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "span", 11);
    i0.ɵɵtext(1);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const ctx_r2 = i0.ɵɵnextContext();
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate(ctx_r2.options.placeholder);
} }
function DropdownComponent_span_5_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "span", 12);
    i0.ɵɵelement(1, "img", 13);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const ctx_r3 = i0.ɵɵnextContext();
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("src", ctx_r3.getSelectedImage(), i0.ɵɵsanitizeUrl);
} }
function DropdownComponent_span_6_span_1_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "span");
    i0.ɵɵtext(1);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const ctx_r10 = i0.ɵɵnextContext(2);
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate(ctx_r10.getSelectedLabel());
} }
function DropdownComponent_span_6_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "span", 14);
    i0.ɵɵtemplate(1, DropdownComponent_span_6_span_1_Template, 2, 1, "span", 3);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const ctx_r4 = i0.ɵɵnextContext();
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngIf", ctx_r4.isSelected());
} }
function DropdownComponent_i_7_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelement(0, "i");
} if (rf & 2) {
    const ctx_r5 = i0.ɵɵnextContext();
    i0.ɵɵclassMap(ctx_r5.options.caretClass);
} }
function DropdownComponent_8_ng_template_0_Template(rf, ctx) { }
function DropdownComponent_8_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵtemplate(0, DropdownComponent_8_ng_template_0_Template, 0, 0, "ng-template", 10);
} if (rf & 2) {
    const ctx_r6 = i0.ɵɵnextContext();
    i0.ɵɵproperty("ngTemplateOutlet", ctx_r6.placeholderCaratTemplate);
} }
function DropdownComponent_div_10_Template(rf, ctx) { if (rf & 1) {
    const _r13 = i0.ɵɵgetCurrentView();
    i0.ɵɵelementStart(0, "div", 15)(1, "input", 16);
    i0.ɵɵlistener("ngModelChange", function DropdownComponent_div_10_Template_input_ngModelChange_1_listener($event) { i0.ɵɵrestoreView(_r13); const ctx_r12 = i0.ɵɵnextContext(); return i0.ɵɵresetView(ctx_r12.filterModel = $event); })("ngModelChange", function DropdownComponent_div_10_Template_input_ngModelChange_1_listener() { i0.ɵɵrestoreView(_r13); const ctx_r14 = i0.ɵɵnextContext(); return i0.ɵɵresetView(ctx_r14.onFilterChange()); });
    i0.ɵɵelementEnd()();
} if (rf & 2) {
    const ctx_r7 = i0.ɵɵnextContext();
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngModel", ctx_r7.filterModel);
} }
function DropdownComponent_div_11_span_1_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "span", 12)(1, "span");
    i0.ɵɵelement(2, "img", 13);
    i0.ɵɵelementEnd()();
} if (rf & 2) {
    const option_r15 = i0.ɵɵnextContext().$implicit;
    const ctx_r16 = i0.ɵɵnextContext();
    i0.ɵɵadvance(2);
    i0.ɵɵproperty("src", option_r15[ctx_r16.options.imageKey], i0.ɵɵsanitizeUrl);
} }
function DropdownComponent_div_11_span_2_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "span", 14)(1, "span");
    i0.ɵɵtext(2);
    i0.ɵɵelementEnd()();
} if (rf & 2) {
    const option_r15 = i0.ɵɵnextContext().$implicit;
    const ctx_r17 = i0.ɵɵnextContext();
    i0.ɵɵadvance(2);
    i0.ɵɵtextInterpolate(option_r15[ctx_r17.options.label]);
} }
function DropdownComponent_div_11_i_4_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelement(0, "i");
} if (rf & 2) {
    const ctx_r18 = i0.ɵɵnextContext(2);
    i0.ɵɵclassMap(ctx_r18.options.selectedItemClass);
} }
function DropdownComponent_div_11_5_ng_template_0_Template(rf, ctx) { }
function DropdownComponent_div_11_5_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵtemplate(0, DropdownComponent_div_11_5_ng_template_0_Template, 0, 0, "ng-template", 10);
} if (rf & 2) {
    const ctx_r19 = i0.ɵɵnextContext(2);
    i0.ɵɵproperty("ngTemplateOutlet", ctx_r19.selectedCaratTemplate);
} }
const _c0 = function (a0) { return { selected: a0 }; };
function DropdownComponent_div_11_Template(rf, ctx) { if (rf & 1) {
    const _r24 = i0.ɵɵgetCurrentView();
    i0.ɵɵelementStart(0, "div", 17);
    i0.ɵɵlistener("click", function DropdownComponent_div_11_Template_div_click_0_listener($event) { const restoredCtx = i0.ɵɵrestoreView(_r24); const option_r15 = restoredCtx.$implicit; const ctx_r23 = i0.ɵɵnextContext(); return i0.ɵɵresetView(ctx_r23.selectItem($event, option_r15)); });
    i0.ɵɵtemplate(1, DropdownComponent_div_11_span_1_Template, 3, 1, "span", 5);
    i0.ɵɵtemplate(2, DropdownComponent_div_11_span_2_Template, 3, 1, "span", 6);
    i0.ɵɵelementStart(3, "span", 18);
    i0.ɵɵtemplate(4, DropdownComponent_div_11_i_4_Template, 1, 3, "i", 2);
    i0.ɵɵtemplate(5, DropdownComponent_div_11_5_Template, 1, 1, null, 3);
    i0.ɵɵelementEnd()();
} if (rf & 2) {
    const option_r15 = ctx.$implicit;
    const ctx_r8 = i0.ɵɵnextContext();
    i0.ɵɵproperty("ngClass", i0.ɵɵpureFunction1(5, _c0, ctx_r8.isItemSelected(option_r15)));
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngIf", ctx_r8.options.useImages);
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngIf", ctx_r8.options.useLabels);
    i0.ɵɵadvance(2);
    i0.ɵɵproperty("ngIf", !ctx_r8.selectedCaratTemplate);
    i0.ɵɵadvance(1);
    i0.ɵɵproperty("ngIf", ctx_r8.selectedCaratTemplate && ctx_r8.isItemSelected(option_r15));
} }
const _c1 = function (a0, a1) { return { open: a0, animated: a1 }; };
export class DropdownComponent {
    /**
     * Constructor / Dependency Injection
     */
    constructor(dropdownService) {
        this.dropdownService = dropdownService;
        this.elementChange = new EventEmitter();
        /**
         * The delay to wait for close all subscriptions to complete
         */
        this.closeDelay = 1;
        /**
         * The filter model
         */
        this.filterModel = '';
        /**
         * Flag indicating whether the menu is open or not
         */
        this.isOpen = false;
        /**
         * Flag for whether the placeholder has been injected
         */
        this.placeholderInjected = false;
    }
    /**
     * Will request that all dropdowns close
     */
    closeAllDropDowns() {
        this.dropdownService.closeAllDropdowns();
    }
    /**
     * Toggles this dropdown open/closed
     */
    toggle($event) {
        $event.stopImmediatePropagation();
        $event.stopPropagation();
        if (!this.isOpen && this.options.closeOthers) {
            this.closeAllDropDowns();
            setTimeout(() => {
                this.isOpen = true;
            }, this.closeDelay);
            return false;
        }
        this.isOpen = !this.isOpen;
        return false;
    }
    /**
     * Selects an item
     */
    selectItem($event, item) {
        $event.stopImmediatePropagation();
        $event.stopPropagation();
        if (!this.isValidSelection(item)) {
            return;
        }
        if (this.options.closeOnSelection) {
            this.isOpen = false;
        }
        if (this.getElementValue() === item[this.options.identifier] || item.isPlaceholder) {
            this.selectedItem = null;
            this.setElementValue(null);
            if (item.isPlaceholder) {
                this.options.items.shift();
                this.placeholderInjected = false;
            }
            return false;
        }
        this.selectedItem = item;
        this.setElementValue(item[this.options.identifier]);
        if (this.options.usePlaceholderAsItem && !this.placeholderInjected) {
            const props = JSON.parse(`{
        "${this.options.identifier}":null,
        "${this.options.label}":"${this.options.placeholder}",
        "isPlaceholder":true}`);
            this.options.items.unshift(props);
            this.placeholderInjected = true;
        }
        return false;
    }
    /**
     * Tests if the selected item is a valid selection
     */
    isValidSelection(item) {
        if (!this.options.allowOnlyValidValues) {
            return true;
        }
        if (item === '' || item === null || typeof item.length === 'undefined' || item.length === 0) {
            return true;
        }
        const validKeys = [];
        for (let i = 0; i < this.options.items.length; i++) {
            validKeys.push(this.options.items[i][this.options.identifier]);
        }
        if (typeof item === 'string') {
            return validKeys.indexOf(item) >= 0;
        }
        return validKeys.indexOf(item[this.options.identifier]) >= 0;
    }
    /**
     * Tests if an item is the selected item
     */
    isItemSelected(item) {
        return this.getElementValue() === item[this.options.identifier];
    }
    /**
     * Tests if the dropdown has a selection
     */
    isSelected() {
        const value = this.getElementValue();
        if (value && !this.selectedItem && this.options && this.options.items && this.options.items.length) {
            for (let i = 0; i < this.options.items.length; i++) {
                if (this.options.items[i][this.options.identifier] === value) {
                    this.selectedItem = this.options.items[i];
                    break;
                }
            }
        }
        return value;
    }
    /**
     * Gets the label of the selected item
     */
    getSelectedLabel() {
        if (this.selectedItem) {
            return this.selectedItem[this.options.label];
        }
    }
    /**
     * Gets the label of the selected item
     */
    getSelectedImage() {
        if (this.options.useImages) {
            if (this.selectedItem) {
                return this.selectedItem[this.options.imageKey];
            }
        }
    }
    /**
     * OnInit, listed for close all events
     */
    ngOnInit() {
        if (this.options.closeOthers) {
            this.closeAllDropdownsObserver = this.dropdownService.getCloseAllDropdownsObservable().subscribe(value => {
                this.isOpen = false;
            });
        }
        if (this.options.closeOnDocumentClick) {
            this.closeOnDocumentObserver = this.dropdownService.getDocumentClickObserver().subscribe(() => {
                this.isOpen = false;
            });
        }
        if (this.isSelected()) {
            if (!this.isValidSelection(this.getElementValue())) {
                this.clearSelection();
            }
        }
        if (this.elementRef instanceof FormControl) {
            this.elementRef.registerOnChange((value) => {
                if (!this.isValidSelection(value)) {
                    this.clearSelection();
                }
            });
        }
    }
    /**
     * Resets the value of the element
     */
    clearSelection() {
        const value = this.getElementValue();
        if (typeof value === 'string') {
            this.setElementValue(null);
        }
        else {
            this.setElementValue([]);
        }
    }
    /**
     * OnDestroy, stop listening for close all events
     */
    ngOnDestroy() {
        if (this.options.closeOthers) {
            this.closeAllDropdownsObserver.unsubscribe();
        }
        if (this.options.closeOnDocumentClick) {
            this.closeOnDocumentObserver.unsubscribe();
        }
    }
    /**
     * Gets the current value
     */
    getElementValue() {
        if (this.elementRef instanceof FormControl) {
            return this.elementRef.value;
        }
        return this.elementRef;
    }
    /**
     * Sets the current value
     */
    setElementValue(value) {
        this.element = value;
    }
    /**
     * Sets the element
     */
    set element(value) {
        if (this.elementRef instanceof FormControl) {
            this.elementRef.setValue(value);
        }
        else {
            this.elementRef = value;
        }
        this.elementChange.emit(value);
    }
    /**
     * Gets the element
     */
    get element() {
        return this.getElementValue();
    }
    /**
     * Filter the list on change
     */
    onFilterChange() {
        if (!this.filterMasterList) {
            this.filterMasterList = JSON.parse(JSON.stringify(this.options.items));
        }
        if (this.filterModel === '' || !this.filterModel) {
            this.options.items = JSON.parse(JSON.stringify(this.filterMasterList));
        }
        const value = this.filterModel.toLowerCase();
        this.options.items = this.filterMasterList.filter((item) => {
            return item[this.options.label].toLowerCase().indexOf(value) >= 0;
        });
    }
}
DropdownComponent.ɵfac = function DropdownComponent_Factory(t) { return new (t || DropdownComponent)(i0.ɵɵdirectiveInject(i1.DropdownService)); };
DropdownComponent.ɵcmp = /*@__PURE__*/ i0.ɵɵdefineComponent({ type: DropdownComponent, selectors: [["click-dropdown"]], inputs: { elementRef: "elementRef", options: "options", placeholderCaratTemplate: "placeholderCaratTemplate", selectedCaratTemplate: "selectedCaratTemplate", element: "element" }, outputs: { elementChange: "elementChange" }, decls: 12, vars: 13, consts: [[1, "dropdown", 3, "ngClass"], [1, "dropdown-display", 3, "click"], [3, "class", 4, "ngIf"], [4, "ngIf"], ["class", "placeholder", 4, "ngIf"], ["class", "image", 4, "ngIf"], ["class", "label", 4, "ngIf"], [1, "dropdown-list"], ["class", "dropdown-list-filter", 4, "ngIf"], ["class", "dropdown-list-item", 3, "ngClass", "click", 4, "ngFor", "ngForOf"], [3, "ngTemplateOutlet"], [1, "placeholder"], [1, "image"], [3, "src"], [1, "label"], [1, "dropdown-list-filter"], [3, "ngModel", "ngModelChange"], [1, "dropdown-list-item", 3, "ngClass", "click"], [1, "selected-item-indicator"]], template: function DropdownComponent_Template(rf, ctx) { if (rf & 1) {
        i0.ɵɵelementStart(0, "div", 0)(1, "div", 1);
        i0.ɵɵlistener("click", function DropdownComponent_Template_div_click_1_listener($event) { return ctx.toggle($event); });
        i0.ɵɵtemplate(2, DropdownComponent_i_2_Template, 1, 3, "i", 2);
        i0.ɵɵtemplate(3, DropdownComponent_3_Template, 1, 1, null, 3);
        i0.ɵɵtemplate(4, DropdownComponent_span_4_Template, 2, 1, "span", 4);
        i0.ɵɵtemplate(5, DropdownComponent_span_5_Template, 2, 1, "span", 5);
        i0.ɵɵtemplate(6, DropdownComponent_span_6_Template, 2, 1, "span", 6);
        i0.ɵɵtemplate(7, DropdownComponent_i_7_Template, 1, 3, "i", 2);
        i0.ɵɵtemplate(8, DropdownComponent_8_Template, 1, 1, null, 3);
        i0.ɵɵelementEnd();
        i0.ɵɵelementStart(9, "div", 7);
        i0.ɵɵtemplate(10, DropdownComponent_div_10_Template, 2, 1, "div", 8);
        i0.ɵɵtemplate(11, DropdownComponent_div_11_Template, 6, 7, "div", 9);
        i0.ɵɵelementEnd()();
    } if (rf & 2) {
        i0.ɵɵproperty("ngClass", i0.ɵɵpureFunction2(10, _c1, ctx.isOpen, ctx.options.animated));
        i0.ɵɵadvance(2);
        i0.ɵɵproperty("ngIf", ctx.options.displayPlaceholderCaretBefore && !ctx.placeholderCaratTemplate);
        i0.ɵɵadvance(1);
        i0.ɵɵproperty("ngIf", ctx.options.displayPlaceholderCaretBefore && ctx.placeholderCaratTemplate);
        i0.ɵɵadvance(1);
        i0.ɵɵproperty("ngIf", !ctx.isSelected());
        i0.ɵɵadvance(1);
        i0.ɵɵproperty("ngIf", ctx.isSelected() && ctx.getSelectedImage());
        i0.ɵɵadvance(1);
        i0.ɵɵproperty("ngIf", ctx.options.useLabels);
        i0.ɵɵadvance(1);
        i0.ɵɵproperty("ngIf", ctx.options.displayPlaceholderCaretAfter && !ctx.placeholderCaratTemplate);
        i0.ɵɵadvance(1);
        i0.ɵɵproperty("ngIf", ctx.options.displayPlaceholderCaretAfter && ctx.placeholderCaratTemplate);
        i0.ɵɵadvance(2);
        i0.ɵɵproperty("ngIf", ctx.options.filterable);
        i0.ɵɵadvance(1);
        i0.ɵɵproperty("ngForOf", ctx.options.items);
    } }, dependencies: [i2.DefaultValueAccessor, i2.NgControlStatus, i2.NgModel, i3.NgClass, i3.NgForOf, i3.NgIf, i3.NgTemplateOutlet], encapsulation: 2 });
(function () { (typeof ngDevMode === "undefined" || ngDevMode) && i0.ɵsetClassMetadata(DropdownComponent, [{
        type: Component,
        args: [{ selector: 'click-dropdown', template: "<div class=\"dropdown\" [ngClass]=\"{open: isOpen, animated: options.animated}\">\n  <div class=\"dropdown-display\" (click)=\"toggle($event)\">\n    <i class=\"{{options.caretClass}}\" *ngIf=\"options.displayPlaceholderCaretBefore && !placeholderCaratTemplate\"></i>\n    <ng-template *ngIf=\"options.displayPlaceholderCaretBefore && placeholderCaratTemplate\" [ngTemplateOutlet]=\"placeholderCaratTemplate\"></ng-template>\n    <span *ngIf=\"! isSelected()\" class=\"placeholder\">{{ options.placeholder }}</span>\n    <span class=\"image\" *ngIf=\"isSelected() && getSelectedImage()\">\n      <img [src]=\"getSelectedImage()\"/>\n    </span>\n    <span class=\"label\" *ngIf=\"options.useLabels\">\n      <span *ngIf=\"isSelected()\">{{ getSelectedLabel() }}</span>\n    </span>\n    <i class=\"{{options.caretClass}}\" *ngIf=\"options.displayPlaceholderCaretAfter && !placeholderCaratTemplate\"></i>\n    <ng-template *ngIf=\"options.displayPlaceholderCaretAfter && placeholderCaratTemplate\" [ngTemplateOutlet]=\"placeholderCaratTemplate\"></ng-template>\n  </div>\n  <div class=\"dropdown-list\">\n    <div class=\"dropdown-list-filter\" *ngIf=\"options.filterable\">\n      <input [(ngModel)]=\"filterModel\" (ngModelChange)=\"onFilterChange()\"/>\n    </div>\n    <div class=\"dropdown-list-item\" [ngClass]=\"{selected:isItemSelected(option)}\" *ngFor=\"let option of options.items\" (click)=\"selectItem($event, option)\">\n      <span class=\"image\" *ngIf=\"options.useImages\">\n         <span><img [src]=\"option[options.imageKey]\"/></span>\n       </span>\n      <span class=\"label\" *ngIf=\"options.useLabels\">\n        <span>{{option[options.label]}}</span>\n      </span>\n       <span class=\"selected-item-indicator\">\n         <i class=\"{{options.selectedItemClass}}\" *ngIf=\"!selectedCaratTemplate\"></i>\n         <ng-template *ngIf=\"selectedCaratTemplate && isItemSelected(option)\" [ngTemplateOutlet]=\"selectedCaratTemplate\"></ng-template>\n     </span>\n    </div>\n  </div>\n</div>\n" }]
    }], function () { return [{ type: i1.DropdownService }]; }, { elementRef: [{
            type: Input
        }], options: [{
            type: Input
        }], elementChange: [{
            type: Output
        }], placeholderCaratTemplate: [{
            type: Input
        }], selectedCaratTemplate: [{
            type: Input
        }], element: [{
            type: Input
        }] }); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZHJvcGRvd24uY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vLi4vLi4vLi4vLi4vcHJvamVjdHMvZHJvcGRvd25zL3NyYy9saWIvY29tcG9uZW50cy9kcm9wZG93bi9kcm9wZG93bi5jb21wb25lbnQudHMiLCIuLi8uLi8uLi8uLi8uLi8uLi9wcm9qZWN0cy9kcm9wZG93bnMvc3JjL2xpYi9jb21wb25lbnRzL2Ryb3Bkb3duL2Ryb3Bkb3duLmNvbXBvbmVudC5odG1sIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sRUFBQyxTQUFTLEVBQUUsWUFBWSxFQUFFLEtBQUssRUFBcUIsTUFBTSxFQUFjLE1BQU0sZUFBZSxDQUFDO0FBSXJHLE9BQU8sRUFBQyxXQUFXLEVBQUMsTUFBTSxnQkFBZ0IsQ0FBQzs7Ozs7O0lDRnZDLG9CQUFpSDs7O0lBQTlHLHdDQUE4Qjs7OztJQUNqQyxxRkFBbUo7OztJQUE1RCxrRUFBNkM7OztJQUNwSSxnQ0FBaUQ7SUFBQSxZQUF5QjtJQUFBLGlCQUFPOzs7SUFBaEMsZUFBeUI7SUFBekIsZ0RBQXlCOzs7SUFDMUUsZ0NBQStEO0lBQzdELDBCQUFpQztJQUNuQyxpQkFBTzs7O0lBREEsZUFBMEI7SUFBMUIsaUVBQTBCOzs7SUFHL0IsNEJBQTJCO0lBQUEsWUFBd0I7SUFBQSxpQkFBTzs7O0lBQS9CLGVBQXdCO0lBQXhCLGdEQUF3Qjs7O0lBRHJELGdDQUE4QztJQUM1QywyRUFBMEQ7SUFDNUQsaUJBQU87OztJQURFLGVBQWtCO0lBQWxCLDBDQUFrQjs7O0lBRTNCLG9CQUFnSDs7O0lBQTdHLHdDQUE4Qjs7OztJQUNqQyxxRkFBa0o7OztJQUE1RCxrRUFBNkM7Ozs7SUFHbkksK0JBQTZELGdCQUFBO0lBQ3BELHNPQUF5QixtS0FBa0IsZUFBQSx3QkFBZ0IsQ0FBQSxJQUFsQztJQUFoQyxpQkFBcUUsRUFBQTs7O0lBQTlELGVBQXlCO0lBQXpCLDRDQUF5Qjs7O0lBR2hDLGdDQUE4QyxXQUFBO0lBQ3JDLDBCQUF1QztJQUFBLGlCQUFPLEVBQUE7Ozs7SUFBekMsZUFBZ0M7SUFBaEMsNEVBQWdDOzs7SUFFOUMsZ0NBQThDLFdBQUE7SUFDdEMsWUFBeUI7SUFBQSxpQkFBTyxFQUFBOzs7O0lBQWhDLGVBQXlCO0lBQXpCLHVEQUF5Qjs7O0lBRzlCLG9CQUE0RTs7O0lBQXpFLGdEQUFxQzs7OztJQUN4Qyw0RkFBOEg7OztJQUF6RCxnRUFBMEM7Ozs7O0lBVHBILCtCQUF3SjtJQUFyQyxrT0FBUyxlQUFBLHNDQUEwQixDQUFBLElBQUM7SUFDckosMkVBRVE7SUFDUiwyRUFFTztJQUNOLGdDQUFzQztJQUNwQyxxRUFBNEU7SUFDNUUsb0VBQThIO0lBQ2xJLGlCQUFPLEVBQUE7Ozs7SUFWd0IsdUZBQTZDO0lBQ3RELGVBQXVCO0lBQXZCLCtDQUF1QjtJQUd2QixlQUF1QjtJQUF2QiwrQ0FBdUI7SUFJQyxlQUE0QjtJQUE1QixvREFBNEI7SUFDeEQsZUFBcUQ7SUFBckQsd0ZBQXFEOzs7QURoQjVFLE1BQU0sT0FBTyxpQkFBaUI7SUFpRDVCOztPQUVHO0lBQ0gsWUFBc0IsZUFBZ0M7UUFBaEMsb0JBQWUsR0FBZixlQUFlLENBQWlCO1FBaEQ1QyxrQkFBYSxHQUFzQixJQUFJLFlBQVksRUFBTyxDQUFDO1FBVXJFOztXQUVHO1FBQ08sZUFBVSxHQUFHLENBQUMsQ0FBQztRQVl6Qjs7V0FFRztRQUNJLGdCQUFXLEdBQUcsRUFBRSxDQUFDO1FBT3hCOztXQUVHO1FBQ0ksV0FBTSxHQUFHLEtBQUssQ0FBQztRQUV0Qjs7V0FFRztRQUNLLHdCQUFtQixHQUFHLEtBQUssQ0FBQztJQUtxQixDQUFDO0lBRTFEOztPQUVHO0lBQ08saUJBQWlCO1FBQ3pCLElBQUksQ0FBQyxlQUFlLENBQUMsaUJBQWlCLEVBQUUsQ0FBQztJQUMzQyxDQUFDO0lBRUQ7O09BRUc7SUFDSCxNQUFNLENBQUMsTUFBYTtRQUNsQixNQUFNLENBQUMsd0JBQXdCLEVBQUUsQ0FBQztRQUNsQyxNQUFNLENBQUMsZUFBZSxFQUFFLENBQUM7UUFFekIsSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLElBQUksSUFBSSxDQUFDLE9BQU8sQ0FBQyxXQUFXLEVBQUc7WUFDN0MsSUFBSSxDQUFDLGlCQUFpQixFQUFFLENBQUM7WUFDekIsVUFBVSxDQUFDLEdBQUcsRUFBRTtnQkFDZCxJQUFJLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQztZQUNyQixDQUFDLEVBQUUsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDO1lBQ3BCLE9BQU8sS0FBSyxDQUFDO1NBQ2Q7UUFFRCxJQUFJLENBQUMsTUFBTSxHQUFHLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQztRQUMzQixPQUFPLEtBQUssQ0FBQztJQUNmLENBQUM7SUFFRDs7T0FFRztJQUNILFVBQVUsQ0FBQyxNQUFNLEVBQUUsSUFBUztRQUMxQixNQUFNLENBQUMsd0JBQXdCLEVBQUUsQ0FBQztRQUNsQyxNQUFNLENBQUMsZUFBZSxFQUFFLENBQUM7UUFFekIsSUFBSyxDQUFFLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxJQUFJLENBQUMsRUFBRTtZQUNsQyxPQUFPO1NBQ1I7UUFFRCxJQUFLLElBQUksQ0FBQyxPQUFPLENBQUMsZ0JBQWdCLEVBQUc7WUFDbkMsSUFBSSxDQUFDLE1BQU0sR0FBRyxLQUFLLENBQUM7U0FDckI7UUFFRCxJQUFJLElBQUksQ0FBQyxlQUFlLEVBQUUsS0FBTSxJQUFJLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxVQUFVLENBQUMsSUFBSSxJQUFJLENBQUMsYUFBYSxFQUFFO1lBQ25GLElBQUksQ0FBQyxZQUFZLEdBQUcsSUFBSSxDQUFDO1lBQ3pCLElBQUksQ0FBQyxlQUFlLENBQUMsSUFBSSxDQUFDLENBQUM7WUFDM0IsSUFBSSxJQUFJLENBQUMsYUFBYSxFQUFFO2dCQUN0QixJQUFJLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxLQUFLLEVBQUUsQ0FBQztnQkFDM0IsSUFBSSxDQUFDLG1CQUFtQixHQUFHLEtBQUssQ0FBQzthQUNsQztZQUNELE9BQU8sS0FBSyxDQUFDO1NBQ2Q7UUFFRCxJQUFJLENBQUMsWUFBWSxHQUFHLElBQUksQ0FBQztRQUN6QixJQUFJLENBQUMsZUFBZSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUM7UUFFcEQsSUFBSSxJQUFJLENBQUMsT0FBTyxDQUFDLG9CQUFvQixJQUFJLENBQUUsSUFBSSxDQUFDLG1CQUFtQixFQUFFO1lBQ25FLE1BQU0sS0FBSyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUM7V0FDcEIsSUFBSSxDQUFDLE9BQU8sQ0FBQyxVQUFVO1dBQ3ZCLElBQUksQ0FBQyxPQUFPLENBQUMsS0FBSyxNQUFNLElBQUksQ0FBQyxPQUFPLENBQUMsV0FBVzs4QkFDN0IsQ0FBQyxDQUFDO1lBQzFCLElBQUksQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsQ0FBQztZQUNsQyxJQUFJLENBQUMsbUJBQW1CLEdBQUcsSUFBSSxDQUFDO1NBQ2pDO1FBRUQsT0FBTyxLQUFLLENBQUM7SUFDZixDQUFDO0lBRUQ7O09BRUc7SUFDSCxnQkFBZ0IsQ0FBQyxJQUFTO1FBQ3hCLElBQUssQ0FBRSxJQUFJLENBQUMsT0FBTyxDQUFDLG9CQUFvQixFQUFHO1lBQ3pDLE9BQU8sSUFBSSxDQUFDO1NBQ2I7UUFFRCxJQUFJLElBQUksS0FBSyxFQUFFLElBQUksSUFBSSxLQUFLLElBQUksSUFBSSxPQUFPLElBQUksQ0FBQyxNQUFNLEtBQUssV0FBVyxJQUFJLElBQUksQ0FBQyxNQUFNLEtBQUssQ0FBQyxFQUFFO1lBQzNGLE9BQU8sSUFBSSxDQUFDO1NBQ2I7UUFFRCxNQUFNLFNBQVMsR0FBRyxFQUFFLENBQUM7UUFFckIsS0FBSyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFBRztZQUNuRCxTQUFTLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQztTQUNoRTtRQUVELElBQUssT0FBTyxJQUFJLEtBQUssUUFBUSxFQUFHO1lBQzlCLE9BQU8sU0FBUyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7U0FDckM7UUFFRCxPQUFPLFNBQVMsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsVUFBVSxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUM7SUFDL0QsQ0FBQztJQUVEOztPQUVHO0lBQ0gsY0FBYyxDQUFDLElBQVM7UUFDdEIsT0FBTyxJQUFJLENBQUMsZUFBZSxFQUFFLEtBQUssSUFBSSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsVUFBVSxDQUFDLENBQUM7SUFDbEUsQ0FBQztJQUVEOztPQUVHO0lBQ0gsVUFBVTtRQUNSLE1BQU0sS0FBSyxHQUFHLElBQUksQ0FBQyxlQUFlLEVBQUUsQ0FBQztRQUNyQyxJQUFJLEtBQUssSUFBSSxDQUFFLElBQUksQ0FBQyxZQUFZLElBQUksSUFBSSxDQUFDLE9BQU8sSUFBSSxJQUFJLENBQUMsT0FBTyxDQUFDLEtBQUssSUFBSSxJQUFJLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxNQUFNLEVBQUU7WUFDbkcsS0FBSyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFBRTtnQkFDbEQsSUFBSyxJQUFJLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLFVBQVUsQ0FBQyxLQUFLLEtBQUssRUFBRTtvQkFDN0QsSUFBSSxDQUFDLFlBQVksR0FBRyxJQUFJLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQztvQkFDMUMsTUFBTTtpQkFDUDthQUNGO1NBQ0Y7UUFDRCxPQUFPLEtBQUssQ0FBQztJQUNmLENBQUM7SUFFRDs7T0FFRztJQUNILGdCQUFnQjtRQUNkLElBQUksSUFBSSxDQUFDLFlBQVksRUFBRztZQUN0QixPQUFPLElBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsQ0FBQztTQUM5QztJQUNILENBQUM7SUFFRDs7T0FFRztJQUNILGdCQUFnQjtRQUNkLElBQUksSUFBSSxDQUFDLE9BQU8sQ0FBQyxTQUFTLEVBQUU7WUFDMUIsSUFBSSxJQUFJLENBQUMsWUFBWSxFQUFHO2dCQUN0QixPQUFPLElBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxRQUFRLENBQUMsQ0FBQzthQUNqRDtTQUNGO0lBQ0gsQ0FBQztJQUVEOztPQUVHO0lBQ0gsUUFBUTtRQUNOLElBQUksSUFBSSxDQUFDLE9BQU8sQ0FBQyxXQUFXLEVBQUc7WUFDN0IsSUFBSSxDQUFDLHlCQUF5QixHQUFHLElBQUksQ0FBQyxlQUFlLENBQUMsOEJBQThCLEVBQUUsQ0FBQyxTQUFTLENBQUMsS0FBSyxDQUFDLEVBQUU7Z0JBQ3ZHLElBQUksQ0FBQyxNQUFNLEdBQUcsS0FBSyxDQUFDO1lBQ3RCLENBQUMsQ0FBQyxDQUFDO1NBQ0o7UUFFRCxJQUFJLElBQUksQ0FBQyxPQUFPLENBQUMsb0JBQW9CLEVBQUc7WUFDdEMsSUFBSSxDQUFDLHVCQUF1QixHQUFHLElBQUksQ0FBQyxlQUFlLENBQUMsd0JBQXdCLEVBQUUsQ0FBQyxTQUFTLENBQUMsR0FBRyxFQUFFO2dCQUM1RixJQUFJLENBQUMsTUFBTSxHQUFHLEtBQUssQ0FBQztZQUN0QixDQUFDLENBQUMsQ0FBQztTQUNKO1FBRUQsSUFBSSxJQUFJLENBQUMsVUFBVSxFQUFFLEVBQUc7WUFDdEIsSUFBSSxDQUFFLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxJQUFJLENBQUMsZUFBZSxFQUFFLENBQUMsRUFBRTtnQkFDbkQsSUFBSSxDQUFDLGNBQWMsRUFBRSxDQUFDO2FBQ3ZCO1NBQ0Y7UUFFRCxJQUFJLElBQUksQ0FBQyxVQUFVLFlBQVksV0FBVyxFQUFFO1lBQzFDLElBQUksQ0FBQyxVQUFVLENBQUMsZ0JBQWdCLENBQUMsQ0FBQyxLQUFLLEVBQUUsRUFBRTtnQkFDekMsSUFBSyxDQUFDLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxLQUFLLENBQUMsRUFBRztvQkFDbkMsSUFBSSxDQUFDLGNBQWMsRUFBRSxDQUFDO2lCQUN2QjtZQUNILENBQUMsQ0FBQyxDQUFDO1NBQ0o7SUFDSCxDQUFDO0lBRUQ7O09BRUc7SUFDSCxjQUFjO1FBQ1osTUFBTSxLQUFLLEdBQUcsSUFBSSxDQUFDLGVBQWUsRUFBRSxDQUFDO1FBRXJDLElBQUksT0FBTyxLQUFLLEtBQUssUUFBUSxFQUFFO1lBQzdCLElBQUksQ0FBQyxlQUFlLENBQUMsSUFBSSxDQUFDLENBQUM7U0FDNUI7YUFBTTtZQUNMLElBQUksQ0FBQyxlQUFlLENBQUMsRUFBRSxDQUFDLENBQUM7U0FDMUI7SUFDSCxDQUFDO0lBRUQ7O09BRUc7SUFDSCxXQUFXO1FBQ1QsSUFBSSxJQUFJLENBQUMsT0FBTyxDQUFDLFdBQVcsRUFBRztZQUM3QixJQUFJLENBQUMseUJBQXlCLENBQUMsV0FBVyxFQUFFLENBQUM7U0FDOUM7UUFFRCxJQUFJLElBQUksQ0FBQyxPQUFPLENBQUMsb0JBQW9CLEVBQUc7WUFDdEMsSUFBSSxDQUFDLHVCQUF1QixDQUFDLFdBQVcsRUFBRSxDQUFDO1NBQzVDO0lBQ0gsQ0FBQztJQUVEOztPQUVHO0lBQ0gsZUFBZTtRQUNiLElBQUksSUFBSSxDQUFDLFVBQVUsWUFBWSxXQUFXLEVBQUc7WUFDM0MsT0FBTyxJQUFJLENBQUMsVUFBVSxDQUFDLEtBQUssQ0FBQztTQUM5QjtRQUVELE9BQU8sSUFBSSxDQUFDLFVBQVUsQ0FBQztJQUN6QixDQUFDO0lBRUQ7O09BRUc7SUFDSCxlQUFlLENBQUMsS0FBVTtRQUN4QixJQUFJLENBQUMsT0FBTyxHQUFHLEtBQUssQ0FBQztJQUN2QixDQUFDO0lBRUQ7O09BRUc7SUFDSCxJQUFJLE9BQU8sQ0FBQyxLQUFLO1FBQ2YsSUFBSSxJQUFJLENBQUMsVUFBVSxZQUFZLFdBQVcsRUFBRTtZQUMxQyxJQUFJLENBQUMsVUFBVSxDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUMsQ0FBQztTQUNqQzthQUFNO1lBQ0wsSUFBSSxDQUFDLFVBQVUsR0FBRyxLQUFLLENBQUM7U0FDekI7UUFFRCxJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztJQUNqQyxDQUFDO0lBRUQ7O09BRUc7SUFDSCxJQUNJLE9BQU87UUFDVCxPQUFPLElBQUksQ0FBQyxlQUFlLEVBQUUsQ0FBQztJQUNoQyxDQUFDO0lBRUQ7O09BRUc7SUFDSCxjQUFjO1FBQ1osSUFBSSxDQUFDLElBQUksQ0FBQyxnQkFBZ0IsRUFBRTtZQUMxQixJQUFJLENBQUMsZ0JBQWdCLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQztTQUN4RTtRQUVELElBQUksSUFBSSxDQUFDLFdBQVcsS0FBSyxFQUFFLElBQUksQ0FBRSxJQUFJLENBQUMsV0FBVyxFQUFFO1lBQ2pELElBQUksQ0FBQyxPQUFPLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsQ0FBQyxDQUFDO1NBQ3hFO1FBRUQsTUFBTSxLQUFLLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxXQUFXLEVBQUUsQ0FBQztRQUM3QyxJQUFJLENBQUMsT0FBTyxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsTUFBTSxDQUFDLENBQUMsSUFBSSxFQUFFLEVBQUU7WUFDekQsT0FBTyxJQUFJLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQ3BFLENBQUMsQ0FBQyxDQUFDO0lBQ0wsQ0FBQzs7a0ZBNVNVLGlCQUFpQjtvRUFBakIsaUJBQWlCO1FDWDlCLDhCQUE2RSxhQUFBO1FBQzdDLGlHQUFTLGtCQUFjLElBQUM7UUFDcEQsOERBQWlIO1FBQ2pILDZEQUFtSjtRQUNuSixvRUFBaUY7UUFDakYsb0VBRU87UUFDUCxvRUFFTztRQUNQLDhEQUFnSDtRQUNoSCw2REFBa0o7UUFDcEosaUJBQU07UUFDTiw4QkFBMkI7UUFDekIsb0VBRU07UUFDTixvRUFXTTtRQUNSLGlCQUFNLEVBQUE7O1FBOUJjLHVGQUFzRDtRQUVyQyxlQUF3RTtRQUF4RSxpR0FBd0U7UUFDN0YsZUFBdUU7UUFBdkUsZ0dBQXVFO1FBQzlFLGVBQW9CO1FBQXBCLHdDQUFvQjtRQUNOLGVBQXdDO1FBQXhDLGlFQUF3QztRQUd4QyxlQUF1QjtRQUF2Qiw0Q0FBdUI7UUFHVCxlQUF1RTtRQUF2RSxnR0FBdUU7UUFDNUYsZUFBc0U7UUFBdEUsK0ZBQXNFO1FBR2pELGVBQXdCO1FBQXhCLDZDQUF3QjtRQUdzQyxlQUFnQjtRQUFoQiwyQ0FBZ0I7O3VGRFB4RyxpQkFBaUI7Y0FMN0IsU0FBUzsyQkFDRSxnQkFBZ0I7a0VBTWpCLFVBQVU7a0JBQWxCLEtBQUs7WUFDRyxPQUFPO2tCQUFmLEtBQUs7WUFDSSxhQUFhO2tCQUF0QixNQUFNO1lBQ0Usd0JBQXdCO2tCQUFoQyxLQUFLO1lBQ0cscUJBQXFCO2tCQUE3QixLQUFLO1lBa1JGLE9BQU87a0JBRFYsS0FBSyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7Q29tcG9uZW50LCBFdmVudEVtaXR0ZXIsIElucHV0LCBPbkRlc3Ryb3ksIE9uSW5pdCwgT3V0cHV0LCBUZW1wbGF0ZVJlZn0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQge0Ryb3Bkb3duT3B0aW9uc01vZGVsfSBmcm9tICcuLi8uLi9tb2RlbHMvZHJvcGRvd24tb3B0aW9ucy5tb2RlbCc7XG5pbXBvcnQge1N1YnNjcmlwdGlvbn0gZnJvbSAncnhqcyc7XG5pbXBvcnQge0Ryb3Bkb3duU2VydmljZX0gZnJvbSAnLi4vLi4vc2VydmljZXMvZHJvcGRvd24uc2VydmljZSc7XG5pbXBvcnQge0Zvcm1Db250cm9sfSBmcm9tICdAYW5ndWxhci9mb3Jtcyc7XG5cbkBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogJ2NsaWNrLWRyb3Bkb3duJyxcbiAgdGVtcGxhdGVVcmw6ICcuL2Ryb3Bkb3duLmNvbXBvbmVudC5odG1sJyxcbiAgLy8gc3R5bGVVcmxzOiBbJy4vZHJvcGRvd24uY29tcG9uZW50LnNjc3MnXVxufSlcbmV4cG9ydCBjbGFzcyBEcm9wZG93bkNvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCwgT25EZXN0cm95IHtcblxuICBASW5wdXQoKSBlbGVtZW50UmVmOiBGb3JtQ29udHJvbCB8IGFueTtcbiAgQElucHV0KCkgb3B0aW9uczogRHJvcGRvd25PcHRpb25zTW9kZWw7XG4gIEBPdXRwdXQoKSBlbGVtZW50Q2hhbmdlOiBFdmVudEVtaXR0ZXI8YW55PiA9IG5ldyBFdmVudEVtaXR0ZXI8YW55PigpO1xuICBASW5wdXQoKSBwbGFjZWhvbGRlckNhcmF0VGVtcGxhdGU6IFRlbXBsYXRlUmVmPGFueT47XG4gIEBJbnB1dCgpIHNlbGVjdGVkQ2FyYXRUZW1wbGF0ZTogVGVtcGxhdGVSZWY8YW55PjtcblxuXG4gIC8qKlxuICAgKiBUaGUgc2VsZWN0ZWQgaXRlbVxuICAgKi9cbiAgcHJvdGVjdGVkIHNlbGVjdGVkSXRlbTogYW55O1xuXG4gIC8qKlxuICAgKiBUaGUgZGVsYXkgdG8gd2FpdCBmb3IgY2xvc2UgYWxsIHN1YnNjcmlwdGlvbnMgdG8gY29tcGxldGVcbiAgICovXG4gIHByb3RlY3RlZCBjbG9zZURlbGF5ID0gMTtcblxuICAvKipcbiAgICogQW4gb2JzZXJ2ZXIgdGhhdCBsaXN0ZW5zIGZvciBhbGwgY2xvc2UgYWxsIHJlcXVlc3RzXG4gICAqL1xuICBwcm90ZWN0ZWQgY2xvc2VBbGxEcm9wZG93bnNPYnNlcnZlcjogU3Vic2NyaXB0aW9uO1xuXG4gIC8qKlxuICAgKiBBbiBvYnNlcnZlciB0aGF0IGxpc3RlbnMgZm9yIGFsbCBkb2N1bWVudCBjbGlja3NcbiAgICovXG4gIHByb3RlY3RlZCBjbG9zZU9uRG9jdW1lbnRPYnNlcnZlcjogU3Vic2NyaXB0aW9uO1xuXG4gIC8qKlxuICAgKiBUaGUgZmlsdGVyIG1vZGVsXG4gICAqL1xuICBwdWJsaWMgZmlsdGVyTW9kZWwgPSAnJztcblxuICAvKipcbiAgICogRmlsdGVyZXJzIG1hc3RlciBsaXN0XG4gICAqL1xuICBwcm90ZWN0ZWQgZmlsdGVyTWFzdGVyTGlzdDogYW55W107XG5cbiAgLyoqXG4gICAqIEZsYWcgaW5kaWNhdGluZyB3aGV0aGVyIHRoZSBtZW51IGlzIG9wZW4gb3Igbm90XG4gICAqL1xuICBwdWJsaWMgaXNPcGVuID0gZmFsc2U7XG5cbiAgLyoqXG4gICAqIEZsYWcgZm9yIHdoZXRoZXIgdGhlIHBsYWNlaG9sZGVyIGhhcyBiZWVuIGluamVjdGVkXG4gICAqL1xuICBwcml2YXRlIHBsYWNlaG9sZGVySW5qZWN0ZWQgPSBmYWxzZTtcblxuICAvKipcbiAgICogQ29uc3RydWN0b3IgLyBEZXBlbmRlbmN5IEluamVjdGlvblxuICAgKi9cbiAgY29uc3RydWN0b3IocHJvdGVjdGVkIGRyb3Bkb3duU2VydmljZTogRHJvcGRvd25TZXJ2aWNlKSB7fVxuXG4gIC8qKlxuICAgKiBXaWxsIHJlcXVlc3QgdGhhdCBhbGwgZHJvcGRvd25zIGNsb3NlXG4gICAqL1xuICBwcm90ZWN0ZWQgY2xvc2VBbGxEcm9wRG93bnMoKSB7XG4gICAgdGhpcy5kcm9wZG93blNlcnZpY2UuY2xvc2VBbGxEcm9wZG93bnMoKTtcbiAgfVxuXG4gIC8qKlxuICAgKiBUb2dnbGVzIHRoaXMgZHJvcGRvd24gb3Blbi9jbG9zZWRcbiAgICovXG4gIHRvZ2dsZSgkZXZlbnQ6IEV2ZW50KTogYm9vbGVhbiB7XG4gICAgJGV2ZW50LnN0b3BJbW1lZGlhdGVQcm9wYWdhdGlvbigpO1xuICAgICRldmVudC5zdG9wUHJvcGFnYXRpb24oKTtcblxuICAgIGlmICghdGhpcy5pc09wZW4gJiYgdGhpcy5vcHRpb25zLmNsb3NlT3RoZXJzICkge1xuICAgICAgdGhpcy5jbG9zZUFsbERyb3BEb3ducygpO1xuICAgICAgc2V0VGltZW91dCgoKSA9PiB7XG4gICAgICAgIHRoaXMuaXNPcGVuID0gdHJ1ZTtcbiAgICAgIH0sIHRoaXMuY2xvc2VEZWxheSk7XG4gICAgICByZXR1cm4gZmFsc2U7XG4gICAgfVxuXG4gICAgdGhpcy5pc09wZW4gPSAhdGhpcy5pc09wZW47XG4gICAgcmV0dXJuIGZhbHNlO1xuICB9XG5cbiAgLyoqXG4gICAqIFNlbGVjdHMgYW4gaXRlbVxuICAgKi9cbiAgc2VsZWN0SXRlbSgkZXZlbnQsIGl0ZW06IGFueSk6IGJvb2xlYW4ge1xuICAgICRldmVudC5zdG9wSW1tZWRpYXRlUHJvcGFnYXRpb24oKTtcbiAgICAkZXZlbnQuc3RvcFByb3BhZ2F0aW9uKCk7XG5cbiAgICBpZiAoICEgdGhpcy5pc1ZhbGlkU2VsZWN0aW9uKGl0ZW0pKSB7XG4gICAgICByZXR1cm47XG4gICAgfVxuXG4gICAgaWYgKCB0aGlzLm9wdGlvbnMuY2xvc2VPblNlbGVjdGlvbiApIHtcbiAgICAgIHRoaXMuaXNPcGVuID0gZmFsc2U7XG4gICAgfVxuXG4gICAgaWYgKHRoaXMuZ2V0RWxlbWVudFZhbHVlKCkgPT09ICBpdGVtW3RoaXMub3B0aW9ucy5pZGVudGlmaWVyXSB8fCBpdGVtLmlzUGxhY2Vob2xkZXIpIHtcbiAgICAgIHRoaXMuc2VsZWN0ZWRJdGVtID0gbnVsbDtcbiAgICAgIHRoaXMuc2V0RWxlbWVudFZhbHVlKG51bGwpO1xuICAgICAgaWYgKGl0ZW0uaXNQbGFjZWhvbGRlcikge1xuICAgICAgICB0aGlzLm9wdGlvbnMuaXRlbXMuc2hpZnQoKTtcbiAgICAgICAgdGhpcy5wbGFjZWhvbGRlckluamVjdGVkID0gZmFsc2U7XG4gICAgICB9XG4gICAgICByZXR1cm4gZmFsc2U7XG4gICAgfVxuXG4gICAgdGhpcy5zZWxlY3RlZEl0ZW0gPSBpdGVtO1xuICAgIHRoaXMuc2V0RWxlbWVudFZhbHVlKGl0ZW1bdGhpcy5vcHRpb25zLmlkZW50aWZpZXJdKTtcblxuICAgIGlmICh0aGlzLm9wdGlvbnMudXNlUGxhY2Vob2xkZXJBc0l0ZW0gJiYgISB0aGlzLnBsYWNlaG9sZGVySW5qZWN0ZWQpIHtcbiAgICAgIGNvbnN0IHByb3BzID0gSlNPTi5wYXJzZShge1xuICAgICAgICBcIiR7dGhpcy5vcHRpb25zLmlkZW50aWZpZXJ9XCI6bnVsbCxcbiAgICAgICAgXCIke3RoaXMub3B0aW9ucy5sYWJlbH1cIjpcIiR7dGhpcy5vcHRpb25zLnBsYWNlaG9sZGVyfVwiLFxuICAgICAgICBcImlzUGxhY2Vob2xkZXJcIjp0cnVlfWApO1xuICAgICAgdGhpcy5vcHRpb25zLml0ZW1zLnVuc2hpZnQocHJvcHMpO1xuICAgICAgdGhpcy5wbGFjZWhvbGRlckluamVjdGVkID0gdHJ1ZTtcbiAgICB9XG5cbiAgICByZXR1cm4gZmFsc2U7XG4gIH1cblxuICAvKipcbiAgICogVGVzdHMgaWYgdGhlIHNlbGVjdGVkIGl0ZW0gaXMgYSB2YWxpZCBzZWxlY3Rpb25cbiAgICovXG4gIGlzVmFsaWRTZWxlY3Rpb24oaXRlbTogYW55KTogYm9vbGVhbiB7XG4gICAgaWYgKCAhIHRoaXMub3B0aW9ucy5hbGxvd09ubHlWYWxpZFZhbHVlcyApIHtcbiAgICAgIHJldHVybiB0cnVlO1xuICAgIH1cblxuICAgIGlmIChpdGVtID09PSAnJyB8fCBpdGVtID09PSBudWxsIHx8IHR5cGVvZiBpdGVtLmxlbmd0aCA9PT0gJ3VuZGVmaW5lZCcgfHwgaXRlbS5sZW5ndGggPT09IDApIHtcbiAgICAgIHJldHVybiB0cnVlO1xuICAgIH1cblxuICAgIGNvbnN0IHZhbGlkS2V5cyA9IFtdO1xuXG4gICAgZm9yIChsZXQgaSA9IDA7IGkgPCB0aGlzLm9wdGlvbnMuaXRlbXMubGVuZ3RoOyBpKysgKSB7XG4gICAgICB2YWxpZEtleXMucHVzaCh0aGlzLm9wdGlvbnMuaXRlbXNbaV1bdGhpcy5vcHRpb25zLmlkZW50aWZpZXJdKTtcbiAgICB9XG5cbiAgICBpZiAoIHR5cGVvZiBpdGVtID09PSAnc3RyaW5nJyApIHtcbiAgICAgIHJldHVybiB2YWxpZEtleXMuaW5kZXhPZihpdGVtKSA+PSAwO1xuICAgIH1cblxuICAgIHJldHVybiB2YWxpZEtleXMuaW5kZXhPZihpdGVtW3RoaXMub3B0aW9ucy5pZGVudGlmaWVyXSkgPj0gMDtcbiAgfVxuXG4gIC8qKlxuICAgKiBUZXN0cyBpZiBhbiBpdGVtIGlzIHRoZSBzZWxlY3RlZCBpdGVtXG4gICAqL1xuICBpc0l0ZW1TZWxlY3RlZChpdGVtOiBhbnkpOiBib29sZWFuIHtcbiAgICByZXR1cm4gdGhpcy5nZXRFbGVtZW50VmFsdWUoKSA9PT0gaXRlbVt0aGlzLm9wdGlvbnMuaWRlbnRpZmllcl07XG4gIH1cblxuICAvKipcbiAgICogVGVzdHMgaWYgdGhlIGRyb3Bkb3duIGhhcyBhIHNlbGVjdGlvblxuICAgKi9cbiAgaXNTZWxlY3RlZCgpOiBib29sZWFuIHtcbiAgICBjb25zdCB2YWx1ZSA9IHRoaXMuZ2V0RWxlbWVudFZhbHVlKCk7XG4gICAgaWYgKHZhbHVlICYmICEgdGhpcy5zZWxlY3RlZEl0ZW0gJiYgdGhpcy5vcHRpb25zICYmIHRoaXMub3B0aW9ucy5pdGVtcyAmJiB0aGlzLm9wdGlvbnMuaXRlbXMubGVuZ3RoKSB7XG4gICAgICBmb3IgKGxldCBpID0gMDsgaSA8IHRoaXMub3B0aW9ucy5pdGVtcy5sZW5ndGg7IGkrKykge1xuICAgICAgICBpZiAoIHRoaXMub3B0aW9ucy5pdGVtc1tpXVt0aGlzLm9wdGlvbnMuaWRlbnRpZmllcl0gPT09IHZhbHVlKSB7XG4gICAgICAgICAgdGhpcy5zZWxlY3RlZEl0ZW0gPSB0aGlzLm9wdGlvbnMuaXRlbXNbaV07XG4gICAgICAgICAgYnJlYWs7XG4gICAgICAgIH1cbiAgICAgIH1cbiAgICB9XG4gICAgcmV0dXJuIHZhbHVlO1xuICB9XG5cbiAgLyoqXG4gICAqIEdldHMgdGhlIGxhYmVsIG9mIHRoZSBzZWxlY3RlZCBpdGVtXG4gICAqL1xuICBnZXRTZWxlY3RlZExhYmVsKCk6IHN0cmluZyB7XG4gICAgaWYgKHRoaXMuc2VsZWN0ZWRJdGVtICkge1xuICAgICAgcmV0dXJuIHRoaXMuc2VsZWN0ZWRJdGVtW3RoaXMub3B0aW9ucy5sYWJlbF07XG4gICAgfVxuICB9XG5cbiAgLyoqXG4gICAqIEdldHMgdGhlIGxhYmVsIG9mIHRoZSBzZWxlY3RlZCBpdGVtXG4gICAqL1xuICBnZXRTZWxlY3RlZEltYWdlKCk6IHN0cmluZyB7XG4gICAgaWYgKHRoaXMub3B0aW9ucy51c2VJbWFnZXMpIHtcbiAgICAgIGlmICh0aGlzLnNlbGVjdGVkSXRlbSApIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuc2VsZWN0ZWRJdGVtW3RoaXMub3B0aW9ucy5pbWFnZUtleV07XG4gICAgICB9XG4gICAgfVxuICB9XG5cbiAgLyoqXG4gICAqIE9uSW5pdCwgbGlzdGVkIGZvciBjbG9zZSBhbGwgZXZlbnRzXG4gICAqL1xuICBuZ09uSW5pdCgpOiB2b2lkIHtcbiAgICBpZiAodGhpcy5vcHRpb25zLmNsb3NlT3RoZXJzICkge1xuICAgICAgdGhpcy5jbG9zZUFsbERyb3Bkb3duc09ic2VydmVyID0gdGhpcy5kcm9wZG93blNlcnZpY2UuZ2V0Q2xvc2VBbGxEcm9wZG93bnNPYnNlcnZhYmxlKCkuc3Vic2NyaWJlKHZhbHVlID0+IHtcbiAgICAgICAgdGhpcy5pc09wZW4gPSBmYWxzZTtcbiAgICAgIH0pO1xuICAgIH1cblxuICAgIGlmICh0aGlzLm9wdGlvbnMuY2xvc2VPbkRvY3VtZW50Q2xpY2sgKSB7XG4gICAgICB0aGlzLmNsb3NlT25Eb2N1bWVudE9ic2VydmVyID0gdGhpcy5kcm9wZG93blNlcnZpY2UuZ2V0RG9jdW1lbnRDbGlja09ic2VydmVyKCkuc3Vic2NyaWJlKCgpID0+IHtcbiAgICAgICAgdGhpcy5pc09wZW4gPSBmYWxzZTtcbiAgICAgIH0pO1xuICAgIH1cblxuICAgIGlmICh0aGlzLmlzU2VsZWN0ZWQoKSApIHtcbiAgICAgIGlmICghIHRoaXMuaXNWYWxpZFNlbGVjdGlvbih0aGlzLmdldEVsZW1lbnRWYWx1ZSgpKSkge1xuICAgICAgICB0aGlzLmNsZWFyU2VsZWN0aW9uKCk7XG4gICAgICB9XG4gICAgfVxuXG4gICAgaWYgKHRoaXMuZWxlbWVudFJlZiBpbnN0YW5jZW9mIEZvcm1Db250cm9sKSB7XG4gICAgICB0aGlzLmVsZW1lbnRSZWYucmVnaXN0ZXJPbkNoYW5nZSgodmFsdWUpID0+IHtcbiAgICAgICAgaWYgKCAhdGhpcy5pc1ZhbGlkU2VsZWN0aW9uKHZhbHVlKSApIHtcbiAgICAgICAgICB0aGlzLmNsZWFyU2VsZWN0aW9uKCk7XG4gICAgICAgIH1cbiAgICAgIH0pO1xuICAgIH1cbiAgfVxuXG4gIC8qKlxuICAgKiBSZXNldHMgdGhlIHZhbHVlIG9mIHRoZSBlbGVtZW50XG4gICAqL1xuICBjbGVhclNlbGVjdGlvbigpOiB2b2lkIHtcbiAgICBjb25zdCB2YWx1ZSA9IHRoaXMuZ2V0RWxlbWVudFZhbHVlKCk7XG5cbiAgICBpZiAodHlwZW9mIHZhbHVlID09PSAnc3RyaW5nJykge1xuICAgICAgdGhpcy5zZXRFbGVtZW50VmFsdWUobnVsbCk7XG4gICAgfSBlbHNlIHtcbiAgICAgIHRoaXMuc2V0RWxlbWVudFZhbHVlKFtdKTtcbiAgICB9XG4gIH1cblxuICAvKipcbiAgICogT25EZXN0cm95LCBzdG9wIGxpc3RlbmluZyBmb3IgY2xvc2UgYWxsIGV2ZW50c1xuICAgKi9cbiAgbmdPbkRlc3Ryb3koKTogdm9pZCB7XG4gICAgaWYgKHRoaXMub3B0aW9ucy5jbG9zZU90aGVycyApIHtcbiAgICAgIHRoaXMuY2xvc2VBbGxEcm9wZG93bnNPYnNlcnZlci51bnN1YnNjcmliZSgpO1xuICAgIH1cblxuICAgIGlmICh0aGlzLm9wdGlvbnMuY2xvc2VPbkRvY3VtZW50Q2xpY2sgKSB7XG4gICAgICB0aGlzLmNsb3NlT25Eb2N1bWVudE9ic2VydmVyLnVuc3Vic2NyaWJlKCk7XG4gICAgfVxuICB9XG5cbiAgLyoqXG4gICAqIEdldHMgdGhlIGN1cnJlbnQgdmFsdWVcbiAgICovXG4gIGdldEVsZW1lbnRWYWx1ZSgpIHtcbiAgICBpZiAodGhpcy5lbGVtZW50UmVmIGluc3RhbmNlb2YgRm9ybUNvbnRyb2wgKSB7XG4gICAgICByZXR1cm4gdGhpcy5lbGVtZW50UmVmLnZhbHVlO1xuICAgIH1cblxuICAgIHJldHVybiB0aGlzLmVsZW1lbnRSZWY7XG4gIH1cblxuICAvKipcbiAgICogU2V0cyB0aGUgY3VycmVudCB2YWx1ZVxuICAgKi9cbiAgc2V0RWxlbWVudFZhbHVlKHZhbHVlOiBhbnkpIHtcbiAgICB0aGlzLmVsZW1lbnQgPSB2YWx1ZTtcbiAgfVxuXG4gIC8qKlxuICAgKiBTZXRzIHRoZSBlbGVtZW50XG4gICAqL1xuICBzZXQgZWxlbWVudCh2YWx1ZSkge1xuICAgIGlmICh0aGlzLmVsZW1lbnRSZWYgaW5zdGFuY2VvZiBGb3JtQ29udHJvbCkge1xuICAgICAgdGhpcy5lbGVtZW50UmVmLnNldFZhbHVlKHZhbHVlKTtcbiAgICB9IGVsc2Uge1xuICAgICAgdGhpcy5lbGVtZW50UmVmID0gdmFsdWU7XG4gICAgfVxuXG4gICAgdGhpcy5lbGVtZW50Q2hhbmdlLmVtaXQodmFsdWUpO1xuICB9XG5cbiAgLyoqXG4gICAqIEdldHMgdGhlIGVsZW1lbnRcbiAgICovXG4gIEBJbnB1dCgpXG4gIGdldCBlbGVtZW50KCkge1xuICAgIHJldHVybiB0aGlzLmdldEVsZW1lbnRWYWx1ZSgpO1xuICB9XG5cbiAgLyoqXG4gICAqIEZpbHRlciB0aGUgbGlzdCBvbiBjaGFuZ2VcbiAgICovXG4gIG9uRmlsdGVyQ2hhbmdlKCkge1xuICAgIGlmICghdGhpcy5maWx0ZXJNYXN0ZXJMaXN0KSB7XG4gICAgICB0aGlzLmZpbHRlck1hc3Rlckxpc3QgPSBKU09OLnBhcnNlKEpTT04uc3RyaW5naWZ5KHRoaXMub3B0aW9ucy5pdGVtcykpO1xuICAgIH1cblxuICAgIGlmICh0aGlzLmZpbHRlck1vZGVsID09PSAnJyB8fCAhIHRoaXMuZmlsdGVyTW9kZWwpIHtcbiAgICAgIHRoaXMub3B0aW9ucy5pdGVtcyA9IEpTT04ucGFyc2UoSlNPTi5zdHJpbmdpZnkodGhpcy5maWx0ZXJNYXN0ZXJMaXN0KSk7XG4gICAgfVxuXG4gICAgY29uc3QgdmFsdWUgPSB0aGlzLmZpbHRlck1vZGVsLnRvTG93ZXJDYXNlKCk7XG4gICAgdGhpcy5vcHRpb25zLml0ZW1zID0gdGhpcy5maWx0ZXJNYXN0ZXJMaXN0LmZpbHRlcigoaXRlbSkgPT4ge1xuICAgICAgcmV0dXJuIGl0ZW1bdGhpcy5vcHRpb25zLmxhYmVsXS50b0xvd2VyQ2FzZSgpLmluZGV4T2YodmFsdWUpID49IDA7XG4gICAgfSk7XG4gIH1cbn1cbiIsIjxkaXYgY2xhc3M9XCJkcm9wZG93blwiIFtuZ0NsYXNzXT1cIntvcGVuOiBpc09wZW4sIGFuaW1hdGVkOiBvcHRpb25zLmFuaW1hdGVkfVwiPlxuICA8ZGl2IGNsYXNzPVwiZHJvcGRvd24tZGlzcGxheVwiIChjbGljayk9XCJ0b2dnbGUoJGV2ZW50KVwiPlxuICAgIDxpIGNsYXNzPVwie3tvcHRpb25zLmNhcmV0Q2xhc3N9fVwiICpuZ0lmPVwib3B0aW9ucy5kaXNwbGF5UGxhY2Vob2xkZXJDYXJldEJlZm9yZSAmJiAhcGxhY2Vob2xkZXJDYXJhdFRlbXBsYXRlXCI+PC9pPlxuICAgIDxuZy10ZW1wbGF0ZSAqbmdJZj1cIm9wdGlvbnMuZGlzcGxheVBsYWNlaG9sZGVyQ2FyZXRCZWZvcmUgJiYgcGxhY2Vob2xkZXJDYXJhdFRlbXBsYXRlXCIgW25nVGVtcGxhdGVPdXRsZXRdPVwicGxhY2Vob2xkZXJDYXJhdFRlbXBsYXRlXCI+PC9uZy10ZW1wbGF0ZT5cbiAgICA8c3BhbiAqbmdJZj1cIiEgaXNTZWxlY3RlZCgpXCIgY2xhc3M9XCJwbGFjZWhvbGRlclwiPnt7IG9wdGlvbnMucGxhY2Vob2xkZXIgfX08L3NwYW4+XG4gICAgPHNwYW4gY2xhc3M9XCJpbWFnZVwiICpuZ0lmPVwiaXNTZWxlY3RlZCgpICYmIGdldFNlbGVjdGVkSW1hZ2UoKVwiPlxuICAgICAgPGltZyBbc3JjXT1cImdldFNlbGVjdGVkSW1hZ2UoKVwiLz5cbiAgICA8L3NwYW4+XG4gICAgPHNwYW4gY2xhc3M9XCJsYWJlbFwiICpuZ0lmPVwib3B0aW9ucy51c2VMYWJlbHNcIj5cbiAgICAgIDxzcGFuICpuZ0lmPVwiaXNTZWxlY3RlZCgpXCI+e3sgZ2V0U2VsZWN0ZWRMYWJlbCgpIH19PC9zcGFuPlxuICAgIDwvc3Bhbj5cbiAgICA8aSBjbGFzcz1cInt7b3B0aW9ucy5jYXJldENsYXNzfX1cIiAqbmdJZj1cIm9wdGlvbnMuZGlzcGxheVBsYWNlaG9sZGVyQ2FyZXRBZnRlciAmJiAhcGxhY2Vob2xkZXJDYXJhdFRlbXBsYXRlXCI+PC9pPlxuICAgIDxuZy10ZW1wbGF0ZSAqbmdJZj1cIm9wdGlvbnMuZGlzcGxheVBsYWNlaG9sZGVyQ2FyZXRBZnRlciAmJiBwbGFjZWhvbGRlckNhcmF0VGVtcGxhdGVcIiBbbmdUZW1wbGF0ZU91dGxldF09XCJwbGFjZWhvbGRlckNhcmF0VGVtcGxhdGVcIj48L25nLXRlbXBsYXRlPlxuICA8L2Rpdj5cbiAgPGRpdiBjbGFzcz1cImRyb3Bkb3duLWxpc3RcIj5cbiAgICA8ZGl2IGNsYXNzPVwiZHJvcGRvd24tbGlzdC1maWx0ZXJcIiAqbmdJZj1cIm9wdGlvbnMuZmlsdGVyYWJsZVwiPlxuICAgICAgPGlucHV0IFsobmdNb2RlbCldPVwiZmlsdGVyTW9kZWxcIiAobmdNb2RlbENoYW5nZSk9XCJvbkZpbHRlckNoYW5nZSgpXCIvPlxuICAgIDwvZGl2PlxuICAgIDxkaXYgY2xhc3M9XCJkcm9wZG93bi1saXN0LWl0ZW1cIiBbbmdDbGFzc109XCJ7c2VsZWN0ZWQ6aXNJdGVtU2VsZWN0ZWQob3B0aW9uKX1cIiAqbmdGb3I9XCJsZXQgb3B0aW9uIG9mIG9wdGlvbnMuaXRlbXNcIiAoY2xpY2spPVwic2VsZWN0SXRlbSgkZXZlbnQsIG9wdGlvbilcIj5cbiAgICAgIDxzcGFuIGNsYXNzPVwiaW1hZ2VcIiAqbmdJZj1cIm9wdGlvbnMudXNlSW1hZ2VzXCI+XG4gICAgICAgICA8c3Bhbj48aW1nIFtzcmNdPVwib3B0aW9uW29wdGlvbnMuaW1hZ2VLZXldXCIvPjwvc3Bhbj5cbiAgICAgICA8L3NwYW4+XG4gICAgICA8c3BhbiBjbGFzcz1cImxhYmVsXCIgKm5nSWY9XCJvcHRpb25zLnVzZUxhYmVsc1wiPlxuICAgICAgICA8c3Bhbj57e29wdGlvbltvcHRpb25zLmxhYmVsXX19PC9zcGFuPlxuICAgICAgPC9zcGFuPlxuICAgICAgIDxzcGFuIGNsYXNzPVwic2VsZWN0ZWQtaXRlbS1pbmRpY2F0b3JcIj5cbiAgICAgICAgIDxpIGNsYXNzPVwie3tvcHRpb25zLnNlbGVjdGVkSXRlbUNsYXNzfX1cIiAqbmdJZj1cIiFzZWxlY3RlZENhcmF0VGVtcGxhdGVcIj48L2k+XG4gICAgICAgICA8bmctdGVtcGxhdGUgKm5nSWY9XCJzZWxlY3RlZENhcmF0VGVtcGxhdGUgJiYgaXNJdGVtU2VsZWN0ZWQob3B0aW9uKVwiIFtuZ1RlbXBsYXRlT3V0bGV0XT1cInNlbGVjdGVkQ2FyYXRUZW1wbGF0ZVwiPjwvbmctdGVtcGxhdGU+XG4gICAgIDwvc3Bhbj5cbiAgICA8L2Rpdj5cbiAgPC9kaXY+XG48L2Rpdj5cbiJdfQ==