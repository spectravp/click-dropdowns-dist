export declare class DropdownOptionsModel {
    /**
     * These are the items to loop over in the drop down
     */
    items: any[];
    /**
     * The is what we should use to get the identifier of the selected item
     * Defaults to ID
     */
    identifier: string;
    /**
     * This is what we should use to get the display value of the selected item
     */
    label: string;
    /**
     * Placeholder to display
     */
    placeholder: string;
    /**
     * If true, adds the animated class to the dropdown
     */
    animated: boolean;
    /**
     * If true, will close other dropdowns when this one opens
     */
    closeOthers: boolean;
    /**
     * If true, will close when the document is clicked
     */
    closeOnDocumentClick: boolean;
    /**
     * If true, will close when they make a selection
     */
    closeOnSelection: boolean;
    /**
     * When true, will display the placeholder caret after the placeholder text
     */
    displayPlaceholderCaretAfter: boolean;
    /**
     * When true, will display the placeholder caret before the placeholder text
     */
    displayPlaceholderCaretBefore: boolean;
    /**
     * The CSS caret class (You can use font awesome classes, but I wouldn't include font awesome in this library)
     */
    caretClass: string;
    /**
     * The CSS selected item class (You can use font awesome classes, but I wouldn't include font awesome in this library)
     */
    selectedItemClass: string;
    /**
     * Used only for multi dropdowns, when true will use all of the selections as the placeholder label
     * When it is a string, it will use the string. When false, it will use the placeholder label
     */
    selectionsAsLabel: boolean | string;
    /**
     * When true, will use an image in the drop down, requires the imageKey
     */
    useImages: boolean;
    /**
     * Where in the object of options to get the image from
     */
    imageKey: string;
    /**
     * Flag to use labels, typically only used if you want to hide labels because you are using images
     */
    useLabels: boolean;
    /**
     * When true, it will only allow valid values. All other will be destroyed
     */
    allowOnlyValidValues: boolean;
    /**
     * Flag for if this is filterable
     */
    filterable: boolean;
    /**
     * If true, the placeholder will become a selectable item when something other than
     * the placeholder is selected.
     */
    usePlaceholderAsItem: boolean;
    /**
     * Constructor
     */
    constructor(items: any, options?: any);
}
//# sourceMappingURL=dropdown-options.model.d.ts.map