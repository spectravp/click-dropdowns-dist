import { Observable } from 'rxjs';
import * as i0 from "@angular/core";
export declare class DropdownService {
    /**
     * A singular subscription for closing all dropdowns in the app
     */
    private closeAllObserver;
    /**
     * A singuar subscription for document clicks
     */
    private documentClickObserver;
    /**
     * Constructor, init the observables
     */
    constructor();
    /**
     * Gets the document click observer
     */
    getDocumentClickObserver(): Observable<any>;
    /**
     * Gets the close all dropdowns observer
     */
    getCloseAllDropdownsObservable(): Observable<any>;
    /**
     * Will broadcast a close all to all observables
     */
    closeAllDropdowns(): void;
    static ɵfac: i0.ɵɵFactoryDeclaration<DropdownService, never>;
    static ɵprov: i0.ɵɵInjectableDeclaration<DropdownService>;
}
//# sourceMappingURL=dropdown.service.d.ts.map