import { EventEmitter, OnDestroy, OnInit, TemplateRef } from '@angular/core';
import { DropdownOptionsModel } from '../../models/dropdown-options.model';
import { Subscription } from 'rxjs';
import { DropdownService } from '../../services/dropdown.service';
import { FormControl } from '@angular/forms';
import * as i0 from "@angular/core";
export declare class DropdownComponent implements OnInit, OnDestroy {
    protected dropdownService: DropdownService;
    elementRef: FormControl | any;
    options: DropdownOptionsModel;
    elementChange: EventEmitter<any>;
    placeholderCaratTemplate: TemplateRef<any>;
    selectedCaratTemplate: TemplateRef<any>;
    /**
     * The selected item
     */
    protected selectedItem: any;
    /**
     * The delay to wait for close all subscriptions to complete
     */
    protected closeDelay: number;
    /**
     * An observer that listens for all close all requests
     */
    protected closeAllDropdownsObserver: Subscription;
    /**
     * An observer that listens for all document clicks
     */
    protected closeOnDocumentObserver: Subscription;
    /**
     * The filter model
     */
    filterModel: string;
    /**
     * Filterers master list
     */
    protected filterMasterList: any[];
    /**
     * Flag indicating whether the menu is open or not
     */
    isOpen: boolean;
    /**
     * Flag for whether the placeholder has been injected
     */
    private placeholderInjected;
    /**
     * Constructor / Dependency Injection
     */
    constructor(dropdownService: DropdownService);
    /**
     * Will request that all dropdowns close
     */
    protected closeAllDropDowns(): void;
    /**
     * Toggles this dropdown open/closed
     */
    toggle($event: Event): boolean;
    /**
     * Selects an item
     */
    selectItem($event: any, item: any): boolean;
    /**
     * Tests if the selected item is a valid selection
     */
    isValidSelection(item: any): boolean;
    /**
     * Tests if an item is the selected item
     */
    isItemSelected(item: any): boolean;
    /**
     * Tests if the dropdown has a selection
     */
    isSelected(): boolean;
    /**
     * Gets the label of the selected item
     */
    getSelectedLabel(): string;
    /**
     * Gets the label of the selected item
     */
    getSelectedImage(): string;
    /**
     * OnInit, listed for close all events
     */
    ngOnInit(): void;
    /**
     * Resets the value of the element
     */
    clearSelection(): void;
    /**
     * OnDestroy, stop listening for close all events
     */
    ngOnDestroy(): void;
    /**
     * Gets the current value
     */
    getElementValue(): any;
    /**
     * Sets the current value
     */
    setElementValue(value: any): void;
    /**
     * Sets the element
     */
    set element(value: any);
    /**
     * Gets the element
     */
    get element(): any;
    /**
     * Filter the list on change
     */
    onFilterChange(): void;
    static ɵfac: i0.ɵɵFactoryDeclaration<DropdownComponent, never>;
    static ɵcmp: i0.ɵɵComponentDeclaration<DropdownComponent, "click-dropdown", never, { "elementRef": "elementRef"; "options": "options"; "placeholderCaratTemplate": "placeholderCaratTemplate"; "selectedCaratTemplate": "selectedCaratTemplate"; "element": "element"; }, { "elementChange": "elementChange"; }, never, never, false>;
}
//# sourceMappingURL=dropdown.component.d.ts.map