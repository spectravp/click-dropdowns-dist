import { DropdownComponent } from '../dropdown/dropdown.component';
import { DropdownService } from '../../services/dropdown.service';
import * as i0 from "@angular/core";
export declare class DropdownMultiComponent extends DropdownComponent {
    protected dropdownService: DropdownService;
    /**
     * The actual model of selected values
     */
    protected multiModel: any[];
    /**
     * A model of selected labels
     */
    protected multiModelLabels: any[];
    /**
     * Constructor
     */
    constructor(dropdownService: DropdownService);
    /**
     * Selects an item
     */
    selectItem($event: Event, item: any): boolean;
    isValidSelection(item: any): boolean;
    clearSelection(): void;
    /**
     * Tests if an item is the selected item
     */
    isItemSelected(item: any): boolean;
    /**
     * Tests if the dropdown has a selection
     */
    isSelected(): boolean;
    /**
     * Gets the label of the selected item
     */
    getSelectedLabel(): any;
    /**
     * Gets the label of the selected item
     */
    getSelectedImage(): string;
    static ɵfac: i0.ɵɵFactoryDeclaration<DropdownMultiComponent, never>;
    static ɵcmp: i0.ɵɵComponentDeclaration<DropdownMultiComponent, "click-dropdown-multi", never, {}, {}, never, never, false>;
}
//# sourceMappingURL=dropdown-multi.component.d.ts.map