import * as i0 from "@angular/core";
import * as i1 from "./components/dropdown/dropdown.component";
import * as i2 from "./components/dropdown-multi/dropdown-multi.component";
import * as i3 from "@angular/forms";
import * as i4 from "@angular/common";
export declare class DropdownsModule {
    static ɵfac: i0.ɵɵFactoryDeclaration<DropdownsModule, never>;
    static ɵmod: i0.ɵɵNgModuleDeclaration<DropdownsModule, [typeof i1.DropdownComponent, typeof i2.DropdownMultiComponent], [typeof i3.FormsModule, typeof i3.ReactiveFormsModule, typeof i4.CommonModule], [typeof i1.DropdownComponent, typeof i2.DropdownMultiComponent]>;
    static ɵinj: i0.ɵɵInjectorDeclaration<DropdownsModule>;
}
//# sourceMappingURL=dropdowns.module.d.ts.map