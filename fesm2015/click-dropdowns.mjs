import * as i0 from '@angular/core';
import { Injectable, EventEmitter, Component, Input, Output, NgModule } from '@angular/core';
import * as i2 from '@angular/forms';
import { FormControl, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Subject, fromEvent } from 'rxjs';
import * as i3 from '@angular/common';
import { CommonModule } from '@angular/common';

class DropdownService {
    /**
     * Constructor, init the observables
     */
    constructor() {
        this.closeAllObserver = new Subject();
        this.documentClickObserver = fromEvent(document, 'click');
    }
    /**
     * Gets the document click observer
     */
    getDocumentClickObserver() {
        return this.documentClickObserver;
    }
    /**
     * Gets the close all dropdowns observer
     */
    getCloseAllDropdownsObservable() {
        return this.closeAllObserver.asObservable();
    }
    /**
     * Will broadcast a close all to all observables
     */
    closeAllDropdowns() {
        this.closeAllObserver.next(new Date());
    }
}
DropdownService.ɵfac = function DropdownService_Factory(t) { return new (t || DropdownService)(); };
DropdownService.ɵprov = /*@__PURE__*/ i0.ɵɵdefineInjectable({ token: DropdownService, factory: DropdownService.ɵfac, providedIn: 'root' });
(function () {
    (typeof ngDevMode === "undefined" || ngDevMode) && i0.ɵsetClassMetadata(DropdownService, [{
            type: Injectable,
            args: [{
                    providedIn: 'root'
                }]
        }], function () { return []; }, null);
})();

function DropdownComponent_i_2_Template(rf, ctx) {
    if (rf & 1) {
        i0.ɵɵelement(0, "i");
    }
    if (rf & 2) {
        const ctx_r0 = i0.ɵɵnextContext();
        i0.ɵɵclassMap(ctx_r0.options.caretClass);
    }
}
function DropdownComponent_3_ng_template_0_Template(rf, ctx) { }
function DropdownComponent_3_Template(rf, ctx) {
    if (rf & 1) {
        i0.ɵɵtemplate(0, DropdownComponent_3_ng_template_0_Template, 0, 0, "ng-template", 10);
    }
    if (rf & 2) {
        const ctx_r1 = i0.ɵɵnextContext();
        i0.ɵɵproperty("ngTemplateOutlet", ctx_r1.placeholderCaratTemplate);
    }
}
function DropdownComponent_span_4_Template(rf, ctx) {
    if (rf & 1) {
        i0.ɵɵelementStart(0, "span", 11);
        i0.ɵɵtext(1);
        i0.ɵɵelementEnd();
    }
    if (rf & 2) {
        const ctx_r2 = i0.ɵɵnextContext();
        i0.ɵɵadvance(1);
        i0.ɵɵtextInterpolate(ctx_r2.options.placeholder);
    }
}
function DropdownComponent_span_5_Template(rf, ctx) {
    if (rf & 1) {
        i0.ɵɵelementStart(0, "span", 12);
        i0.ɵɵelement(1, "img", 13);
        i0.ɵɵelementEnd();
    }
    if (rf & 2) {
        const ctx_r3 = i0.ɵɵnextContext();
        i0.ɵɵadvance(1);
        i0.ɵɵproperty("src", ctx_r3.getSelectedImage(), i0.ɵɵsanitizeUrl);
    }
}
function DropdownComponent_span_6_span_1_Template(rf, ctx) {
    if (rf & 1) {
        i0.ɵɵelementStart(0, "span");
        i0.ɵɵtext(1);
        i0.ɵɵelementEnd();
    }
    if (rf & 2) {
        const ctx_r10 = i0.ɵɵnextContext(2);
        i0.ɵɵadvance(1);
        i0.ɵɵtextInterpolate(ctx_r10.getSelectedLabel());
    }
}
function DropdownComponent_span_6_Template(rf, ctx) {
    if (rf & 1) {
        i0.ɵɵelementStart(0, "span", 14);
        i0.ɵɵtemplate(1, DropdownComponent_span_6_span_1_Template, 2, 1, "span", 3);
        i0.ɵɵelementEnd();
    }
    if (rf & 2) {
        const ctx_r4 = i0.ɵɵnextContext();
        i0.ɵɵadvance(1);
        i0.ɵɵproperty("ngIf", ctx_r4.isSelected());
    }
}
function DropdownComponent_i_7_Template(rf, ctx) {
    if (rf & 1) {
        i0.ɵɵelement(0, "i");
    }
    if (rf & 2) {
        const ctx_r5 = i0.ɵɵnextContext();
        i0.ɵɵclassMap(ctx_r5.options.caretClass);
    }
}
function DropdownComponent_8_ng_template_0_Template(rf, ctx) { }
function DropdownComponent_8_Template(rf, ctx) {
    if (rf & 1) {
        i0.ɵɵtemplate(0, DropdownComponent_8_ng_template_0_Template, 0, 0, "ng-template", 10);
    }
    if (rf & 2) {
        const ctx_r6 = i0.ɵɵnextContext();
        i0.ɵɵproperty("ngTemplateOutlet", ctx_r6.placeholderCaratTemplate);
    }
}
function DropdownComponent_div_10_Template(rf, ctx) {
    if (rf & 1) {
        const _r13 = i0.ɵɵgetCurrentView();
        i0.ɵɵelementStart(0, "div", 15)(1, "input", 16);
        i0.ɵɵlistener("ngModelChange", function DropdownComponent_div_10_Template_input_ngModelChange_1_listener($event) { i0.ɵɵrestoreView(_r13); const ctx_r12 = i0.ɵɵnextContext(); return i0.ɵɵresetView(ctx_r12.filterModel = $event); })("ngModelChange", function DropdownComponent_div_10_Template_input_ngModelChange_1_listener() { i0.ɵɵrestoreView(_r13); const ctx_r14 = i0.ɵɵnextContext(); return i0.ɵɵresetView(ctx_r14.onFilterChange()); });
        i0.ɵɵelementEnd()();
    }
    if (rf & 2) {
        const ctx_r7 = i0.ɵɵnextContext();
        i0.ɵɵadvance(1);
        i0.ɵɵproperty("ngModel", ctx_r7.filterModel);
    }
}
function DropdownComponent_div_11_span_1_Template(rf, ctx) {
    if (rf & 1) {
        i0.ɵɵelementStart(0, "span", 12)(1, "span");
        i0.ɵɵelement(2, "img", 13);
        i0.ɵɵelementEnd()();
    }
    if (rf & 2) {
        const option_r15 = i0.ɵɵnextContext().$implicit;
        const ctx_r16 = i0.ɵɵnextContext();
        i0.ɵɵadvance(2);
        i0.ɵɵproperty("src", option_r15[ctx_r16.options.imageKey], i0.ɵɵsanitizeUrl);
    }
}
function DropdownComponent_div_11_span_2_Template(rf, ctx) {
    if (rf & 1) {
        i0.ɵɵelementStart(0, "span", 14)(1, "span");
        i0.ɵɵtext(2);
        i0.ɵɵelementEnd()();
    }
    if (rf & 2) {
        const option_r15 = i0.ɵɵnextContext().$implicit;
        const ctx_r17 = i0.ɵɵnextContext();
        i0.ɵɵadvance(2);
        i0.ɵɵtextInterpolate(option_r15[ctx_r17.options.label]);
    }
}
function DropdownComponent_div_11_i_4_Template(rf, ctx) {
    if (rf & 1) {
        i0.ɵɵelement(0, "i");
    }
    if (rf & 2) {
        const ctx_r18 = i0.ɵɵnextContext(2);
        i0.ɵɵclassMap(ctx_r18.options.selectedItemClass);
    }
}
function DropdownComponent_div_11_5_ng_template_0_Template(rf, ctx) { }
function DropdownComponent_div_11_5_Template(rf, ctx) {
    if (rf & 1) {
        i0.ɵɵtemplate(0, DropdownComponent_div_11_5_ng_template_0_Template, 0, 0, "ng-template", 10);
    }
    if (rf & 2) {
        const ctx_r19 = i0.ɵɵnextContext(2);
        i0.ɵɵproperty("ngTemplateOutlet", ctx_r19.selectedCaratTemplate);
    }
}
const _c0$1 = function (a0) { return { selected: a0 }; };
function DropdownComponent_div_11_Template(rf, ctx) {
    if (rf & 1) {
        const _r24 = i0.ɵɵgetCurrentView();
        i0.ɵɵelementStart(0, "div", 17);
        i0.ɵɵlistener("click", function DropdownComponent_div_11_Template_div_click_0_listener($event) { const restoredCtx = i0.ɵɵrestoreView(_r24); const option_r15 = restoredCtx.$implicit; const ctx_r23 = i0.ɵɵnextContext(); return i0.ɵɵresetView(ctx_r23.selectItem($event, option_r15)); });
        i0.ɵɵtemplate(1, DropdownComponent_div_11_span_1_Template, 3, 1, "span", 5);
        i0.ɵɵtemplate(2, DropdownComponent_div_11_span_2_Template, 3, 1, "span", 6);
        i0.ɵɵelementStart(3, "span", 18);
        i0.ɵɵtemplate(4, DropdownComponent_div_11_i_4_Template, 1, 3, "i", 2);
        i0.ɵɵtemplate(5, DropdownComponent_div_11_5_Template, 1, 1, null, 3);
        i0.ɵɵelementEnd()();
    }
    if (rf & 2) {
        const option_r15 = ctx.$implicit;
        const ctx_r8 = i0.ɵɵnextContext();
        i0.ɵɵproperty("ngClass", i0.ɵɵpureFunction1(5, _c0$1, ctx_r8.isItemSelected(option_r15)));
        i0.ɵɵadvance(1);
        i0.ɵɵproperty("ngIf", ctx_r8.options.useImages);
        i0.ɵɵadvance(1);
        i0.ɵɵproperty("ngIf", ctx_r8.options.useLabels);
        i0.ɵɵadvance(2);
        i0.ɵɵproperty("ngIf", !ctx_r8.selectedCaratTemplate);
        i0.ɵɵadvance(1);
        i0.ɵɵproperty("ngIf", ctx_r8.selectedCaratTemplate && ctx_r8.isItemSelected(option_r15));
    }
}
const _c1$1 = function (a0, a1) { return { open: a0, animated: a1 }; };
class DropdownComponent {
    /**
     * Constructor / Dependency Injection
     */
    constructor(dropdownService) {
        this.dropdownService = dropdownService;
        this.elementChange = new EventEmitter();
        /**
         * The delay to wait for close all subscriptions to complete
         */
        this.closeDelay = 1;
        /**
         * The filter model
         */
        this.filterModel = '';
        /**
         * Flag indicating whether the menu is open or not
         */
        this.isOpen = false;
        /**
         * Flag for whether the placeholder has been injected
         */
        this.placeholderInjected = false;
    }
    /**
     * Will request that all dropdowns close
     */
    closeAllDropDowns() {
        this.dropdownService.closeAllDropdowns();
    }
    /**
     * Toggles this dropdown open/closed
     */
    toggle($event) {
        $event.stopImmediatePropagation();
        $event.stopPropagation();
        if (!this.isOpen && this.options.closeOthers) {
            this.closeAllDropDowns();
            setTimeout(() => {
                this.isOpen = true;
            }, this.closeDelay);
            return false;
        }
        this.isOpen = !this.isOpen;
        return false;
    }
    /**
     * Selects an item
     */
    selectItem($event, item) {
        $event.stopImmediatePropagation();
        $event.stopPropagation();
        if (!this.isValidSelection(item)) {
            return;
        }
        if (this.options.closeOnSelection) {
            this.isOpen = false;
        }
        if (this.getElementValue() === item[this.options.identifier] || item.isPlaceholder) {
            this.selectedItem = null;
            this.setElementValue(null);
            if (item.isPlaceholder) {
                this.options.items.shift();
                this.placeholderInjected = false;
            }
            return false;
        }
        this.selectedItem = item;
        this.setElementValue(item[this.options.identifier]);
        if (this.options.usePlaceholderAsItem && !this.placeholderInjected) {
            const props = JSON.parse(`{
        "${this.options.identifier}":null,
        "${this.options.label}":"${this.options.placeholder}",
        "isPlaceholder":true}`);
            this.options.items.unshift(props);
            this.placeholderInjected = true;
        }
        return false;
    }
    /**
     * Tests if the selected item is a valid selection
     */
    isValidSelection(item) {
        if (!this.options.allowOnlyValidValues) {
            return true;
        }
        if (item === '' || item === null || typeof item.length === 'undefined' || item.length === 0) {
            return true;
        }
        const validKeys = [];
        for (let i = 0; i < this.options.items.length; i++) {
            validKeys.push(this.options.items[i][this.options.identifier]);
        }
        if (typeof item === 'string') {
            return validKeys.indexOf(item) >= 0;
        }
        return validKeys.indexOf(item[this.options.identifier]) >= 0;
    }
    /**
     * Tests if an item is the selected item
     */
    isItemSelected(item) {
        return this.getElementValue() === item[this.options.identifier];
    }
    /**
     * Tests if the dropdown has a selection
     */
    isSelected() {
        const value = this.getElementValue();
        if (value && !this.selectedItem && this.options && this.options.items && this.options.items.length) {
            for (let i = 0; i < this.options.items.length; i++) {
                if (this.options.items[i][this.options.identifier] === value) {
                    this.selectedItem = this.options.items[i];
                    break;
                }
            }
        }
        return value;
    }
    /**
     * Gets the label of the selected item
     */
    getSelectedLabel() {
        if (this.selectedItem) {
            return this.selectedItem[this.options.label];
        }
    }
    /**
     * Gets the label of the selected item
     */
    getSelectedImage() {
        if (this.options.useImages) {
            if (this.selectedItem) {
                return this.selectedItem[this.options.imageKey];
            }
        }
    }
    /**
     * OnInit, listed for close all events
     */
    ngOnInit() {
        if (this.options.closeOthers) {
            this.closeAllDropdownsObserver = this.dropdownService.getCloseAllDropdownsObservable().subscribe(value => {
                this.isOpen = false;
            });
        }
        if (this.options.closeOnDocumentClick) {
            this.closeOnDocumentObserver = this.dropdownService.getDocumentClickObserver().subscribe(() => {
                this.isOpen = false;
            });
        }
        if (this.isSelected()) {
            if (!this.isValidSelection(this.getElementValue())) {
                this.clearSelection();
            }
        }
        if (this.elementRef instanceof FormControl) {
            this.elementRef.registerOnChange((value) => {
                if (!this.isValidSelection(value)) {
                    this.clearSelection();
                }
            });
        }
    }
    /**
     * Resets the value of the element
     */
    clearSelection() {
        const value = this.getElementValue();
        if (typeof value === 'string') {
            this.setElementValue(null);
        }
        else {
            this.setElementValue([]);
        }
    }
    /**
     * OnDestroy, stop listening for close all events
     */
    ngOnDestroy() {
        if (this.options.closeOthers) {
            this.closeAllDropdownsObserver.unsubscribe();
        }
        if (this.options.closeOnDocumentClick) {
            this.closeOnDocumentObserver.unsubscribe();
        }
    }
    /**
     * Gets the current value
     */
    getElementValue() {
        if (this.elementRef instanceof FormControl) {
            return this.elementRef.value;
        }
        return this.elementRef;
    }
    /**
     * Sets the current value
     */
    setElementValue(value) {
        this.element = value;
    }
    /**
     * Sets the element
     */
    set element(value) {
        if (this.elementRef instanceof FormControl) {
            this.elementRef.setValue(value);
        }
        else {
            this.elementRef = value;
        }
        this.elementChange.emit(value);
    }
    /**
     * Gets the element
     */
    get element() {
        return this.getElementValue();
    }
    /**
     * Filter the list on change
     */
    onFilterChange() {
        if (!this.filterMasterList) {
            this.filterMasterList = JSON.parse(JSON.stringify(this.options.items));
        }
        if (this.filterModel === '' || !this.filterModel) {
            this.options.items = JSON.parse(JSON.stringify(this.filterMasterList));
        }
        const value = this.filterModel.toLowerCase();
        this.options.items = this.filterMasterList.filter((item) => {
            return item[this.options.label].toLowerCase().indexOf(value) >= 0;
        });
    }
}
DropdownComponent.ɵfac = function DropdownComponent_Factory(t) { return new (t || DropdownComponent)(i0.ɵɵdirectiveInject(DropdownService)); };
DropdownComponent.ɵcmp = /*@__PURE__*/ i0.ɵɵdefineComponent({ type: DropdownComponent, selectors: [["click-dropdown"]], inputs: { elementRef: "elementRef", options: "options", placeholderCaratTemplate: "placeholderCaratTemplate", selectedCaratTemplate: "selectedCaratTemplate", element: "element" }, outputs: { elementChange: "elementChange" }, decls: 12, vars: 13, consts: [[1, "dropdown", 3, "ngClass"], [1, "dropdown-display", 3, "click"], [3, "class", 4, "ngIf"], [4, "ngIf"], ["class", "placeholder", 4, "ngIf"], ["class", "image", 4, "ngIf"], ["class", "label", 4, "ngIf"], [1, "dropdown-list"], ["class", "dropdown-list-filter", 4, "ngIf"], ["class", "dropdown-list-item", 3, "ngClass", "click", 4, "ngFor", "ngForOf"], [3, "ngTemplateOutlet"], [1, "placeholder"], [1, "image"], [3, "src"], [1, "label"], [1, "dropdown-list-filter"], [3, "ngModel", "ngModelChange"], [1, "dropdown-list-item", 3, "ngClass", "click"], [1, "selected-item-indicator"]], template: function DropdownComponent_Template(rf, ctx) {
        if (rf & 1) {
            i0.ɵɵelementStart(0, "div", 0)(1, "div", 1);
            i0.ɵɵlistener("click", function DropdownComponent_Template_div_click_1_listener($event) { return ctx.toggle($event); });
            i0.ɵɵtemplate(2, DropdownComponent_i_2_Template, 1, 3, "i", 2);
            i0.ɵɵtemplate(3, DropdownComponent_3_Template, 1, 1, null, 3);
            i0.ɵɵtemplate(4, DropdownComponent_span_4_Template, 2, 1, "span", 4);
            i0.ɵɵtemplate(5, DropdownComponent_span_5_Template, 2, 1, "span", 5);
            i0.ɵɵtemplate(6, DropdownComponent_span_6_Template, 2, 1, "span", 6);
            i0.ɵɵtemplate(7, DropdownComponent_i_7_Template, 1, 3, "i", 2);
            i0.ɵɵtemplate(8, DropdownComponent_8_Template, 1, 1, null, 3);
            i0.ɵɵelementEnd();
            i0.ɵɵelementStart(9, "div", 7);
            i0.ɵɵtemplate(10, DropdownComponent_div_10_Template, 2, 1, "div", 8);
            i0.ɵɵtemplate(11, DropdownComponent_div_11_Template, 6, 7, "div", 9);
            i0.ɵɵelementEnd()();
        }
        if (rf & 2) {
            i0.ɵɵproperty("ngClass", i0.ɵɵpureFunction2(10, _c1$1, ctx.isOpen, ctx.options.animated));
            i0.ɵɵadvance(2);
            i0.ɵɵproperty("ngIf", ctx.options.displayPlaceholderCaretBefore && !ctx.placeholderCaratTemplate);
            i0.ɵɵadvance(1);
            i0.ɵɵproperty("ngIf", ctx.options.displayPlaceholderCaretBefore && ctx.placeholderCaratTemplate);
            i0.ɵɵadvance(1);
            i0.ɵɵproperty("ngIf", !ctx.isSelected());
            i0.ɵɵadvance(1);
            i0.ɵɵproperty("ngIf", ctx.isSelected() && ctx.getSelectedImage());
            i0.ɵɵadvance(1);
            i0.ɵɵproperty("ngIf", ctx.options.useLabels);
            i0.ɵɵadvance(1);
            i0.ɵɵproperty("ngIf", ctx.options.displayPlaceholderCaretAfter && !ctx.placeholderCaratTemplate);
            i0.ɵɵadvance(1);
            i0.ɵɵproperty("ngIf", ctx.options.displayPlaceholderCaretAfter && ctx.placeholderCaratTemplate);
            i0.ɵɵadvance(2);
            i0.ɵɵproperty("ngIf", ctx.options.filterable);
            i0.ɵɵadvance(1);
            i0.ɵɵproperty("ngForOf", ctx.options.items);
        }
    }, dependencies: [i2.DefaultValueAccessor, i2.NgControlStatus, i2.NgModel, i3.NgClass, i3.NgForOf, i3.NgIf, i3.NgTemplateOutlet], encapsulation: 2 });
(function () {
    (typeof ngDevMode === "undefined" || ngDevMode) && i0.ɵsetClassMetadata(DropdownComponent, [{
            type: Component,
            args: [{ selector: 'click-dropdown', template: "<div class=\"dropdown\" [ngClass]=\"{open: isOpen, animated: options.animated}\">\n  <div class=\"dropdown-display\" (click)=\"toggle($event)\">\n    <i class=\"{{options.caretClass}}\" *ngIf=\"options.displayPlaceholderCaretBefore && !placeholderCaratTemplate\"></i>\n    <ng-template *ngIf=\"options.displayPlaceholderCaretBefore && placeholderCaratTemplate\" [ngTemplateOutlet]=\"placeholderCaratTemplate\"></ng-template>\n    <span *ngIf=\"! isSelected()\" class=\"placeholder\">{{ options.placeholder }}</span>\n    <span class=\"image\" *ngIf=\"isSelected() && getSelectedImage()\">\n      <img [src]=\"getSelectedImage()\"/>\n    </span>\n    <span class=\"label\" *ngIf=\"options.useLabels\">\n      <span *ngIf=\"isSelected()\">{{ getSelectedLabel() }}</span>\n    </span>\n    <i class=\"{{options.caretClass}}\" *ngIf=\"options.displayPlaceholderCaretAfter && !placeholderCaratTemplate\"></i>\n    <ng-template *ngIf=\"options.displayPlaceholderCaretAfter && placeholderCaratTemplate\" [ngTemplateOutlet]=\"placeholderCaratTemplate\"></ng-template>\n  </div>\n  <div class=\"dropdown-list\">\n    <div class=\"dropdown-list-filter\" *ngIf=\"options.filterable\">\n      <input [(ngModel)]=\"filterModel\" (ngModelChange)=\"onFilterChange()\"/>\n    </div>\n    <div class=\"dropdown-list-item\" [ngClass]=\"{selected:isItemSelected(option)}\" *ngFor=\"let option of options.items\" (click)=\"selectItem($event, option)\">\n      <span class=\"image\" *ngIf=\"options.useImages\">\n         <span><img [src]=\"option[options.imageKey]\"/></span>\n       </span>\n      <span class=\"label\" *ngIf=\"options.useLabels\">\n        <span>{{option[options.label]}}</span>\n      </span>\n       <span class=\"selected-item-indicator\">\n         <i class=\"{{options.selectedItemClass}}\" *ngIf=\"!selectedCaratTemplate\"></i>\n         <ng-template *ngIf=\"selectedCaratTemplate && isItemSelected(option)\" [ngTemplateOutlet]=\"selectedCaratTemplate\"></ng-template>\n     </span>\n    </div>\n  </div>\n</div>\n" }]
        }], function () { return [{ type: DropdownService }]; }, { elementRef: [{
                type: Input
            }], options: [{
                type: Input
            }], elementChange: [{
                type: Output
            }], placeholderCaratTemplate: [{
                type: Input
            }], selectedCaratTemplate: [{
                type: Input
            }], element: [{
                type: Input
            }] });
})();

function DropdownMultiComponent_i_2_Template(rf, ctx) {
    if (rf & 1) {
        i0.ɵɵelement(0, "i");
    }
    if (rf & 2) {
        const ctx_r0 = i0.ɵɵnextContext();
        i0.ɵɵclassMap(ctx_r0.options.caretClass);
    }
}
function DropdownMultiComponent_3_ng_template_0_Template(rf, ctx) { }
function DropdownMultiComponent_3_Template(rf, ctx) {
    if (rf & 1) {
        i0.ɵɵtemplate(0, DropdownMultiComponent_3_ng_template_0_Template, 0, 0, "ng-template", 10);
    }
    if (rf & 2) {
        const ctx_r1 = i0.ɵɵnextContext();
        i0.ɵɵproperty("ngTemplateOutlet", ctx_r1.placeholderCaratTemplate);
    }
}
function DropdownMultiComponent_span_4_Template(rf, ctx) {
    if (rf & 1) {
        i0.ɵɵelementStart(0, "span", 11);
        i0.ɵɵtext(1);
        i0.ɵɵelementEnd();
    }
    if (rf & 2) {
        const ctx_r2 = i0.ɵɵnextContext();
        i0.ɵɵadvance(1);
        i0.ɵɵtextInterpolate(ctx_r2.options.placeholder);
    }
}
function DropdownMultiComponent_span_5_Template(rf, ctx) {
    if (rf & 1) {
        i0.ɵɵelementStart(0, "span", 12);
        i0.ɵɵelement(1, "img", 13);
        i0.ɵɵelementEnd();
    }
    if (rf & 2) {
        const ctx_r3 = i0.ɵɵnextContext();
        i0.ɵɵadvance(1);
        i0.ɵɵproperty("src", ctx_r3.getSelectedImage(), i0.ɵɵsanitizeUrl);
    }
}
function DropdownMultiComponent_span_6_span_1_Template(rf, ctx) {
    if (rf & 1) {
        i0.ɵɵelementStart(0, "span");
        i0.ɵɵtext(1);
        i0.ɵɵelementEnd();
    }
    if (rf & 2) {
        const ctx_r10 = i0.ɵɵnextContext(2);
        i0.ɵɵadvance(1);
        i0.ɵɵtextInterpolate(ctx_r10.getSelectedLabel());
    }
}
function DropdownMultiComponent_span_6_Template(rf, ctx) {
    if (rf & 1) {
        i0.ɵɵelementStart(0, "span", 14);
        i0.ɵɵtemplate(1, DropdownMultiComponent_span_6_span_1_Template, 2, 1, "span", 3);
        i0.ɵɵelementEnd();
    }
    if (rf & 2) {
        const ctx_r4 = i0.ɵɵnextContext();
        i0.ɵɵadvance(1);
        i0.ɵɵproperty("ngIf", ctx_r4.isSelected());
    }
}
function DropdownMultiComponent_i_7_Template(rf, ctx) {
    if (rf & 1) {
        i0.ɵɵelement(0, "i");
    }
    if (rf & 2) {
        const ctx_r5 = i0.ɵɵnextContext();
        i0.ɵɵclassMap(ctx_r5.options.caretClass);
    }
}
function DropdownMultiComponent_8_ng_template_0_Template(rf, ctx) { }
function DropdownMultiComponent_8_Template(rf, ctx) {
    if (rf & 1) {
        i0.ɵɵtemplate(0, DropdownMultiComponent_8_ng_template_0_Template, 0, 0, "ng-template", 10);
    }
    if (rf & 2) {
        const ctx_r6 = i0.ɵɵnextContext();
        i0.ɵɵproperty("ngTemplateOutlet", ctx_r6.placeholderCaratTemplate);
    }
}
function DropdownMultiComponent_div_10_Template(rf, ctx) {
    if (rf & 1) {
        const _r13 = i0.ɵɵgetCurrentView();
        i0.ɵɵelementStart(0, "div", 15)(1, "input", 16);
        i0.ɵɵlistener("ngModelChange", function DropdownMultiComponent_div_10_Template_input_ngModelChange_1_listener($event) { i0.ɵɵrestoreView(_r13); const ctx_r12 = i0.ɵɵnextContext(); return i0.ɵɵresetView(ctx_r12.filterModel = $event); })("ngModelChange", function DropdownMultiComponent_div_10_Template_input_ngModelChange_1_listener() { i0.ɵɵrestoreView(_r13); const ctx_r14 = i0.ɵɵnextContext(); return i0.ɵɵresetView(ctx_r14.onFilterChange()); });
        i0.ɵɵelementEnd()();
    }
    if (rf & 2) {
        const ctx_r7 = i0.ɵɵnextContext();
        i0.ɵɵadvance(1);
        i0.ɵɵproperty("ngModel", ctx_r7.filterModel);
    }
}
function DropdownMultiComponent_div_11_span_1_Template(rf, ctx) {
    if (rf & 1) {
        i0.ɵɵelementStart(0, "span", 12)(1, "span");
        i0.ɵɵelement(2, "img", 13);
        i0.ɵɵelementEnd()();
    }
    if (rf & 2) {
        const option_r15 = i0.ɵɵnextContext().$implicit;
        const ctx_r16 = i0.ɵɵnextContext();
        i0.ɵɵadvance(2);
        i0.ɵɵproperty("src", option_r15[ctx_r16.options.imageKey], i0.ɵɵsanitizeUrl);
    }
}
function DropdownMultiComponent_div_11_span_2_Template(rf, ctx) {
    if (rf & 1) {
        i0.ɵɵelementStart(0, "span", 14)(1, "span");
        i0.ɵɵtext(2);
        i0.ɵɵelementEnd()();
    }
    if (rf & 2) {
        const option_r15 = i0.ɵɵnextContext().$implicit;
        const ctx_r17 = i0.ɵɵnextContext();
        i0.ɵɵadvance(2);
        i0.ɵɵtextInterpolate(option_r15[ctx_r17.options.label]);
    }
}
function DropdownMultiComponent_div_11_i_4_Template(rf, ctx) {
    if (rf & 1) {
        i0.ɵɵelement(0, "i");
    }
    if (rf & 2) {
        const ctx_r18 = i0.ɵɵnextContext(2);
        i0.ɵɵclassMap(ctx_r18.options.selectedItemClass);
    }
}
function DropdownMultiComponent_div_11_5_ng_template_0_Template(rf, ctx) { }
function DropdownMultiComponent_div_11_5_Template(rf, ctx) {
    if (rf & 1) {
        i0.ɵɵtemplate(0, DropdownMultiComponent_div_11_5_ng_template_0_Template, 0, 0, "ng-template", 10);
    }
    if (rf & 2) {
        const ctx_r19 = i0.ɵɵnextContext(2);
        i0.ɵɵproperty("ngTemplateOutlet", ctx_r19.selectedCaratTemplate);
    }
}
const _c0 = function (a0) { return { selected: a0 }; };
function DropdownMultiComponent_div_11_Template(rf, ctx) {
    if (rf & 1) {
        const _r24 = i0.ɵɵgetCurrentView();
        i0.ɵɵelementStart(0, "div", 17);
        i0.ɵɵlistener("click", function DropdownMultiComponent_div_11_Template_div_click_0_listener($event) { const restoredCtx = i0.ɵɵrestoreView(_r24); const option_r15 = restoredCtx.$implicit; const ctx_r23 = i0.ɵɵnextContext(); return i0.ɵɵresetView(ctx_r23.selectItem($event, option_r15)); });
        i0.ɵɵtemplate(1, DropdownMultiComponent_div_11_span_1_Template, 3, 1, "span", 5);
        i0.ɵɵtemplate(2, DropdownMultiComponent_div_11_span_2_Template, 3, 1, "span", 6);
        i0.ɵɵelementStart(3, "span", 18);
        i0.ɵɵtemplate(4, DropdownMultiComponent_div_11_i_4_Template, 1, 3, "i", 2);
        i0.ɵɵtemplate(5, DropdownMultiComponent_div_11_5_Template, 1, 1, null, 3);
        i0.ɵɵelementEnd()();
    }
    if (rf & 2) {
        const option_r15 = ctx.$implicit;
        const ctx_r8 = i0.ɵɵnextContext();
        i0.ɵɵproperty("ngClass", i0.ɵɵpureFunction1(5, _c0, ctx_r8.isItemSelected(option_r15)));
        i0.ɵɵadvance(1);
        i0.ɵɵproperty("ngIf", ctx_r8.options.useImages);
        i0.ɵɵadvance(1);
        i0.ɵɵproperty("ngIf", ctx_r8.options.useLabels);
        i0.ɵɵadvance(2);
        i0.ɵɵproperty("ngIf", !ctx_r8.selectedCaratTemplate);
        i0.ɵɵadvance(1);
        i0.ɵɵproperty("ngIf", ctx_r8.selectedCaratTemplate && ctx_r8.isItemSelected(option_r15));
    }
}
const _c1 = function (a0, a1) { return { open: a0, animated: a1 }; };
class DropdownMultiComponent extends DropdownComponent {
    /**
     * Constructor
     */
    constructor(dropdownService) {
        super(dropdownService);
        this.dropdownService = dropdownService;
        this.multiModel = [];
        this.multiModelLabels = [];
    }
    /**
     * Selects an item
     */
    selectItem($event, item) {
        $event.stopImmediatePropagation();
        $event.stopPropagation();
        if (this.options.closeOnSelection) {
            this.isOpen = false;
        }
        if (this.multiModel.indexOf(item[this.options.identifier]) >= 0) {
            this.multiModel.splice(this.multiModel.indexOf(item[this.options.identifier]), 1);
            this.multiModelLabels.splice(this.multiModel.indexOf(item[this.options.identifier]), 1);
            this.setElementValue(this.multiModel);
            return false;
        }
        this.multiModel.push(item[this.options.identifier]);
        this.multiModelLabels.push(item[this.options.label]);
        this.setElementValue(this.multiModel);
        return false;
    }
    isValidSelection(item) {
        return true;
    }
    clearSelection() { }
    /**
     * Tests if an item is the selected item
     */
    isItemSelected(item) {
        return this.multiModel.indexOf(item[this.options.identifier]) >= 0;
    }
    /**
     * Tests if the dropdown has a selection
     */
    isSelected() {
        return this.multiModel.length > 0;
    }
    /**
     * Gets the label of the selected item
     */
    getSelectedLabel() {
        if (this.options.selectionsAsLabel === false) {
            return this.options.placeholder;
        }
        else if (this.options.selectionsAsLabel === true) {
            return this.multiModelLabels.join(', ');
        }
        return this.options.selectionsAsLabel;
    }
    /**
     * Gets the label of the selected item
     */
    getSelectedImage() {
        return null;
    }
}
DropdownMultiComponent.ɵfac = function DropdownMultiComponent_Factory(t) { return new (t || DropdownMultiComponent)(i0.ɵɵdirectiveInject(DropdownService)); };
DropdownMultiComponent.ɵcmp = /*@__PURE__*/ i0.ɵɵdefineComponent({ type: DropdownMultiComponent, selectors: [["click-dropdown-multi"]], features: [i0.ɵɵInheritDefinitionFeature], decls: 12, vars: 13, consts: [[1, "dropdown", 3, "ngClass"], [1, "dropdown-display", 3, "click"], [3, "class", 4, "ngIf"], [4, "ngIf"], ["class", "placeholder", 4, "ngIf"], ["class", "image", 4, "ngIf"], ["class", "label", 4, "ngIf"], [1, "dropdown-list"], ["class", "dropdown-list-filter", 4, "ngIf"], ["class", "dropdown-list-item", 3, "ngClass", "click", 4, "ngFor", "ngForOf"], [3, "ngTemplateOutlet"], [1, "placeholder"], [1, "image"], [3, "src"], [1, "label"], [1, "dropdown-list-filter"], [3, "ngModel", "ngModelChange"], [1, "dropdown-list-item", 3, "ngClass", "click"], [1, "selected-item-indicator"]], template: function DropdownMultiComponent_Template(rf, ctx) {
        if (rf & 1) {
            i0.ɵɵelementStart(0, "div", 0)(1, "div", 1);
            i0.ɵɵlistener("click", function DropdownMultiComponent_Template_div_click_1_listener($event) { return ctx.toggle($event); });
            i0.ɵɵtemplate(2, DropdownMultiComponent_i_2_Template, 1, 3, "i", 2);
            i0.ɵɵtemplate(3, DropdownMultiComponent_3_Template, 1, 1, null, 3);
            i0.ɵɵtemplate(4, DropdownMultiComponent_span_4_Template, 2, 1, "span", 4);
            i0.ɵɵtemplate(5, DropdownMultiComponent_span_5_Template, 2, 1, "span", 5);
            i0.ɵɵtemplate(6, DropdownMultiComponent_span_6_Template, 2, 1, "span", 6);
            i0.ɵɵtemplate(7, DropdownMultiComponent_i_7_Template, 1, 3, "i", 2);
            i0.ɵɵtemplate(8, DropdownMultiComponent_8_Template, 1, 1, null, 3);
            i0.ɵɵelementEnd();
            i0.ɵɵelementStart(9, "div", 7);
            i0.ɵɵtemplate(10, DropdownMultiComponent_div_10_Template, 2, 1, "div", 8);
            i0.ɵɵtemplate(11, DropdownMultiComponent_div_11_Template, 6, 7, "div", 9);
            i0.ɵɵelementEnd()();
        }
        if (rf & 2) {
            i0.ɵɵproperty("ngClass", i0.ɵɵpureFunction2(10, _c1, ctx.isOpen, ctx.options.animated));
            i0.ɵɵadvance(2);
            i0.ɵɵproperty("ngIf", ctx.options.displayPlaceholderCaretBefore && !ctx.placeholderCaratTemplate);
            i0.ɵɵadvance(1);
            i0.ɵɵproperty("ngIf", ctx.options.displayPlaceholderCaretBefore && ctx.placeholderCaratTemplate);
            i0.ɵɵadvance(1);
            i0.ɵɵproperty("ngIf", !ctx.isSelected());
            i0.ɵɵadvance(1);
            i0.ɵɵproperty("ngIf", ctx.isSelected() && ctx.getSelectedImage());
            i0.ɵɵadvance(1);
            i0.ɵɵproperty("ngIf", ctx.options.useLabels);
            i0.ɵɵadvance(1);
            i0.ɵɵproperty("ngIf", ctx.options.displayPlaceholderCaretAfter && !ctx.placeholderCaratTemplate);
            i0.ɵɵadvance(1);
            i0.ɵɵproperty("ngIf", ctx.options.displayPlaceholderCaretAfter && ctx.placeholderCaratTemplate);
            i0.ɵɵadvance(2);
            i0.ɵɵproperty("ngIf", ctx.options.filterable);
            i0.ɵɵadvance(1);
            i0.ɵɵproperty("ngForOf", ctx.options.items);
        }
    }, dependencies: [i2.DefaultValueAccessor, i2.NgControlStatus, i2.NgModel, i3.NgClass, i3.NgForOf, i3.NgIf, i3.NgTemplateOutlet], styles: ["[_nghost-%COMP%]   .pull-left[_ngcontent-%COMP%]{float:left}[_nghost-%COMP%]   .pull-right[_ngcontent-%COMP%]{float:right}[_nghost-%COMP%]   .relative[_ngcontent-%COMP%]{position:relative}[_nghost-%COMP%]   .align-left[_ngcontent-%COMP%]{text-align:left}@media (max-width: 1199px){[_nghost-%COMP%]   .align-lg-left[_ngcontent-%COMP%]{text-align:left}}@media (max-width: 991px){[_nghost-%COMP%]   .align-md-left[_ngcontent-%COMP%]{text-align:left}}@media (max-width: 767px){[_nghost-%COMP%]   .align-sm-left[_ngcontent-%COMP%]{text-align:left}}@media (max-width: 543px){[_nghost-%COMP%]   .align-xs-left[_ngcontent-%COMP%]{text-align:left}}[_nghost-%COMP%]   .align-center[_ngcontent-%COMP%]{text-align:center}@media (max-width: 1199px){[_nghost-%COMP%]   .align-lg-center[_ngcontent-%COMP%]{text-align:center}}@media (max-width: 991px){[_nghost-%COMP%]   .align-md-center[_ngcontent-%COMP%]{text-align:center}}@media (max-width: 767px){[_nghost-%COMP%]   .align-sm-center[_ngcontent-%COMP%]{text-align:center}}@media (max-width: 543px){[_nghost-%COMP%]   .align-xs-center[_ngcontent-%COMP%]{text-align:center}}[_nghost-%COMP%]   .align-right[_ngcontent-%COMP%]{text-align:right}@media (max-width: 1199px){[_nghost-%COMP%]   .align-lg-right[_ngcontent-%COMP%]{text-align:right}}@media (max-width: 991px){[_nghost-%COMP%]   .align-md-right[_ngcontent-%COMP%]{text-align:right}}@media (max-width: 767px){[_nghost-%COMP%]   .align-sm-right[_ngcontent-%COMP%]{text-align:right}}@media (max-width: 543px){[_nghost-%COMP%]   .align-xs-right[_ngcontent-%COMP%]{text-align:right}}[_nghost-%COMP%]   .dropdown[_ngcontent-%COMP%]{position:relative;line-height:100%}[_nghost-%COMP%]   .dropdown[_ngcontent-%COMP%]   *[_ngcontent-%COMP%]{box-sizing:border-box}[_nghost-%COMP%]   .dropdown[_ngcontent-%COMP%]   .dropdown-display[_ngcontent-%COMP%]{position:relative;padding:1rem;border:.0625rem solid black;cursor:pointer}[_nghost-%COMP%]   .dropdown[_ngcontent-%COMP%]   .dropdown-display[_ngcontent-%COMP%]   i[_ngcontent-%COMP%], [_nghost-%COMP%]   .dropdown[_ngcontent-%COMP%]   .dropdown-display[_ngcontent-%COMP%]   fa-icon[_ngcontent-%COMP%]{position:absolute;top:50%;transform:translateY(-50%);right:1rem}[_nghost-%COMP%]   .dropdown[_ngcontent-%COMP%]   .dropdown-display[_ngcontent-%COMP%]   span[_ngcontent-%COMP%]{line-height:100%}[_nghost-%COMP%]   .dropdown[_ngcontent-%COMP%]   img[_ngcontent-%COMP%]{max-width:50px;max-height:50px}[_nghost-%COMP%]   .dropdown[_ngcontent-%COMP%]   .dropdown-list[_ngcontent-%COMP%]{max-height:0;overflow:hidden;position:absolute;width:100%;background:white;z-index:2;left:0;transition:max-height .2s linear;transition-delay:.15s;animation-delay:.15s;backface-visibility:hidden;-webkit-transition:max-height .2s linear;-webkit-transition-delay:.15s;-webkit-animation-delay:.15s;-webkit-backface-visibility:hidden}[_nghost-%COMP%]   .dropdown[_ngcontent-%COMP%]   .dropdown-list[_ngcontent-%COMP%]   .dropdown-list-item[_ngcontent-%COMP%]{border-left:.0625rem solid black;border-bottom:.0625rem solid black;border-right:.0625rem solid black;padding:.875rem;font-size:.875rem;position:relative;transition:all .3s linear;transition-delay:0s;animation-delay:0s;backface-visibility:hidden;-webkit-transition:all .3s linear;-webkit-transition-delay:0s;-webkit-animation-delay:0s;-webkit-backface-visibility:hidden;cursor:pointer}[_nghost-%COMP%]   .dropdown[_ngcontent-%COMP%]   .dropdown-list[_ngcontent-%COMP%]   .dropdown-list-item[_ngcontent-%COMP%]   i[_ngcontent-%COMP%], [_nghost-%COMP%]   .dropdown[_ngcontent-%COMP%]   .dropdown-list[_ngcontent-%COMP%]   .dropdown-list-item[_ngcontent-%COMP%]   fa-icon[_ngcontent-%COMP%]{color:#000;opacity:0;position:absolute;top:50%;transform:translateY(-50%);transition:all .15s linear;transition-delay:0s;animation-delay:0s;backface-visibility:hidden;-webkit-transition:all .15s linear;-webkit-transition-delay:0s;-webkit-animation-delay:0s;-webkit-backface-visibility:hidden;right:1rem}[_nghost-%COMP%]   .dropdown[_ngcontent-%COMP%]   .dropdown-list[_ngcontent-%COMP%]   .dropdown-list-item[_ngcontent-%COMP%]   img[_ngcontent-%COMP%]{vertical-align:middle;margin:0 .5rem 0 0}[_nghost-%COMP%]   .dropdown[_ngcontent-%COMP%]   .dropdown-list[_ngcontent-%COMP%]   .dropdown-list-item.selected[_ngcontent-%COMP%], [_nghost-%COMP%]   .dropdown[_ngcontent-%COMP%]   .dropdown-list[_ngcontent-%COMP%]   .dropdown-list-item[_ngcontent-%COMP%]:hover{background:#f2f2f2}[_nghost-%COMP%]   .dropdown[_ngcontent-%COMP%]   .dropdown-list[_ngcontent-%COMP%]   .dropdown-list-item.selected[_ngcontent-%COMP%]   i[_ngcontent-%COMP%], [_nghost-%COMP%]   .dropdown[_ngcontent-%COMP%]   .dropdown-list[_ngcontent-%COMP%]   .dropdown-list-item.selected[_ngcontent-%COMP%]   fa-icon[_ngcontent-%COMP%]{opacity:1}[_nghost-%COMP%]   .dropdown.open[_ngcontent-%COMP%] > .dropdown-list[_ngcontent-%COMP%]{max-height:800px}"] });
(function () {
    (typeof ngDevMode === "undefined" || ngDevMode) && i0.ɵsetClassMetadata(DropdownMultiComponent, [{
            type: Component,
            args: [{ selector: 'click-dropdown-multi', template: "<div class=\"dropdown\" [ngClass]=\"{open: isOpen, animated: options.animated}\">\n  <div class=\"dropdown-display\" (click)=\"toggle($event)\">\n    <i class=\"{{options.caretClass}}\" *ngIf=\"options.displayPlaceholderCaretBefore && !placeholderCaratTemplate\"></i>\n    <ng-template *ngIf=\"options.displayPlaceholderCaretBefore && placeholderCaratTemplate\" [ngTemplateOutlet]=\"placeholderCaratTemplate\"></ng-template>\n    <span *ngIf=\"! isSelected()\" class=\"placeholder\">{{ options.placeholder }}</span>\n    <span class=\"image\" *ngIf=\"isSelected() && getSelectedImage()\">\n      <img [src]=\"getSelectedImage()\"/>\n    </span>\n    <span class=\"label\" *ngIf=\"options.useLabels\">\n      <span *ngIf=\"isSelected()\">{{ getSelectedLabel() }}</span>\n    </span>\n    <i class=\"{{options.caretClass}}\" *ngIf=\"options.displayPlaceholderCaretAfter && !placeholderCaratTemplate\"></i>\n    <ng-template *ngIf=\"options.displayPlaceholderCaretAfter && placeholderCaratTemplate\" [ngTemplateOutlet]=\"placeholderCaratTemplate\"></ng-template>\n  </div>\n  <div class=\"dropdown-list\">\n    <div class=\"dropdown-list-filter\" *ngIf=\"options.filterable\">\n      <input [(ngModel)]=\"filterModel\" (ngModelChange)=\"onFilterChange()\"/>\n    </div>\n    <div class=\"dropdown-list-item\" [ngClass]=\"{selected:isItemSelected(option)}\" *ngFor=\"let option of options.items\" (click)=\"selectItem($event, option)\">\n      <span class=\"image\" *ngIf=\"options.useImages\">\n         <span><img [src]=\"option[options.imageKey]\"/></span>\n       </span>\n      <span class=\"label\" *ngIf=\"options.useLabels\">\n        <span>{{option[options.label]}}</span>\n      </span>\n       <span class=\"selected-item-indicator\">\n         <i class=\"{{options.selectedItemClass}}\" *ngIf=\"!selectedCaratTemplate\"></i>\n         <ng-template *ngIf=\"selectedCaratTemplate && isItemSelected(option)\" [ngTemplateOutlet]=\"selectedCaratTemplate\"></ng-template>\n     </span>\n    </div>\n  </div>\n</div>\n", styles: [":host .pull-left{float:left}:host .pull-right{float:right}:host .relative{position:relative}:host .align-left{text-align:left}@media (max-width: 1199px){:host .align-lg-left{text-align:left}}@media (max-width: 991px){:host .align-md-left{text-align:left}}@media (max-width: 767px){:host .align-sm-left{text-align:left}}@media (max-width: 543px){:host .align-xs-left{text-align:left}}:host .align-center{text-align:center}@media (max-width: 1199px){:host .align-lg-center{text-align:center}}@media (max-width: 991px){:host .align-md-center{text-align:center}}@media (max-width: 767px){:host .align-sm-center{text-align:center}}@media (max-width: 543px){:host .align-xs-center{text-align:center}}:host .align-right{text-align:right}@media (max-width: 1199px){:host .align-lg-right{text-align:right}}@media (max-width: 991px){:host .align-md-right{text-align:right}}@media (max-width: 767px){:host .align-sm-right{text-align:right}}@media (max-width: 543px){:host .align-xs-right{text-align:right}}:host .dropdown{position:relative;line-height:100%}:host .dropdown *{box-sizing:border-box}:host .dropdown .dropdown-display{position:relative;padding:1rem;border:.0625rem solid black;cursor:pointer}:host .dropdown .dropdown-display i,:host .dropdown .dropdown-display fa-icon{position:absolute;top:50%;transform:translateY(-50%);right:1rem}:host .dropdown .dropdown-display span{line-height:100%}:host .dropdown img{max-width:50px;max-height:50px}:host .dropdown .dropdown-list{max-height:0;overflow:hidden;position:absolute;width:100%;background:white;z-index:2;left:0;transition:max-height .2s linear;transition-delay:.15s;animation-delay:.15s;backface-visibility:hidden;-webkit-transition:max-height .2s linear;-webkit-transition-delay:.15s;-webkit-animation-delay:.15s;-webkit-backface-visibility:hidden}:host .dropdown .dropdown-list .dropdown-list-item{border-left:.0625rem solid black;border-bottom:.0625rem solid black;border-right:.0625rem solid black;padding:.875rem;font-size:.875rem;position:relative;transition:all .3s linear;transition-delay:0s;animation-delay:0s;backface-visibility:hidden;-webkit-transition:all .3s linear;-webkit-transition-delay:0s;-webkit-animation-delay:0s;-webkit-backface-visibility:hidden;cursor:pointer}:host .dropdown .dropdown-list .dropdown-list-item i,:host .dropdown .dropdown-list .dropdown-list-item fa-icon{color:#000;opacity:0;position:absolute;top:50%;transform:translateY(-50%);transition:all .15s linear;transition-delay:0s;animation-delay:0s;backface-visibility:hidden;-webkit-transition:all .15s linear;-webkit-transition-delay:0s;-webkit-animation-delay:0s;-webkit-backface-visibility:hidden;right:1rem}:host .dropdown .dropdown-list .dropdown-list-item img{vertical-align:middle;margin:0 .5rem 0 0}:host .dropdown .dropdown-list .dropdown-list-item.selected,:host .dropdown .dropdown-list .dropdown-list-item:hover{background:#f2f2f2}:host .dropdown .dropdown-list .dropdown-list-item.selected i,:host .dropdown .dropdown-list .dropdown-list-item.selected fa-icon{opacity:1}:host .dropdown.open>.dropdown-list{max-height:800px}\n"] }]
        }], function () { return [{ type: DropdownService }]; }, null);
})();

class DropdownOptionsModel {
    /**
     * Constructor
     */
    constructor(items, options) {
        /**
         * The is what we should use to get the identifier of the selected item
         * Defaults to ID
         */
        this.identifier = 'id';
        /**
         * This is what we should use to get the display value of the selected item
         */
        this.label = 'label';
        /**
         * Placeholder to display
         */
        this.placeholder = 'Select...';
        /**
         * If true, adds the animated class to the dropdown
         */
        this.animated = true;
        /**
         * If true, will close other dropdowns when this one opens
         */
        this.closeOthers = true;
        /**
         * If true, will close when the document is clicked
         */
        this.closeOnDocumentClick = true;
        /**
         * If true, will close when they make a selection
         */
        this.closeOnSelection = true;
        /**
         * When true, will display the placeholder caret after the placeholder text
         */
        this.displayPlaceholderCaretAfter = true;
        /**
         * When true, will display the placeholder caret before the placeholder text
         */
        this.displayPlaceholderCaretBefore = false;
        /**
         * The CSS caret class (You can use font awesome classes, but I wouldn't include font awesome in this library)
         */
        this.caretClass = 'fa fa-angle-down';
        /**
         * The CSS selected item class (You can use font awesome classes, but I wouldn't include font awesome in this library)
         */
        this.selectedItemClass = 'fa fa-check-circle';
        /**
         * Used only for multi dropdowns, when true will use all of the selections as the placeholder label
         * When it is a string, it will use the string. When false, it will use the placeholder label
         */
        this.selectionsAsLabel = false;
        /**
         * When true, will use an image in the drop down, requires the imageKey
         */
        this.useImages = false;
        /**
         * Where in the object of options to get the image from
         */
        this.imageKey = 'image';
        /**
         * Flag to use labels, typically only used if you want to hide labels because you are using images
         */
        this.useLabels = true;
        /**
         * When true, it will only allow valid values. All other will be destroyed
         */
        this.allowOnlyValidValues = true;
        /**
         * Flag for if this is filterable
         */
        this.filterable = false;
        /**
         * If true, the placeholder will become a selectable item when something other than
         * the placeholder is selected.
         */
        this.usePlaceholderAsItem = false;
        this.items = items;
        if (typeof options !== 'undefined' && options !== null) {
            if (options.identifier) {
                this.identifier = options.identifier;
            }
            if (options.label) {
                this.label = options.label;
            }
            if (typeof options.placeholder !== 'undefined') {
                this.placeholder = options.placeholder;
            }
            if (typeof options.animated !== 'undefined') {
                this.animated = options.animated;
            }
            if (typeof options.closeOthers !== 'undefined') {
                this.closeOthers = options.closeOthers;
            }
            if (typeof options.closeOnDocumentClick !== 'undefined') {
                this.closeOnDocumentClick = options.closeOnDocumentClick;
            }
            if (typeof options.closeOnSelection !== 'undefined') {
                this.closeOnSelection = options.closeOnSelection;
            }
            if (typeof options.displayPlaceholderCaretAfter !== 'undefined') {
                this.displayPlaceholderCaretAfter = options.displayPlaceholderCaretAfter;
            }
            if (typeof options.displayPlaceholderCaretBefore !== 'undefined') {
                this.displayPlaceholderCaretBefore = options.displayPlaceholderCaretBefore;
            }
            if (typeof options.selectionsAsLabel !== 'undefined') {
                this.selectionsAsLabel = options.selectionsAsLabel;
            }
            if (typeof options.caretClass !== 'undefined') {
                this.caretClass = options.caretClass;
            }
            if (typeof options.selectedItemClass !== 'undefined') {
                this.selectedItemClass = options.selectedItemClass;
            }
            if (typeof options.useImages !== 'undefined') {
                this.useImages = options.useImages;
            }
            if (typeof options.imageKey !== 'undefined') {
                this.imageKey = options.imageKey;
            }
            if (typeof options.useLabels !== 'undefined') {
                this.useLabels = options.useLabels;
            }
            if (typeof options.allowOnlyValidValues !== 'undefined') {
                this.allowOnlyValidValues = options.allowOnlyValidValues;
            }
            if (typeof options.filterable !== 'undefined') {
                this.filterable = options.filterable;
            }
            if (typeof options.usePlaceholderAsItem !== 'undefined') {
                this.usePlaceholderAsItem = options.usePlaceholderAsItem;
            }
        }
    }
}

class DropdownsModule {
}
DropdownsModule.ɵfac = function DropdownsModule_Factory(t) { return new (t || DropdownsModule)(); };
DropdownsModule.ɵmod = /*@__PURE__*/ i0.ɵɵdefineNgModule({ type: DropdownsModule });
DropdownsModule.ɵinj = /*@__PURE__*/ i0.ɵɵdefineInjector({ imports: [FormsModule,
        ReactiveFormsModule,
        CommonModule] });
(function () {
    (typeof ngDevMode === "undefined" || ngDevMode) && i0.ɵsetClassMetadata(DropdownsModule, [{
            type: NgModule,
            args: [{
                    declarations: [DropdownComponent, DropdownMultiComponent],
                    imports: [
                        FormsModule,
                        ReactiveFormsModule,
                        CommonModule
                    ],
                    exports: [DropdownComponent, DropdownMultiComponent]
                }]
        }], null, null);
})();
(function () {
    (typeof ngJitMode === "undefined" || ngJitMode) && i0.ɵɵsetNgModuleScope(DropdownsModule, { declarations: [DropdownComponent, DropdownMultiComponent], imports: [FormsModule,
            ReactiveFormsModule,
            CommonModule], exports: [DropdownComponent, DropdownMultiComponent] });
})();

/*
 * Public API Surface of dropdowns
 */

/**
 * Generated bundle index. Do not edit.
 */

export { DropdownComponent, DropdownMultiComponent, DropdownOptionsModel, DropdownService, DropdownsModule };
//# sourceMappingURL=click-dropdowns.mjs.map
