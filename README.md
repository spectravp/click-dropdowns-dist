# Dropdown #

A component to make pretty ```select``` elements.

## Installation ##

```angular2
import {DropdownsModule} from '@click/dropdowns';

@NgModule({
  imports: [
    DropdownsModule,
  ],
})
```

### Dropdown Options ###

* ```items```: (REQUIRED) An array of objects to use as the select menu options.
* ```identifier```: The property to use as the id. Defaults to 'id'.
* ```label```: The property to use as the label. Defaults to 'label'.
* ```placeholder```: What to display when nothing is selected.
* ```animated```: True/Flase to enable/disable animations.
* ```closeOthers```: When true, will close other dropdowns. Default true
* ```closeOnDocumentClick```: When true, will close menu on document click: Default true
* ```closeOnSelection```: When true, will close the menu when a user makes a selection. Default true
* ```displayPlaceholderCaretAfter```: When true, will display the dropdown carat after the text. Default true
* ```displayPlaceholderCaretBefore```: When true, will display the dropdown carat before the text. Default false
* ```caretClass```: The class to use for the dropdown carat. Default fa fa-angle-down
* ```caretClass```: The class to use for the dropdown carat. Default fa fa-angle-down
* ```selectedItemClass```: The class to use for the selected item indicator. Default fa fa-check-circle
* ```selectionsAsLabel```: Used only for multi dropdowns, when true will use all of the selections as the placeholder label. When it is a string, it will use the string. When false, it will use the placeholder label. Default false
* ```useImages```: When true, will use an image in the drop down, requires the imageKey, default false
* ```imageKey```: Where in the object of options to get the image from. Default "image"
* ```useLabels```: Flag to use labels, typically only used if you want to hide labels because you are using images. Default true
* ```allowOnlyValidValues```: Flag to disregard invalid initial values (Not supported with multi-select dropdowns. Default true
* ```filterable```: Flag to allow filtering of the list. Default false
* ```usePlaceholderAsItem```: When something other than the placeholder is selected, the placeholder becomes the first item in the dropdown. Default false
 

## Usage ##

In a component.ts file:

```typescript
import {Component, OnInit} from '@angular/core';
import {DropdownOptionsModel} from '@click/dropdowns';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  public stateDropDownOptions: DropdownOptionsModel;
  public form: FormGroup;

  constructor(private formBuilder: FormBuilder) { }

  ngOnInit(): void {

    const states = [
      {
        id: 'NY',
        state: 'New York'
      },
      {
        id: 'FL',
        state: 'Florida'
      },
      {
        id: 'SC',
        state: 'South Carolina'
      }
    ];

    this.form = this.formBuilder.group({
      states: new FormControl('', [Validators.required]),
      someotherField: new FormControl('')
    });

    this.stateDropDownOptions = new DropdownOptionsModel(states, {
      placeholder: 'My Custom Placeholder',
      label: 'state'
      // ... any other options you want to override
    });
  }
}
```

In a component.html file:

```angular2html
<!-- Standard Dropdown -->
<click-dropdown [options]="stateDropDownOptions" [element]="form.controls.states"></click-dropdown>

<!-- Multi Select Dropdown -->
<click-dropdown-multi [options]="stateDropDownOptions" [element]="form.controls.states"></click-dropdown-multi>
```

## Template ##

```angular2html
<div class="dropdown" [ngClass]="{open: isOpen, animated: options.animated}">
  <div class="dropdown-display" (click)="toggle($event)">
    <i class="{{options.caretClass}}" *ngIf="options.displayPlaceholderCaretBefore"></i>
    <span *ngIf="! isSelected()" class="placeholder">{{ options.placeholder }}</span>
    <span class="image" *ngIf="isSelected() && getSelectedImage()">
      <img [src]="getSelectedImage()"/>
    </span>
    <span class="label" *ngIf="options.useLabels">
      <span *ngIf="isSelected()">{{ getSelectedLabel() }}</span>
    </span>
    <i class="{{options.caretClass}}" *ngIf="options.displayPlaceholderCaretAfter"></i>
  </div>
  <div class="dropdown-list">
    <div class="dropdown-list-filter" *ngIf="options.filterable">
      <input [(ngModel)]="filterModel" (ngModelChange)="onFilterChange()"/>
    </div>
    <div class="dropdown-list-item" [ngClass]="{selected:isItemSelected(option)}" *ngFor="let option of options.items" (click)="selectItem($event, option)">
      <span class="image" *ngIf="options.useImages">
         <span><img [src]="option[options.imageKey]"/></span>
       </span>
      <span class="label" *ngIf="options.useLabels">
         <span>{{option[options.label]}}</span>
       </span>
       <span class="selected-item-indicator">
         <i class="{{options.selectedItemClass}}"></i>
       </span>
    </div>
  </div>
</div>
```

## Using Dropdowns without a Form Control ##

You can use the dropdown without a form control but if you want two-way data binding you have to use a slightly different syntax for the element property:

```angular2html
<!-- Standard Dropdown -->
<click-dropdown [options]="stateDropDownOptions" [(element)]="myModel.states"></click-dropdown>

<!-- Multi Select Dropdown -->
<click-dropdown-multi [options]="stateDropDownOptions" [(element)]="myModel.states"></click-dropdown-multi>
```
## Using Dropdowns with Custom Carat Templates (FontAwesome) ##

```angular2html
<ng-template #placeholderCarat >
    <fa-icon icon="arrow-down"></fa-icon>
</ng-template>
<ng-template #selectedCarat >
    <fa-icon icon="check"></fa-icon>
</ng-template>
<click-dropdown-multi 
        [options]="stateDropDownOptions" 
        [(element)]="myModel.states"
        [placeholderCaratTemplate]="placeholderCarat" 
        [selectedCaratTemplate]="selectedCarat" ></click-dropdown-multi>
```
## Events ##

Whenever the value of a drop down is changed, an event is dispatched that contains the new value. The $event passed into the callback function is the selected value.
```angular2html
<!-- Standard Dropdown -->
<click-dropdown [options]="stateDropDownOptions" [(element)]="myModel.states"  (elementChange)="testChangeEvent($event)"></click-dropdown>
```

